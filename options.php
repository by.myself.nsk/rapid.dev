<?php
/**@global CMain $APPLICATION */

use Rapid\Dev\Helper\Module\Options;
use Rapid\Dev\Helper\SitePage;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
global $APPLICATION;
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/options.php");
IncludeModuleLangFile(__FILE__);
CJSCore::Init(array('jquery'));
CModule::IncludeModule('iblock');
$obOptions = new Options(dirname(__FILE__) . '/.options.php');
$obAdminTabControl = $obOptions->getAdminTabControl();
$obOptions->saveOptions();

?>
<div id="constructor-wrapper">
    <?
    if (SitePage::isAjax('refresh')) {
        $APPLICATION->RestartBuffer();
        ob_start();
    }
    $obOptions->getOptionsTree();
    $obOptions->showForm('rapid-options', 'rapid-options-wrapper', array('save', 'apply', 'default'));
    if (SitePage::isAjax('refresh')) {
        SitePage::sendAjaxJson([
            'html' => ob_get_clean(),
            'location' => $APPLICATION->GetCurPageParam(http_build_query(array_diff_key($_POST, array('sessid' => '', 'autosave_id' => '', 'action' => ''))))
        ]);
    }
    ?>
</div>
<?
