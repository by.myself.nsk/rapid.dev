<?php

namespace Rapid\Dev;

use Bitrix\Main\Page\Asset;
use COption;
use CUtil;
use Exception;

class Recaptcha
{
    protected static function getPublicKey()
    {
        return COption::GetOptionString('rapid.dev', 'recaptcha_public');
    }

    protected static function getSecretKey()
    {
        return COption::GetOptionString('rapid.dev', 'recaptcha_secret');
    }

    protected static function getVerifyUrl()
    {
        return 'https://www.google.com/recaptcha/api/siteverify';
    }

    public static function loadRecaptchaJs()
    {
        Asset::getInstance()->addString('<script data-skip-moving="true" src="https://www.google.com/recaptcha/api.js?render=' . static::getPublicKey() . '&hl=' . LANGUAGE_ID . '"></script>');
        Asset::getInstance()->addString('<script data-skip-moving="true">window.grecaptchaPublicKey = ' . CUtil::PhpToJSObject(static::getPublicKey()) . ';</script>');
    }

    public static function checkCaptcha($gRecaptchaResponse)
    {
        $result = false;

        try {
            $values = array(
                'secret' => static::getSecretKey(),
                'response' => $gRecaptchaResponse,
                'remoteip' => $_SERVER['REMOTE_ADDR'],
            );

            if (!$curl = curl_init()) {
                throw new Exception('Install curl!');
            }

            curl_setopt($curl, CURLOPT_URL, static::getVerifyUrl());
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $values);
            $out = curl_exec($curl);
            curl_close($curl);

            if (!$out) {
                throw new Exception('Curl error.');
            }
            if (!$out = json_decode($out, true)) {
                throw new Exception('Error while decode json.');
            }

            $result = $out['success'];
        } catch (Exception $e) {

        }
        return $result;
    }
}
