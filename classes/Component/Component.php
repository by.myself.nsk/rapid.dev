<?php

namespace Rapid\Dev\Component;

use CBitrixComponent;
use CBitrixComponentTemplate;
use CEventType;
use CIBlock;
use CIBlockElement;
use CIBlockParameters;
use CIBlockProperty;
use CModule;

class Component extends CBitrixComponent
{
    /**
     * Возвращает список типов инфоблоков в виде id => name
     * @param array $arFirstElement Массив с первым элементом списка. Необязательный
     * @return array|bool
     */
    public static function getParametersIblockTypes($arFirstElement = array())
    {
        if(empty($arFirstElement)) {
            $arFirstElement = array("-"=>" ");
        }
        return CIBlockParameters::GetIBlockTypes($arFirstElement);
    }

    /**
     * Возвращает список инфоблоков в виде id => name
     * @param string $iblockType Тип инфоблока. В случае использования в .parameters.php - $arCurrentValues["IBLOCK_TYPE"]
     * @param string $siteId Идентификатор сайта. В случае использования в .parameters.php - $_REQUEST["site"]
     * @return array
     */
    public static function getParametersIblocks($iblockType, $siteId)
    {
        $arRes = array("-" => " ");
        $dbIblocks = CIBlock::GetList(array("SORT" => "ASC"), array("SITE_ID" => $siteId, "TYPE" => ($iblockType != "-" ? $iblockType : "")));
        while ($arIblock = $dbIblocks->Fetch()) {
            $arRes[$arIblock["ID"]] = $arIblock["NAME"];
        }
        return $arRes;
    }

    /**
     * Возвращает список элементов инфоблока
     * @param int $intIblockId
     * @return array|bool
     */
    public static function getParametersIblockElements($intIblockId)
    {
        $intIblockId = intval($intIblockId);
        if(!$intIblockId) {
            return false;
        }
        CModule::IncludeModule('iblock');
        $dbItems = CIblockElement::GetList(
            array(),
            array(
                'IBLOCK_ID' => $intIblockId,
                'ACTIVE' => 'Y'
            ),
            false,
            false,
            array(
                'IBLOCK_ID',
                'ID',
                'NAME',
                'CODE',
            )
        );
        $arItems = array();
        while ($arItem = $dbItems->Fetch()) {
            $arItems[$arItem['ID']] = $arItem['NAME'];
        }
        return $arItems;
    }

    /**
     * Возвращает список полей инфоблоков в виде code => name. Используется в .parameters.php
     * @param $intIblockId
     * @param bool $bMarkRequired Если выставлен в true, помечает имена обязательных полей звездочкой
     * @return array
     */
    public static function getParametersIblockFields($intIblockId, $bMarkRequired = true)
    {
        CModule::IncludeModule('iblock');
        $arFieldCodes = CIBlockParameters::GetFieldCode('name', 'parent');
        if (empty($arFieldCodes)) {
            return array();
        }
        $arFieldCodes = $arFieldCodes['VALUES'];
        if (!$bMarkRequired) {
            return $arFieldCodes;
        }

        $arFields = CIBlock::GetFields($intIblockId);
        if (empty($arFields)) {
            return array();
        }
        foreach ($arFieldCodes as $strKey => &$strName) {
            if ($arFields[$strKey]['IS_REQUIRED'] == 'Y' && empty($arFields[$strKey]['DEFAULT_VALUE'])) {
                $strName .= ' *';
            }
        }
        return $arFieldCodes;
    }

    /**
     * Возвращает список активных свойств инфоблока в виде code => name
     * @param int $intIblockId ID инфоблока
     * @param bool $bMarkRequired Если выставлен в true, помечает имена обязательных свойств звездочкой
     * @return array|bool
     */
    public static function getParametersIblockProperties($intIblockId, $bMarkRequired = true)
    {
        $arProperties = array();
        if(!intval($intIblockId)) {
            return $arProperties;
        }
        $dbProperties = CIBlockProperty::GetList(array("sort"=>"asc", "name"=>"asc"), array("ACTIVE" => "Y", "IBLOCK_ID" => $intIblockId));
        while ($arProperty = $dbProperties->Fetch()) {
            $arProperties[$arProperty["CODE"]] = "[".$arProperty["CODE"]."] ".$arProperty["NAME"].($bMarkRequired && $arProperty["IS_REQUIRED"] == 'Y' ? ' *' : '');
        }
        return $arProperties;
    }

    public static function getParametersIblockSectionFields($intIblockId, $bMarkRequired = true)
    {
        CModule::IncludeModule('iblock');
        $arFieldCodes = CIBlockParameters::GetSectionFieldCode(
            GetMessage("RPDUFS_PARAM_IBLOCK_FIELD"),
            "DATA_SOURCE",
            array()
        );
        if (empty($arFieldCodes)) {
            return false;
        }
        $arFieldCodes = $arFieldCodes['VALUES'];
        if (!$bMarkRequired) {
            return $arFieldCodes;
        }
        $arFields = CIBlock::GetFields($intIblockId);
        if (empty($arFields)) {
            return false;
        }
        foreach ($arFieldCodes as $strKey => &$strName) {
            if ($arFields['SECTION_'.$strKey]['IS_REQUIRED'] == 'Y' && empty($arFields['SECTION_'.$strKey]['DEFAULT_VALUE'])) {
                $strName .= ' *';
            }
        }
        return $arFieldCodes;
    }

    public static function getParametersIblockSectionUserFields($intIblockId, $bMarkRequired = true)
    {

    }

    /**
     * Возвращает список типов почтовых событий для использования в .parameters.php
     * @param string $languageId ID языка
     * @return array
     */
    public static function getParametersMailTypes($languageId = LANGUAGE_ID)
    {
        $dbEventTypes = CEventType::GetList(array(
            "LID" => $languageId
        ));
        $arEventTypes = array();
        while ($arEventType = $dbEventTypes->Fetch()) {
            $arEventTypes[$arEventType['EVENT_NAME']] = '['.$arEventType['EVENT_NAME'].'] '.$arEventType['NAME'];
        }
        return $arEventTypes;
    }

//----------------------------------------------------------------------------------------------------------------------
// методы для подготовки параметров перед их использованием
    protected $arParametersFromFiles = array();

    protected function getParametersDescriptions()
    {
        if (!empty($this->arParametersFromFiles)) {
            return $this->arParametersFromFiles;
        }
        $strComponentParametersFilePath = $_SERVER['DOCUMENT_ROOT'].$this->getPath() . '/.parameters.php';
        if (file_exists($strComponentParametersFilePath)) {
            IncludeModuleLangFile($strComponentParametersFilePath);
            /** @noinspection PhpIncludeInspection */
            include $strComponentParametersFilePath;
            /**@var array $arComponentParameters*/
            $this->arParametersFromFiles = $arComponentParameters['PARAMETERS'];
        }
        if (!$obComponentTemplate = $this->getTemplate()) {
            $this->initComponentTemplate();
            $obComponentTemplate = $this->getTemplate();
        }
        $strComponentTemplateParameters = $_SERVER['DOCUMENT_ROOT'].$obComponentTemplate->GetFolder() . '/.parameters.php';
        if (file_exists($strComponentTemplateParameters)) {
            IncludeModuleLangFile($strComponentTemplateParameters);
            /** @noinspection PhpIncludeInspection */
            include $strComponentTemplateParameters;
            /**@var array $arTemplateParameters*/
            $this->arParametersFromFiles = array_merge($this->arParametersFromFiles, $arTemplateParameters);
        }
        return $this->arParametersFromFiles;
    }

    public function getParameterDescription($strParamCode)
    {
        if (empty($strParamCode)) {
            return null;
        }
        if (empty($this->arParametersFromFiles)) {
            $this->getParametersDescriptions();
        }
        return $this->arParametersFromFiles[$strParamCode];
    }

    /**
     * Дополняет массив arParams недостающими параметрами, которые описаны в файлах .parameters.php компонента и шаблона,
     * но не переданы при вызове компонента.
     * @param array $arTargetParams Массив, который нужно дополнить
     * @return bool
     */
    public function setDefaultComponentParams(&$arTargetParams)
    {
        $arDefaultParams = $this->getParametersDescriptions();
        if (!is_array($arDefaultParams) || empty($arDefaultParams)) {
            return false;
        }
        foreach ($arDefaultParams as $strCode => $arParameter) {
            if (!isset($arTargetParams[$strCode]) && isset($arParameter['DEFAULT'])) {
                $arTargetParams[$strCode] = $arParameter['DEFAULT'];
            }
        }
        return true;
    }

    /**
     * Готовит переданное значение перед использованием в компоненте. Предназначено для использования в onPrepareComponentParams
     * @param mixed $param Параметр
     * @param string $strParamType Тип параметра. Возможные значения: string, int, bool, array
     * @param mixed|null $defaultValue Значение по умолчанию. Будет использовано, если в $param ничего не передано (в соответствии с типом)
     * @return array|bool|int|mixed|string|null
     */
    public static function prepareParam(&$param, $strParamType, $defaultValue = null)
    {
//        Возможные значения типа параметра в файле .parameters.php:
//        STRING | CHECKBOX | LIST | FILE | CUSTOM
        $strParamType = ToUpper($strParamType);
        switch ($strParamType) {
            default:
            case 'STRING':
                $param = trim($param);
                break;
            case 'INT':
            case 'NUMBER':
                $param = intval(trim($param));
                break;
            case 'BOOL':
            case 'CHECKBOX':
                if(is_bool($param))
                    break;
                $param = !($param == 'N' || $param == '' || $param == 'false');
                break;
            case 'ARRAY':
            case 'LIST':
                if(!is_array($param))
                    $param = array();
                $param = array_diff($param, array(""));
                break;
            case 'FILE':
            case 'CUSTOM':
                break;
        }
        if (!in_array($strParamType, array('BOOL', 'CHECKBOX')) && empty($param) && !is_null($defaultValue)) {
            $param = $defaultValue;
        }
        return $param;
    }

    /**
     * Готовит $arParams перед использованием в компоненте. Предназначено для использования в onPrepareComponentParams
     * Также приводит параметр к указанному в его описании типу
     * @param $arParams
     */
    public function prepareParams(&$arParams)
    {
        if (empty($arParams)) {
            return;
        }
        foreach ($arParams as $strParamCode => &$paramValue) {
            $arParameterDescription = $this->getParameterDescription($strParamCode);
            $strParamType = $arParameterDescription['TYPE'];
            if ($strParamType == 'LIST' && (!isset($arParameterDescription['MULTIPLE']) || $arParameterDescription['MULTIPLE'] != 'Y')) {
                $strParamType = 'STRING';
            }
            $this->prepareParam($paramValue, $strParamType);
        }
        unset($paramValue);
    }

//----------------------------------------------------------------------------------------------------------------------
    public static function checkComponentTemplate($strComponentName, $strComponentTemplate)
    {
        $component = new CBitrixComponent();
        if ($component->InitComponent($strComponentName, $strComponentTemplate)) {
            $obTemplate = new CBitrixComponentTemplate();
            $obTemplate->setLanguageId(LANGUAGE_ID);
            $customTemplatePath = '';
            return $obTemplate->Init($component, SITE_TEMPLATE_ID, $customTemplatePath);
        }
        return false;
    }
}
