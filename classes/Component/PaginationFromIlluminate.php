<?php

namespace Rapid\Dev\Component;

use Illuminate\Pagination\LengthAwarePaginator;
use Rapid\Dev\Helper\SitePage;

class PaginationFromIlluminate
{
    protected $pageWindow = 5;

    /** @var LengthAwarePaginator */
    protected $result;
    protected $startPage;
    protected $endPage;

    public function __construct($result)
    {
        $this->result = $result;
        $this->calculateWindowPages();
    }

    /**
     * @return int
     */
    public function getPageWindow(): int
    {
        return $this->pageWindow;
    }

    /**
     * @param int $pageWindow
     * @return $this
     */
    public function setPageWindow(int $pageWindow)
    {
        $this->pageWindow = $pageWindow;
        $this->calculateWindowPages();
        return $this;
    }

    protected function calculateWindowPages()
    {
        $currentPage = $this->result->currentPage();
        $halfWindow = floor($this->getPageWindow() / 2);
        $this->startPage = max(1, $currentPage - $halfWindow);
        $this->endPage = min($this->result->lastPage(), $currentPage + $halfWindow);
    }

    public function makeUrl($intPage)
    {
        $strPagen = $this->result->getPageName();
        return SitePage::GetCurPageParam(array($strPagen => $intPage), 0, $intPage == 1 ? array($strPagen) : array());
    }

    public function getPrevPageNumber()
    {
        return $this->result->currentPage() - 1;
    }

    public function getPrevPageLink()
    {
        return $this->makeUrl($this->getPrevPageNumber());
    }

    public function getNextPageNumber()
    {
        return $this->result->currentPage() + 1;
    }

    public function getNextPageLink()
    {
        return $this->makeUrl($this->getNextPageNumber());
    }

    public function getFirstPageNumber()
    {
        return 1;
    }

    public function getFirstPageLink()
    {
        return $this->makeUrl($this->getFirstPageNumber());
    }

    public function getLastPageNumber()
    {
        return $this->result->lastPage();
    }

    public function getLastPageLink()
    {
        return $this->makeUrl($this->getLastPageNumber());
    }

    public function getMidBeforePageNumber()
    {
        return floor(($this->startPage - 1) / 2) + 1;
    }

    public function getMidBeforePageLink()
    {
        $intMidPage = $this->getMidBeforePageNumber();
        return $this->makeUrl($intMidPage);
    }

    public function getMidBeforePageText()
    {
        if ($this->result["nStartPage"] == 3) {
            return $this->getMidBeforePageNumber();
        }
        return '&hellip;';
    }

    public function getMidAfterPageNumber()
    {
        return floor(($this->result->lastPage() - $this->endPage) / 2) + $this->endPage;
    }

    public function getMidAfterPageLink()
    {
        $intMidPage = $this->getMidAfterPageNumber();
        return $this->makeUrl($intMidPage);
    }

    public function getMidAfterPageText()
    {
        if ($this->endPage == ($this->result->lastPage() - 2)) {
            return $this->getMidAfterPageNumber();
        }
        return '&hellip;';
    }

    public function needShowNav()
    {
        return $this->result->hasPages();
    }

    public function needShowPrevPage()
    {
        return $this->result->currentPage() > 1;
    }

    public function needShowNextPage()
    {
        return $this->result->currentPage() < $this->result->lastPage();
    }

    public function needShowFirstPage()
    {
        return $this->startPage > 1;
    }

    public function needShowLastPage()
    {
        return $this->endPage < $this->result->lastPage();
    }

    public function needShowMidBeforePage()
    {
        return $this->startPage > 2;
    }

    public function needShowMidAfterPage()
    {
        return $this->endPage < ($this->result->lastPage() - 1);
    }

    public function getStartPageNumber()
    {
        return $this->startPage;
    }

    public function getEndPageNumber()
    {
        return $this->endPage;
    }

    public function getCurrentPageNumber()
    {
        return $this->result->currentPage();
    }

    public function getPageSize()
    {
        return $this->result->perPage();
    }

    public function getTotalCount()
    {
        return $this->result->total();
    }

    public function getLastShownRecordNumber()
    {
        return $this->result->lastItem();
    }
}
