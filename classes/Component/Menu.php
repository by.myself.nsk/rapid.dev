<?php

namespace Rapid\Dev\Component;

use CMain;
use CMenu;
use Rapid\Dev\Helper\Cache;
use Rapid\Dev\Provider\Monolog\Monolog;

class Menu
{
    protected static $templateFile = '/bitrix/components/bitrix/menu/stub.php';

    protected $curDir;
    protected $useExt = true;
    protected $allowMultiSelect = false;
    protected $bCacheSelected = true;
    protected $parentMenuType = 'top';
    protected $childMenuType = 'left';
    protected $depthLevel = 1;

    protected $arResult;
    protected $menu;
    protected $menuDir;
    protected $menuType;

    /**
     * @param bool $useExt
     * @return Menu
     */
    public function setUseExt(bool $useExt): Menu
    {
        $this->useExt = $useExt;
        return $this;
    }

    /**
     * @param bool $allowMultiSelect
     * @return Menu
     */
    public function setAllowMultiSelect(bool $allowMultiSelect): Menu
    {
        $this->allowMultiSelect = $allowMultiSelect;
        return $this;
    }

    /**
     * @param bool $bCacheSelected
     * @return Menu
     */
    public function setCacheSelected(bool $bCacheSelected): Menu
    {
        $this->bCacheSelected = $bCacheSelected;
        return $this;
    }

    /**
     * @param string $parentMenuType
     * @return Menu
     */
    public function setParentMenuType(string $parentMenuType): Menu
    {
        $this->parentMenuType = $parentMenuType;
        return $this;
    }

    /**
     * @param string $childMenuType
     * @return Menu
     */
    public function setChildMenuType(string $childMenuType): Menu
    {
        $this->childMenuType = $childMenuType;
        return $this;
    }

    /**
     * @param int $depthLevel
     * @return Menu
     */
    public function setDepthLevel(int $depthLevel): Menu
    {
        $this->depthLevel = $depthLevel;
        return $this;
    }

    public function __construct()
    {
        global $APPLICATION;
        $this->curDir = $APPLICATION->GetCurDir();
    }

    public function get()
    {
        // в этом методе соберем всю необходимую логику из дефолтного компонента (выкинем лишнее)
        // + дополним своей логикой (раскладывание по иерархии и задание структуры элемента)
        $arResult = &$this->arResult;
        $this->getCachedData();

        if (!$this->bCacheSelected) {
            $this->setSelectedItems();
        }

        $arResult = self::makeHierarchicalMenu($arResult);
        return $arResult;
    }

    protected function getCachedData()
    {
        $getData = function(){
            global $CACHE_MANAGER;
            if (defined("BX_COMP_MANAGED_CACHE")) {
                $CACHE_MANAGER->registerTag("bitrix:menu");
            }
            $this->menu = new CMenu($this->parentMenuType);
            $menu = &$this->menu;
            $menu->Init($this->curDir, $this->useExt, self::$templateFile);
            $menu->RecalcMenu($this->allowMultiSelect, $this->bCacheSelected);

            $this->arResult = [];
            $arResult = &$this->arResult;

            if ($this->depthLevel > 1) {
                self::getChildMenuRecursive(
                    $menu->arMenu,
                    $currentLevel = 1,
                    false
                );

                $this->menuDir = $menu->MenuDir;
                $this->menuType = $menu->type;
            } else {
                $arResult = $menu->arMenu;
                $this->menuDir = $menu->MenuDir;
                $this->menuType = $menu->type;

                for ($menuIndex = 0, $menuCount = count($menu->arMenu); $menuIndex < $menuCount; $menuIndex++) {
                    //Menu from iblock (bitrix:menu.sections)
                    if (is_array($arResult[$menuIndex]["PARAMS"]) && isset($arResult[$menuIndex]["PARAMS"]["FROM_IBLOCK"])) {
                        $arResult[$menuIndex]["DEPTH_LEVEL"] = $arResult[$menuIndex]["PARAMS"]["DEPTH_LEVEL"];
                        $arResult[$menuIndex]["IS_PARENT"] = $arResult[$menuIndex]["PARAMS"]["IS_PARENT"];
                    } else {
                        //Menu from files
                        $arResult[$menuIndex]["DEPTH_LEVEL"] = 1;
                        $arResult[$menuIndex]["IS_PARENT"] = false;
                    }
                }
            }
            unset($menu->arMenu);

            global $APPLICATION;
            return [
                'debug' => [
                    'GetCurDir' => $APPLICATION->GetCurDir(),
                    'SITE_ID' => SITE_ID,
                    'LANGUAGE_ID' => LANGUAGE_ID,
                ],
                'menu' => $this->menu,
                'depthLevel' => $this->depthLevel,
                'menuDir' => $this->menuDir,
                'menuType' => $this->menuType,
                'arResult' => $this->arResult,
            ];
        };
        $obCache = new Cache(__METHOD__, $this->getCacheKeys());
//        $arCachedData = $obCache->get($getData);
        $arCachedData = $getData();

        $this->menu = $arCachedData['menu'];
        $this->depthLevel = $arCachedData['depthLevel'];
        $this->menuDir = $arCachedData['menuDir'];
        $this->menuType = $arCachedData['menuType'];
        $this->arResult = $arCachedData['arResult'];

    }

    protected function getCacheKeys()
    {
        $arKeys = [
            $this->curDir,
            $this->useExt,
            $this->allowMultiSelect,
            $this->bCacheSelected,
            $this->parentMenuType,
            $this->childMenuType,
            $this->depthLevel,
            LANGUAGE_ID,
            SITE_ID,
        ];
        if ($this->bCacheSelected) {
            global $APPLICATION;
            $arKeys[] = $APPLICATION->GetCurPage();
        }

//        if ($_REQUEST['debug_menu']) {
//            $log = Monolog::getInstance();
//            $log->debug('$arKeys', $arKeys);
//        }

        return $arKeys;
    }

    /**
     * Got from bitrix:menu
     * @param $arMenu
     * @param $currentLevel
     * @param $parentItem
     */
    protected function getChildMenuRecursive(&$arMenu, $currentLevel, $parentItem)
    {
        if ($currentLevel > $this->depthLevel) {
            return;
        }

        $arResult = &$this->arResult;

        for ($menuIndex = 0, $menuCount = count($arMenu); $menuIndex < $menuCount; $menuIndex++) {
            //Menu from iblock (bitrix:menu.sections)
            $arMenu[$menuIndex]["CHAIN"] = (is_array($parentItem) && !empty($parentItem["CHAIN"]) ? $parentItem["CHAIN"] : array());
            $arMenu[$menuIndex]["CHAIN"][] = $arMenu[$menuIndex]["TEXT"];

            if (is_array($arMenu[$menuIndex]["PARAMS"]) && isset($arMenu[$menuIndex]["PARAMS"]["FROM_IBLOCK"])) {
                $iblockSectionLevel = intval($arMenu[$menuIndex]["PARAMS"]["DEPTH_LEVEL"]);
                if ($currentLevel > 1) {
                    $iblockSectionLevel = $iblockSectionLevel + $currentLevel - 1;
                }

                $arResult[] = $arMenu[$menuIndex] + Array("DEPTH_LEVEL" => $iblockSectionLevel, "IS_PARENT" => $arMenu[$menuIndex]["PARAMS"]["IS_PARENT"]);
                continue;
            }

            //Menu from files
            $subMenuExists = false;
            if ($currentLevel < $this->depthLevel) {
                //directory link only
                $bDir = false;
                if (!preg_match("'^(([a-z]+://)|mailto:|javascript:)'i", $arMenu[$menuIndex]["LINK"])) {
                    if (substr($arMenu[$menuIndex]["LINK"], -1) == "/") {
                        $bDir = true;
                    }
                }
                if ($bDir) {
                    $menu = new CMenu($this->childMenuType);
                    $menu->disableDebug();
                    $success = $menu->Init($arMenu[$menuIndex]["LINK"], $this->useExt, self::$templateFile, true);
                    $subMenuExists = ($success && count($menu->arMenu) > 0);

                    if ($subMenuExists) {
                        $menu->RecalcMenu($this->allowMultiSelect, $this->bCacheSelected);

                        $arResult[] = $arMenu[$menuIndex] + Array("DEPTH_LEVEL" => $currentLevel, "IS_PARENT" => (count($menu->arMenu) > 0));

                        if ($arMenu[$menuIndex]["SELECTED"]) {
                            $this->menuType = $this->childMenuType;
                            $this->menuDir = $arMenu[$menuIndex]["LINK"];
                        }

                        if (count($menu->arMenu) > 0) {
                            self::GetChildMenuRecursive($menu->arMenu,$currentLevel + 1, $arMenu[$menuIndex]);
                        }
                    }
                }
            }

            if (!$subMenuExists) {
                $arResult[] = $arMenu[$menuIndex] + Array("DEPTH_LEVEL" => $currentLevel, "IS_PARENT" => false);
            }
        }
    }

    protected function setSelectedItems()
    {
        /** @global CMain $APPLICATION */
        global $APPLICATION;
        $cur_page = $APPLICATION->GetCurPage(true);
        $cur_page_no_index = $APPLICATION->GetCurPage(false);
        $cur_selected = -1;
        $cur_selected_len = -1;

        foreach ($this->arResult as $iMenuItem => $MenuItem) {
            $LINK = $MenuItem['LINK'];
            $ADDITIONAL_LINKS = $MenuItem['ADDITIONAL_LINKS'];
            $SELECTED = false;

            $all_links = array();
            if (is_array($ADDITIONAL_LINKS)) {
                foreach ($ADDITIONAL_LINKS as $link) {
                    $tested_link = trim($link);
                    if (strlen($tested_link) > 0) {
                        $all_links[] = $tested_link;
                    }
                }
            }
            $all_links[] = $LINK;

            if ($MenuItem['PERMISSION'] != 'Z') {
                foreach ($all_links as $tested_link) {
                    if ($tested_link == '') {
                        continue;
                    }

                    $SELECTED = CMenu::IsItemSelected($tested_link, $cur_page, $cur_page_no_index);
                    if ($SELECTED) {
                        $this->arResult[$iMenuItem]['SELECTED'] = true;
                        break;
                    }
                }
            }

            if ($SELECTED && !$this->allowMultiSelect) {
                /** @noinspection PhpUndefinedVariableInspection */
                $new_len = strlen($tested_link);
                if ($new_len > $cur_selected_len) {
                    if ($cur_selected !== -1) {
                        $this->arResult[$cur_selected]['SELECTED'] = false;
                    }

                    $cur_selected = $iMenuItem;
                    $cur_selected_len = $new_len;
                } elseif ($tested_link !== SITE_DIR) {
                    $this->arResult[$iMenuItem]['SELECTED'] = false;
                }
            }
        }
    }

    /**
     * Группирует элементы меню из линейного массива в дерево
     * @param array $arMenuItems
     * @return array
     */
    public static function makeHierarchicalMenu($arMenuItems)
    {
        if (!is_array($arMenuItems)) {
            return array();
        }
        $arResult = array();
        $arCurParent = &$arResult;
        $arLastLinks = array();
        $intLastLevel = 0;
        foreach ($arMenuItems as &$arMenuItem) {
            $intLevel = (int)$arMenuItem['DEPTH_LEVEL'];
            if ($intLevel > $intLastLevel) {
                if ($intLastLevel) {
                    end($arLastLinks[$intLastLevel]);
                    $intLastKey = key($arLastLinks[$intLastLevel]);
                    $arCurParent = &$arLastLinks[$intLastLevel][$intLastKey]['ITEMS'];
                } else {
                    $arCurParent = array();
                }
            } elseif ($intLevel < $intLastLevel) {
                $arCurParent = &$arLastLinks[$intLevel];
            }
            $arCurParent[] = &$arMenuItem;
            $intLastLevel = $intLevel;
            $arLastLinks[$intLevel] = &$arCurParent;
        }
        unset($arMenuItem, $arCurParent, $arLastLinks);
        return $arResult;
    }
}
