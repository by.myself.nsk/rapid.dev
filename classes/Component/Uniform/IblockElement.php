<?php

namespace Rapid\Dev\Component\Uniform;

use CFile;
use CIBlockElement;
use CIBlockProperty;
use Exception;
use Rapid\Dev\Enum\Enum;
use Rapid\Dev\Enum\Listing;
use Rapid\Dev\Enum\ObjectField\Property;
use Rapid\Dev\Helper\MyString;

class IblockElement extends Iblock
{
    protected function setItemType()
    {
        $this->arResult['ITEM_TYPE'] = self::ITEM_TYPE_ELEMENT;
    }

    // TODO разбить на несколько более простых методов
    public function getFiles()
    {
        $arResult = &$this->arResult;
        $arFiles = [];
        if (!is_array($_FILES) || empty($_FILES)) {
            return;
        }
        foreach (['F', 'P'] as $strType) {
//            $arFiles[$strType] = File::getPostFiles($_FILES[$strType]);
            if (!array_key_exists($strType, $_FILES) || !is_array($_FILES[$strType])) {
                continue;
            }
            CFile::ConvertFilesToPost($_FILES[$strType], $arFiles[$strType]);
        }

        foreach (array('P', 'F') as $strInputType) {
            if (!is_array($arFiles[$strInputType]) || empty($arFiles[$strInputType])) {
                continue;
            }
            foreach ($arFiles[$strInputType] as $strPropertyCode => $arFile) {
                $strPropertyName = $strPropertyCode;
                if (!is_array($arResult['VALUES'][$strInputType][$strPropertyName])) {
                    $arResult['VALUES'][$strInputType][$strPropertyName] = array();
                }
                //----------------------------------------------------------
                // обработка старых файлов
                $arOldIds = $arResult['VALUES'][$strInputType][$strPropertyName];
                $arNewIds = $_POST[$strInputType][$strPropertyCode];
                if (!is_array($arOldIds)) {
                    $arOldIds = array();
                }
                if (!is_array($arNewIds)) {
                    $arNewIds = array();
                }
                $arAllIds = array_unique(array_merge($arOldIds, $arNewIds));
                foreach ($arAllIds as $intFileId) {
                    $arNewFile = CFile::MakeFileArray($intFileId);
                    if (!in_array($intFileId, $arNewIds)) {
                        $arNewFile['del'] = 'Y';
                    }
                    $arResult['VALUES'][$strInputType][$strPropertyName][] = $arNewFile;
                }
                //----------------------------------------------------------
                // обработка новых файлов
                $arResult['VALUES'][$strInputType][$strPropertyName] = array_merge($arResult['VALUES'][$strInputType][$strPropertyName], $arFile);
            }
        }
    }

    public function prepareProperties()
    {
        $arParams = &$this->arParams;
        $arResultProperties = &$this->arResult['FIELDS']['P'];
        // получаем список выбранных свойств, которые потом будем выводить на форму
        //https://dev.1c-bitrix.ru/api_help/iblock/classes/ciblockproperty/getlist.php
        //Ни один параметр фильтра не принимает массивы
        $dbProperties = CIBlockProperty::GetList(
            array(
                "SORT" => "ASC",
                "NAME" => "ASC"
            ),
            array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => $arParams['IBLOCK_ID']
            )
        );
        // оставляем в результирующем массиве только те значения, которые были указаны в Params
        while ($arProperty = $dbProperties->Fetch()) {
            $strPropertyCode = $arProperty['CODE'];
            if (!in_array($strPropertyCode, $arParams['PROPERTY_CODE'])) {
                continue;
            }
            if (Property::canRepresentArray($arProperty)) {
                try {
                    $arProperty['VALUES'] = Listing::createFromProperty($arProperty);
                } catch (Exception $e) {
                    // $e->getMessage();
                    $arProperty['VALUES'] = [];
                }
            }
            $arResultProperties[$strPropertyCode] = $arProperty;
        }
    }

    public function getItemData()
    {
        $arParams = &$this->arParams;
        $arResult = &$this->arResult;

        $arFilter = array(
            'IBLOCK_ID' => $arParams['IBLOCK_ID'],
        );
        if ($arParams['IBLOCK_SECTION_ID']) {
            $arFilter['SECTION_ID'] = $arParams['IBLOCK_SECTION_ID'];
        }
        if ($arParams['ITEM_ID']) {
            $arFilter['ID'] = $arParams['ITEM_ID'];
        }
        if ($arParams['ITEM_CODE']) {
            $arFilter['CODE'] = $arParams['ITEM_CODE'];
        }
        if (!$arFilter['ID'] && !$arFilter['CODE']) {
            $arFilter['ID'] = false;
        }

        $obItem = CIBlockElement::GetList(
            array(),
            $arFilter,
            false,
            false,
            array_merge(
                array(
                    'IBLOCK_ID',
                    'IBLOCK_TYPE_ID',
                    'ID',
                    'CREATED_BY'
                ),
                $arParams['FIELD_CODE']
            )
        )->GetNextElement();
        if ($obItem) {
            // разложить данные элемента по инпутам
            $arResult['ITEM'] = $obItem->GetFields();
            $arResult['VALUES']['F'] = array_intersect_key($arResult['ITEM'], array_flip($arParams['FIELD_CODE']));
            // фильтр не работает
            $arResult['ITEM']['PROPERTIES'] = $obItem->GetProperties();
            foreach ($arParams['PROPERTY_CODE'] as $strProp) {
                switch ($arResult['ITEM']['PROPERTIES'][$strProp]['PROPERTY_TYPE']) {
                    case 'L':
                        $arResult['VALUES']['P'][$strProp] = $arResult['ITEM']['PROPERTIES'][$strProp]['VALUE_ENUM_ID'];
                        break;
                    case 'F':
                        $arResult['VALUES']['P'][$strProp] = $arResult['ITEM']['PROPERTIES'][$strProp]['~VALUE'];
                        if ($arResult['ITEM']['PROPERTIES'][$strProp]['MULTIPLE'] != 'Y') {
                            $arResult['VALUES']['P'][$strProp] = array($arResult['VALUES']['P'][$strProp]);
                        }
                        if (is_array($arResult['VALUES']['P'][$strProp]) && !empty($arResult['VALUES']['P'][$strProp])) {
                            foreach ($arResult['VALUES']['P'][$strProp] as &$val) {
                                $val = CFile::GetFileArray($val);
                            }
                            unset($val);
                        }
                        break;
                    default:
                        $arResult['VALUES']['P'][$strProp] = $arResult['ITEM']['PROPERTIES'][$strProp]['~VALUE'];
                }
            }
        }
    }

    public function checkPostData()
    {
        $arResult = &$this->arResult;
        foreach (array('F', 'P') as $strInputType) {
            foreach ($arResult['FIELDS'][$strInputType] as $strInputName => &$arResultInput) {
                if ($arResultInput['IS_REQUIRED'] != 'Y') {
                    continue;
                }
                if ($arResultInput['MULTIPLE'] != 'Y') {
                    if ($arResultInput['PROPERTY_TYPE'] != 'F') {
                        if (empty($arResult['VALUES'][$strInputType][$strInputName])) {
                            $this->setInputError($strInputType, $strInputName, GetMessage('RPDUF_MSG_FILL_IN'));
                        }
                    } else {
                        switch ($arResult['VALUES'][$strInputType][$strInputName]['error']) {
                            default:
                                $this->setInputError($strInputType, $strInputName, GetMessage('RPDUF_MSG_SELECT_FILE'));
                                break;
                        }
                    }
                } else {
                    //MULTIPLE == Y
                    // TODO проверить правильность
                    if ($arResultInput['PROPERTY_TYPE'] != 'F') {
                        if (empty($arResult['VALUES'][$strInputType][$strInputName]) || !count(array_diff($arResult['VALUES'][$strInputType][$strInputName], ['']))) {
                            $this->setInputError($strInputType, $strInputName, GetMessage('RPDUF_MSG_FILL_IN'));
                        }
                    } else {
                        $bEmpty = true;
                        foreach ($arResult['VALUES'][$strInputType][$strInputName] as $arFile) {
                            if ($arFile['error'] == '0') {
                                $bEmpty = false;
                                break;
                            }
                        }
                        if ($bEmpty) {
                            $this->setInputError($strInputType, $strInputName, GetMessage('RPDUF_MSG_SELECT_FILE'));
                        }
                    }
                }
            }
            unset($arResultInput);
        }
    }

    public function preparePropertiesForMail()
    {
        $arResult = &$this->arResult;
        if (empty($arResult['VALUES']['P'])) {
            return;
        }
        foreach ($arResult['VALUES']['P'] as $strPropertyCode => &$value) {
            $arResProperty = &$arResult['FIELDS']['P'][$strPropertyCode];
            if (Property::canRepresentArray($arResProperty)) {
                $convertItem = function($arResProperty, $value){
                    $values = $arResProperty['VALUES'];
                    if ($arResProperty['PROPERTY_TYPE'] == 'N' && $arResProperty['USER_TYPE'] == 'Checkbox') {
                        $value = $value ? 'Да' : 'Нет';
                    } else {
                        if ($values instanceof \Rapid\Dev\Enum\Listing) {
                            $values = $values->getList();
                        }
                        if (is_array($values)) {
                            $obEnum = &$values[$value];
                            if ($obEnum instanceof Enum) {
                                $value = $obEnum->getValue();
                            }
                        }
                    }
                    return $value;
                };

                /** @var Enum $obEnum */
                if ($arResProperty['MULTIPLE'] != 'Y') {
                    $value = $convertItem($arResProperty, $value);
                } elseif (is_array($value)) {
                    foreach ($value as &$valueCase) {
                        $valueCase = $convertItem($arResProperty, $valueCase);
                    }
                    unset($valueCase);
                }
            } elseif (MyString::isPropertyTextHtml($arResProperty)) {
                $value = MyString::getDisplayPropertyTextHtml($value);
            }
        }
        unset($value);
    }

    protected function getEditUrl()
    {
        $arItem = &$this->arResult['ITEM'];
        return '/bitrix/admin/iblock_element_edit.php?IBLOCK_ID='.$arItem['IBLOCK_ID'].'&type='.$arItem['IBLOCK_TYPE_ID'].'&ID='.$arItem['ID'].'&find_section_section='.$arItem['IBLOCK_SECTION_ID'];
    }

    public function addUpdate()
    {
        $arParams = &$this->arParams;
        $arResult = &$this->arResult;
        if ($arResult['JSON']['info']['error'] == true) {
            return false;
        }
        $arElementFields = array_merge(
            array(
                'IBLOCK_SECTION_ID' => $arParams['IBLOCK_SECTION_ID'] ?: '',
            ),
            $arResult['VALUES']['F'],
            array(
                'IBLOCK_ID' => $arParams['IBLOCK_ID'],
            )
        );
        $arResult['MAIL_FILES'] = array();
        $obIBlockElement = new CIBlockElement();
        if ($arResult['IS_EDIT']) {// ветка Update
            global $DB;
            $DB->StartTransaction();
            if ($bSuccess = $obIBlockElement->Update($arResult['ITEM']['ID'], $arElementFields)) {
                $obIBlockElement::SetPropertyValuesEx(
                    $arResult['ITEM']['ID'],
                    $arParams['IBLOCK_ID'],
                    $arResult['VALUES']['P']
                );
                $DB->Commit();
                $arResult['JSON']['id'] = $arResult['ITEM']['ID'];
                $arResult['JSON']['info']['messages'][] = GetMessage('RPDUF_MSG_ELEMENT_UPDATED');
                $this->getItemData();
                $this->checkpoint('after'.$this->getItemType().'Update');// "обработчик" после обновления элемента
            } else {
                $DB->Rollback();
                $this->setInfoError($this->getDbErrors($obIBlockElement->LAST_ERROR));
            }
        } else {// ветка Add
            if (empty($arElementFields['NAME'])) {
                $arElementFields['NAME'] = $arParams['ELEMENT_NAME_TEMPLATE'];
            }
            if (!$arElementFields["DATE_ACTIVE_FROM"]) {
                $arElementFields["DATE_ACTIVE_FROM"] = ConvertTimeStamp(time(), "FULL");
            }
            $arElementFields['ACTIVE'] = $arParams['IS_ACTIVE'] ? 'Y' : 'N';
            $arElementFields['PROPERTY_VALUES'] = $arResult['VALUES']['P'];
            if ($intItemId = $obIBlockElement->Add($arElementFields)) {
                // TODO: завести единое protected-поле, пользоваться им везде
                $arResult['JSON']['id'] = $intItemId;
                $arResult['JSON']['info']['messages'][] = GetMessage('RPDUF_MSG_ELEMENT_ADDED');
                $arParams['ITEM_ID'] = $intItemId;
                $this->getItemData();
                $this->checkpoint('after'.$this->getItemType().'Add');// "обработчик" после добавления элемента
            } else {
                $this->setInfoError($this->getDbErrors($obIBlockElement->LAST_ERROR));
            }
            $bSuccess = intval($arResult['ITEM']['ID']) > 0;
        }

        if ($arParams['SEND_MAIL'] && $bSuccess) {
            $this->preparePropertiesForMail();
            $arResult['MAIL_FIELDS'] = array_merge($arResult['VALUES']['F'], $arResult['VALUES']['P']);
            $arResult['MAIL_FIELDS']['ITEM_ID'] = $arResult['ITEM']['ID'];
            $arResult['MAIL_FIELDS']['EDIT_URL'] = $this->getEditUrl();
            $this->fillEmptyMailFields();
            $this->checkpoint('beforeMailSend');// "обработчик" перед отправкой почты
            $strMailType = $arResult['IS_EDIT'] ? $arParams['EDIT_MAIL_TYPE'] : $arParams['ADD_MAIL_TYPE'];
            $this->sendMail($arParams['SEND_IMMEDIATE'], $strMailType, $arResult['MAIL_FIELDS'], $arParams['DUPLICATE_MAIL'], $arResult['MAIL_FILES']);
        }
        return true;
    }

    public function delete()
    {
        $arResult = &$this->arResult;
        if ($arResult['JSON']['info']['error'] == true) {
            return false;
        }
        $obIBlockElement = new CIBlockElement();
        if ($bSuccess = $obIBlockElement->Delete($arResult['ITEM']['ID'])) {
            $arResult['ITEM'] = [];
            $strCheckpointName = 'after'.$this->getItemType().'Delete';
            $this->checkpoint($strCheckpointName);
        } else {
            $this->setInfoError($this->getDbErrors($obIBlockElement->LAST_ERROR));
        }
        return $bSuccess;
    }

    public function hasFileInputs()
    {
        $arResult = &$this->arResult;
        if (empty($arResult['FIELDS']['P'])) {
            return false;
        }
        foreach ($arResult['FIELDS']['P'] as &$arProperty) {
            if ($arProperty['PROPERTY_TYPE'] == 'F') {
                return true;
            }
        }
        unset($arProperty);
        return false;
    }
}
