<?php

namespace Rapid\Dev\Component\Uniform;

use CIBlock;
use CModule;
use CTimeZone;
use Exception;
use Rapid\Dev\Helper\Cache;
use Rapid\Dev\Helper\MyString;
use Rapid\Dev\Helper\SitePage;

abstract class Iblock extends \Rapid\Dev\Component\Uniform\Uniform
{
    public function prepareComponentParams(&$arParams)
    {
        self::prepareParam($arParams["IBLOCK_ID"], 'int');
        self::prepareParam($arParams["IBLOCK_CODE"], 'string');
        self::prepareParam($arParams["IBLOCK_SECTION_ID"], 'int');
        self::prepareParam($arParams["ENABLE_EDIT"], 'bool');
        self::prepareParam($arParams["ITEM_ID"], 'int');
        self::prepareParam($arParams["ITEM_CODE"], 'string');
        self::prepareParam($arParams["FIELD_CODE"], 'array');
        $arParams['FIELD_CODE'] = array_unique(array_merge($arParams['FIELD_CODE'], ['CREATED_DATE', 'DATE_CREATE', 'DATE_CREATE_UNIX', 'TIMESTAMP_X', 'TIMESTAMP_X_UNIX']));
        self::prepareParam($arParams["PROPERTY_CODE"], 'array');
        $time = time();
        self::prepareParam($arParams["ELEMENT_NAME_TEMPLATE"], 'string');
        $arParams['ELEMENT_NAME_TEMPLATE'] = str_replace(
            array('#DATE#'),
            array(gmdate('d.m.Y', $time).' ('.gmdate('H:i:s', $time).')'),
            $arParams['ELEMENT_NAME_TEMPLATE']
        );
        self::prepareParam($arParams["IS_ACTIVE"], 'bool');
        self::prepareParam($arParams["ADD_MAIL_TYPE"], 'string');
        self::prepareParam($arParams["EDIT_MAIL_TYPE"], 'string');
    }

    /**
     * В $arResult['VALUES'] складывает данные из массива $_POST
     */
    public function getPostData()
    {
        $arResult = &$this->arResult;
        foreach (['F', 'P'] as $strInputType) {
            foreach ($_POST[$strInputType] as $strPropKey => $strPropValue) {
                if ($strInputType == 'P') {
                    if ($this->isTextHtmlProperty($strPropKey)) {
                        $strPropValue = MyString::getPropertyTextHtmlArray($strPropValue);
                    }
                }
                $arResult['VALUES'][$strInputType][$strPropKey] = $strPropValue;
            }
        }
    }

    public function restorePostDataFormat()
    {
        $arResult = &$this->arResult;
        foreach (['F', 'P'] as $strInputType) {
            foreach ($_POST[$strInputType] as $strPropKey => $strPropValue) {
                if ($strInputType == 'P') {
                    if ($this->isTextHtmlProperty($strPropKey)) {
                        if (is_array($strPropValue) && isset($strPropValue['VALUE'])) {
                            $strPropValue = $strPropValue['VALUE'];
                        }
                    }
                }
                $arResult['VALUES'][$strInputType][$strPropKey] = $strPropValue;
            }
        }
    }

    protected function isTextHtmlProperty($strCode)
    {
        $arResult = &$this->arResult;
        $strType = 'P';
        return !empty($arResult['FIELDS'][$strType][$strCode]['USER_TYPE']) && $arResult['FIELDS'][$strType][$strCode]['USER_TYPE'] == 'HTML';
    }

    public function setInputError($strInputType, $strInputName, $strMessage)
    {
        $arResult = $this->arResult;
        $arResultInput = $arResult['FIELDS'][$strInputType][$strInputName];

        $arResultInput['ERROR'] = true;
        $arAnswer = &$this->arResult['JSON'];
        $strInputName = $strInputType.'['.$strInputName.']'.($arResultInput['MULTIPLE'] == 'Y' ? '[]' : '');
        $arAnswer['inputs'][$strInputName]['error'] = true;
        $arAnswer['inputs'][$strInputName]['message'] = $strMessage;
        $arAnswer['info']['error'] = true;
    }

    protected function makeCacheId()
    {
        if (!method_exists($this, 'getSiteId') || !$this->getSiteId()) {
            $SITE_ID = SITE_ID;
        } else {
            $SITE_ID = $this->getSiteId();
        }
        if (!method_exists($this, 'getLanguageId') || !$this->getLanguageId()) {
            $LANGUAGE_ID = LANGUAGE_ID;
        } else {
            $LANGUAGE_ID = $this->getLanguageId();
        }
        if (!method_exists($this, 'getSiteTemplateId') || !$this->getSiteTemplateId()) {
            $SITE_TEMPLATE_ID = (defined("SITE_TEMPLATE_ID") ? SITE_TEMPLATE_ID : "");
        } else {
            $SITE_TEMPLATE_ID = $this->getSiteTemplateId();
        }
        $cacheID = $SITE_ID."|".$LANGUAGE_ID.($SITE_TEMPLATE_ID != "" ? "|".$SITE_TEMPLATE_ID : "")."|".$this->__name."|".$this->getTemplateName()."|";

        $arParams = array_intersect_key($this->arParams, array_flip(array(
            'IBLOCK_ID',
            'IBLOCK_SECTION_ID',
            'ITEM_ID',
            'ITEM_CODE',
            'FIELD_CODE',
            'PROPERTY_CODE',
            'ENABLE_EDIT',
        )));

        foreach ($arParams as $k => $v) {
            if (strncmp("~", $k, 1)) {
                $cacheID .= ",".$k."=".serialize($v);
            }
        }
        if (($offset = CTimeZone::GetOffset()) <> 0) {
            $cacheID .= "|".$offset;
        }

        return $cacheID;
    }

    public function prepareFields()
    {
        $arParams = &$this->arParams;
        $arResultFields = &$this->arResult['FIELDS']['F'];
        $arFields = CIBlock::GetFields($arParams["IBLOCK_ID"]);
        $bSection = $this->arResult['ITEM_TYPE'] == 'section';
        foreach ($arFields as $strPropertyCode => &$arField) {
            if ($bSection) {
                // TODO уточнить, нужно ли для элементов делать обратное условие
                if (strpos($strPropertyCode, 'SECTION_') === false) {
                    continue;
                }
                $strPropertyCode = str_replace('SECTION_', '', $strPropertyCode);
            }
            if (!in_array($strPropertyCode, $arParams["FIELD_CODE"])) {
                continue;
            }
            $arResultFields[$strPropertyCode] = $arField;
            $arResultFields[$strPropertyCode]['ID'] = $strPropertyCode;
            $arResultFields[$strPropertyCode]['CODE'] = $strPropertyCode;
        }
        unset($arField);
    }

    abstract public function prepareProperties();

    abstract public function getItemData();

    /**
     * Проверяет корректность указанных данных (заполненность необходимых полей, размер файлов и т.д.)
     * При возникновении ошибки заполняет
     */
    abstract public function checkPostData();

    abstract public function addUpdate();

    abstract public function delete();

    public function executeComponent()
    {
        $arParams = &$this->arParams;
        $arResult = &$this->arResult;
        $this->setItemType();
        try {
            $arResult['COMPONENT_RETURN'] = [
                'CAN_SHOW' => true,
            ];
            if (!CModule::IncludeModule("iblock")) {
                throw new Exception('Модуль инфоблоков не установлен');
            }
            if ($arParams['IBLOCK_CODE']) {
                $this->setIblockId(\Rapid\Dev\Helper\Iblock\Iblock::get($arParams['IBLOCK_CODE']));
            } else {
                $this->setIblockId($arParams['IBLOCK_ID']);
            }
            if (!$this->checkIblockId()) {
                throw new Exception('Инфоблок не найден');
            }
            $obCache = new Cache(__METHOD__, $this->makeCacheId());
            $arCacheData = $obCache->get(function() use (&$arResult, &$arParams) {
                $this->prepareFields();
                $this->prepareProperties();
                // условие удалять нельзя, т.к. в противном случае может быть проблема с безопасностью
                if ($arParams['ENABLE_EDIT']) {
                    $this->getItemData();
                }
                global $CACHE_MANAGER;
                $CACHE_MANAGER->RegisterTag('iblock_id_'.$this->getIblockId());
                return [
                    'FIELDS' => $arResult['FIELDS'],
                    'VALUES' => $arResult['VALUES'],
                    'ITEM' => $arResult['ITEM'],
                ];
            });
            $arResult['FIELDS'] = &$arCacheData['FIELDS'];
            $arResult['VALUES'] = &$arCacheData['VALUES'];
            $arResult['ITEM'] = &$arCacheData['ITEM'];
            $arResult['IS_EDIT'] = $this->isEdit();

            $this->checkPrepareCaptcha();
            $this->prepareFormAction();
            $this->makeFormAttr();
            if ($this->isAjax(false)) {
                if (!$this->isAjax(['load', 'copy'])) {
                    $this->getPostData();
                    $this->getFiles();
                }
                $this->checkBot();
                global $APPLICATION;
                $APPLICATION->RestartBuffer();
                ob_start();
            }
            if ($this->isAjax('delete') && $arParams['ENABLE_EDIT'] && $arResult['IS_EDIT']) {
                $strCheckpointName = 'before'.$this->getItemType().'Delete';
                $this->checkpoint($strCheckpointName);
                $this->delete();
            } elseif ($this->isAjax('submit')) {
                $strCheckpointName = 'before'.$this->getItemType().($arResult['IS_EDIT'] ? 'Update' : 'Add');
                $this->checkpoint($strCheckpointName);
                $this->checkPostData();
                $this->addUpdate();
                $this->restorePostDataFormat();
            }
            $this->includeComponentTemplate();
            if ($this->isAjax(false)) {
                $arResult['JSON']['template'] = ob_get_clean();
                $arResult['JSON']['isEdit'] = $arResult['IS_EDIT'];
                SitePage::sendAjaxJson($arResult['JSON']);
            }
        } catch (Exception $e) {
            ShowError($e->getMessage());
            $arResult['COMPONENT_RETURN']['CAN_SHOW'] = false;
        } finally {
            return $arResult['COMPONENT_RETURN'];
        }
    }

    protected function checkIblockId()
    {
        return CIBlock::GetByID($this->getIblockId())->SelectedRowsCount() > 0;
    }

    protected function setIblockId($iblockId)
    {
        $this->arResult['IBLOCK_ID'] = $iblockId;
    }

    protected function getIblockId()
    {
        return $this->arResult['IBLOCK_ID'];
    }

    protected function checkPrepareCaptcha()
    {
        global $APPLICATION;
        if ($this->arParams["USE_CAPTCHA"]) {
            $this->arResult["CAPTCHA_CODE"] = htmlspecialcharsbx($APPLICATION->CaptchaGetCode());
        }
    }

    protected function prepareFormAction()
    {
        global $APPLICATION;
        $this->arResult['ACTION'] = $APPLICATION->GetCurPage();
    }
}
