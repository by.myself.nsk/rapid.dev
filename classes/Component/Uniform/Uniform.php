<?php

namespace Rapid\Dev\Component\Uniform;

use CEvent;
use Rapid\Dev\Component\Component;
use Rapid\Dev\Helper\SitePage;

abstract class Uniform extends Component
{
    const ITEM_TYPE_ELEMENT = 'Element';
    const ITEM_TYPE_SECTION = 'Section';
    public $arResult;
    protected static $intUniformCounter = 0;

    abstract public function prepareComponentParams(&$arParams);

    public function onPrepareComponentParams($arParams)
    {
        $this->includeComponentLang('class.php');
        $arResult = &$this->arResult;
        self::incrementUniformCounter();
        self::prepareParam($arParams["ID_PREFIX"], 'string');
        $arParams["ID_PREFIX"] .= self::getUniformCounter();
        self::prepareParam($arParams["USE_CAPTCHA"], 'bool');
        self::prepareParam($arParams["SEND_MAIL"], 'bool');
        self::prepareParam($arParams["SEND_IMMEDIATE"], 'bool');
        self::prepareParam($arParams["DUPLICATE_MAIL"], 'bool');
        self::prepareParam($arParams["MSG_EMPTY_MAIL_FIELD"], 'string');

        $this->prepareComponentParams($arParams);

        $arResult = [
            'IBLOCK_ID' => '',
            'ITEM_TYPE' => '',
            'FIELDS' => [
                'F' => [], // FIELDS
                'P' => [], // PROPERTIES/USERFIELDS
            ],
            'VALUES' => [
                'F' => [],
                'P' => [],
            ],
            'IS_EDIT' => false,
            'ITEM' => [],
            'MAIL_FIELDS' => [],
            'JSON' => [
                'inputs' => [],
                'info' => [
                    'error' => false,
                    'messages' => [],
                ],
                'template' => ''
            ],
            'FORM_ID' => 'uniform_'.$arParams['ID_PREFIX'],
        ];
        $arResult['AJAX_ACTION_NAME'] = 'uniform_ajax'.$arParams['ID_PREFIX']; // префикс обязателен! иначе isAjax работает некорректно
        $arResult['AJAX_ACTION_VALUE'] = $this->makeAjaxActionValue();

        $arResult['IS_AJAX'] = $this->isAjax();
        return $arParams;
    }

    abstract protected function setItemType();

    protected function getItemType()
    {
        return $this->arResult['ITEM_TYPE'];
    }

    /**
     * @param false|string $strAction Если false, то проверит, что ajax пришел, и пришел именно к этой форме<br>
     * Если указана строка, то дополнительно проверит, что ajax-запрос был именно такой, как указано в параметре
     * @return bool
     */
    public function isAjax($strAction = 'submit')
    {
        $arResult = &$this->arResult;
        if ($strAction === false) {
            return SitePage::isAjax(false, $arResult['AJAX_ACTION_NAME'], true);
        }
        return SitePage::isAjax($strAction, $arResult['AJAX_ACTION_NAME'], true);
    }

    public function makeAjaxActionValue($strAction = 'submit')
    {
        return $strAction;
    }

    public static function incrementUniformCounter()
    {
        return ++self::$intUniformCounter;
    }

    public static function getUniformCounter()
    {
        return self::$intUniformCounter;
    }

    /**
     * В $this->$arResult['VALUES']['P'] складывает данные об отправленных файлах в корректном виде
     */
    abstract public function getFiles();

    public function setInfoError($message)
    {
        $arAnswer = &$this->arResult['JSON'];
        if (!is_array($message)) {
            $message = array($message);
        }
        $arAnswer['info']['error'] = true;
        $arAnswer['info']['messages'] = array_merge($arAnswer['info']['messages'], $message);
    }

    abstract public function setInputError($strInputType, $strInputName, $strMessage);

    /**
     * Распарсивает сообщение об ошибке добавления элемента в ИБ, складывая все ошибки в массив, и возвращает его.
     * @param $strLastError
     * @return array
     */
    public static function getDbErrors($strLastError)
    {
        $arTmpMsg = array_diff(preg_split('/<br(\s)*(\/)?>/i', $strLastError), ['']);
        foreach ($arTmpMsg as &$strMsg) {
            $strMsg = trim($strMsg);
        }
        unset($strMsg);
        return $arTmpMsg;
    }

    /**
     * Обертка стандартных методов отправки почты
     * @param bool $bImmediate
     * @param string $event
     * @param array $arFields
     * @param bool $bDuplicate
     * @param array $arFiles
     */
    public static function sendMail($bImmediate, $event, $arFields, $bDuplicate = true, $arFiles = array())
    {
        $strMethodName = $bImmediate ? 'SendImmediate' : 'Send';
        CEvent::$strMethodName($event, [SITE_ID], $arFields, $bDuplicate ? 'Y' : 'N', '', $arFiles);
    }

    public function checkpoint($strFilename)
    {
        if (strpos($strFilename, '.') === false) {
            $strFilename .= '.php';
        }
        $obTemplate = $this->getTemplate();
        if (!$obTemplate) {
            $this->initComponentTemplate();
            $obTemplate = $this->getTemplate();
        }
        $strEventFile = $_SERVER['DOCUMENT_ROOT'].$obTemplate->GetFolder().'/'.$strFilename;
        if (file_exists($strEventFile)) {
            /** @noinspection PhpIncludeInspection */
            include $strEventFile;
            return true;
        }
        return false;
    }

    /**
     * Заполняет пустые поля в почтовом сообщении дефолтной фразой
     * @return bool
     */
    public function fillEmptyMailFields()
    {
        $arMessageFields = &$arResult['MAIL_FIELDS'];
        if (empty($arMessageFields) || !is_array($arMessageFields)) {
            return false;
        }
        foreach ($arMessageFields as &$data) {
            if (empty($data)) {
                $data = $this->arParams['MSG_EMPTY_MAIL_FIELD'];
            }
        }
        return true;
    }

    /**
     * Проверяет, есть ли файловые инпуты на форме
     */
    abstract public function hasFileInputs();

    /**
     * Устанавливает атрибуты формы: method, id, action, enctype
     */
    public function makeFormAttr()
    {
        $arResult = &$this->arResult;
        $arResult['FORM_ATTR'] = ' method="POST" id="'.$arResult['FORM_ID'].'" action="'.$arResult['ACTION'].'"';
        if ($this->hasFileInputs()) {
            $arResult['FORM_ATTR'] .= ' enctype="multipart/form-data"';
        }
    }

    /**
     * Возвращает атрибуты формы: method, id, action, enctype
     */
    public function getFormAttr()
    {
        return $this->arResult['FORM_ATTR'];
    }

    public function checkBot()
    {
        global $APPLICATION;
        $arParams = &$this->arParams;
        if (!check_bitrix_sessid() || !empty($_REQUEST['fio']) || $arParams["USE_CAPTCHA"] && !$APPLICATION->CaptchaCheckCode($_REQUEST["CAPTCHA_WORD"], $_REQUEST["CAPTCHA_SID"])) {
            $this->setInputError('', 'CAPTCHA_WORD', GetMessage('RPDUF_MSG_WRONG_CAPTCHA'));
        }
    }

    protected function isEdit()
    {
        $arResult = &$this->arResult;
        $arParams = &$this->arParams;
        return $arParams['ENABLE_EDIT'] && intval($arResult['ITEM']['ID']);
    }

//----------------------------------------------------------------------------------------------------------------------
// методы для шаблона
    public function printHiddenInputs()
    {
        global $APPLICATION;
        /** @noinspection PhpDeprecationInspection */
        $APPLICATION->AddHeadString('<style>.uniform-req-textfield{display:none;}</style>');
        $arResult = &$this->arResult;
        echo bitrix_sessid_post();
        ?><input type="text" name="fio" value="" class="uniform-req-textfield" title="">
        <input type="hidden" name="<?=$arResult['AJAX_ACTION_NAME']?>" value="<?=$arResult['AJAX_ACTION_VALUE']?>">
        <input type="hidden" name="FORM_ID" value="<?=$arResult['FORM_ID']?>"><?
    }

    public function getErrorsFormatted()
    {
        ob_start();
        $arResult = &$this->arResult;
        if (!is_array($arResult['JSON']['info']['messages'])) {
            foreach ($arResult['JSON']['info']['messages'] as $strMessage) {
                ?><p><?=$strMessage?></p><?
            }
        }
        return ob_get_clean();
    }
}
