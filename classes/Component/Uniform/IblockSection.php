<?php

namespace Rapid\Dev\Component\Uniform;

use CAllFile;
use CFile;
use CIBlockSection;
use CUserTypeEntity;
use Rapid\Dev\Enum\Enum;
use Rapid\Dev\Enum\ObjectField\UserField;

class IblockSection extends Iblock
{
    protected function setItemType()
    {
        $this->arResult['ITEM_TYPE'] = self::ITEM_TYPE_SECTION;
    }

    public function getFiles()
    {
        $arResult = &$this->arResult;
        $arFiles = [];
        foreach (['F', 'P'] as $strType) {
//            $arFiles[$strType] = File::getPostFiles($_FILES[$strType]);
            CFile::ConvertFilesToPost($_FILES[$strType], $arFiles[$strType]);
        }

        foreach ($arFiles as $strPropertyCode => $arFile) {
            if ($arFile['error']) {
                continue;
            }
            $strPropertyName = preg_replace('/^PROPERTY_(.*)$/i','$1', $strPropertyCode);
            if (!is_array($arResult['VALUES']['P'][$strPropertyName])) {
                $arResult['VALUES']['P'][$strPropertyName] = array();
            }
            //----------------------------------------------------------
            // обработка старых файлов
            $arOldIds = $arResult['VALUES']['P'][$strPropertyName];
            $arNewIds = $_POST[$strPropertyCode];
            if (!is_array($arOldIds)) {
                $arOldIds = array();
            }
            if (!is_array($arNewIds)) {
                $arNewIds = array();
            }
            $arAllIds = array_unique(array_merge($arOldIds, $arNewIds));
            $arResult['VALUES']['P'][$strPropertyName] = array();
            foreach ($arAllIds as $intFileId) {
                // если удалять не нужно
                if (in_array($intFileId, $arNewIds)) {
                    $arResult['VALUES']['P'][$strPropertyName][] = $intFileId;
                    continue;
                }
                // если нужно удалить
                $arDbFile = CFile::MakeFileArray($intFileId);
                $arDbFile['old_id'] = $intFileId;
                $arDbFile['del'] = 'Y';
                $arResult['VALUES']['P'][$strPropertyName][] = $arDbFile;
            }
            //----------------------------------------------------------
            // обработка новых файлов
            $arResult['VALUES']['P'][$strPropertyName] = array_merge($arResult['VALUES']['P'][$strPropertyName], $arFile);
        }
    }

    public function prepareProperties()
    {
        $arParams = &$this->arParams;
        $arResultProperties = &$this->arResult['FIELDS']['P'];
        $dbProperties = CUserTypeEntity::GetList(
            array(
                'SORT' => 'ASC',
                'FIELD_NAME' => 'ASC',
            ),
            array(
                'ENTITY_ID' => 'IBLOCK_'.$arParams['IBLOCK_ID'].'_SECTION',
                'LANG' => LANGUAGE_ID,
            )
        );
        while ($arProperty = $dbProperties->Fetch()) {
            if (!in_array($arProperty['FIELD_NAME'], $arParams['PROPERTY_CODE'])) {
                continue;
            }
            $strPropertyCode = $arProperty['FIELD_NAME'];

            if (UserField::canRepresentArray($arProperty)) {
                // TODO отладить. Привести параметры конструктора к единому виду
//                $arProperty['VALUES'] = new UserField($arProperty);
            }
            $arResultProperties[$strPropertyCode] = $arProperty;
        }
    }

    public function getItemData()
    {
        $arParams = &$this->arParams;
        $arResult = &$this->arResult;
        $obItem = CIBlockSection::GetList(
            array(),
            array(
                'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                'SECTION_ID' => $arParams['IBLOCK_SECTION_ID'] ?: '',
                'ID' => $arParams['ITEM_ID'],
                'CODE' => $arParams['ITEM_CODE'],
            ),
            false,
            array_merge(
                array(
                    'IBLOCK_ID',
                    'IBLOCK_SECTION_ID',
                    'ID',
                    'CREATED_BY'
                ),
                $arParams['FIELD_CODE'],
                $arParams['PROPERTY_CODE']
            )
        )->GetNextElement();
        if ($obItem) {
            $arResult['ITEM'] = $obItem->GetFields();
            $arResult['VALUES']['F'] = array_intersect_key($arResult['ITEM'], array_flip($arParams['FIELD_CODE']));
            $arResult['VALUES']['P'] = array_intersect_key($arResult['ITEM'], array_flip($arParams['PROPERTY_CODE']));
        }
    }

    public function checkPostData()
    {
        $arResult = &$this->arResult;
        foreach (array('F', 'P') as $strInputType) {
            foreach ($arResult['FIELDS'][$strInputType] as $strInputName => &$arResultInput) {
                if ($arResultInput['IS_REQUIRED'] != 'Y') {
                    continue;
                }
                if ($arResultInput['MULTIPLE'] != 'Y') {
                    if ($arResultInput['USER_TYPE_ID'] != 'file') {
                        if (empty($arResult['VALUES'][$strInputType][$strInputName])) {
                            $this->setInputError($strInputType, $strInputName, GetMessage('RPDUFS_MSG_FILL_IN'));
                        }
                    } else {// ОДИНОЧНЫЙ ФАЙЛ
                        switch ($arResult['VALUES'][$strInputType][$strInputName]['error']) {
                            case '0':
                                break;
//                            case '1':
//                                setInputError($strInputName, '...');
//                                break;
                            default:
                                $this->setInputError($strInputType, $strInputName, GetMessage('RPDUFS_MSG_SELECT_FILE'));
                                break;
                        }
                    }
                } else {//MULTIPLE == Y
                    if ($arResultInput['USER_TYPE_ID'] != 'file') {
                        if (empty($arResult['VALUES'][$strInputType][$strInputName]) || !count(array_diff($arResult['VALUES'][$strInputType][$strInputName],
                                array("")))
                        ) {
                            $this->setInputError($strInputType, $strInputName, GetMessage('RPDUFS_MSG_FILL_IN'));
                        }
                    } else {
                        $bEmpty = true;
                        foreach ($arResult['VALUES'][$strInputType][$strInputName] as $arFile) {
                            if ($arFile['error'] == '0') {
                                $bEmpty = false;
                                break;
                            }
                        }
                        if ($bEmpty) {
                            $this->setInputError($strInputType, $strInputName, GetMessage('RPDUFS_MSG_SELECT_FILE'));
                        }
                    }
                }
            }
            unset($arResultInput);
        }
    }

    public function preparePropertiesForMail()
    {
        $arResult = &$this->arResult;
        if (!empty($arResult['VALUES']['P'])) {
            foreach ($arResult['VALUES']['P'] as $strPropertyCode => &$value) {
                $arResProperty = &$arResult['FIELDS']['P'][$strPropertyCode];

                if (UserField::canRepresentArray($arResProperty)) {
                    /** @var Enum $obEnum */
                    if ($arResProperty['MULTIPLE'] != 'Y') {
                        $obEnum = &$arResProperty['VALUES'][$value];
                        $value = $obEnum->getValue();
                    } elseif (!empty($value)) {
                        foreach ($value as &$valueCase) {
                            $obEnum = &$arResProperty['VALUES'][$valueCase];
                            $value = $obEnum->getValue();
                        }
                        unset($valueCase);
                    }
                }
                // TODO убедиться, что с TextHtml все ок
            }
            unset($value);
        }
    }

    protected function getEditUrl()
    {
        $arItem = &$this->arResult['ITEM'];
        return '/bitrix/admin/iblock_section_edit.php?IBLOCK_ID='.$arItem['IBLOCK_ID'].'&type='.$arItem['IBLOCK_TYPE'].'&ID='.$arItem['ID'].'&find_section_section='.$arItem['IBLOCK_SECTION_ID'];
    }

    public function addUpdate()
    {
        $arParams = &$this->arParams;
        $arResult = &$this->arResult;
        $arAnswer = &$this->arResult['JSON'];
        if ($arAnswer['info']['error'] == true) {
            return false;
        }
        $arElementFields = array_merge(
            array(
                'IBLOCK_SECTION_ID' => $arParams['IBLOCK_SECTION_ID'] ?: '',
            ),
            $arResult['VALUES']['F'],
            $arResult['VALUES']['P'],
            array(
                'IBLOCK_ID' => $arParams['IBLOCK_ID'],
            )
        );
        if (empty($arElementFields['NAME'])) {
            $arElementFields['NAME'] = $arParams['ELEMENT_NAME_TEMPLATE'];
        }
        if (!intval($arResult['ITEM']['ID']) && !$arElementFields["DATE_ACTIVE_FROM"]) {
            $arElementFields["DATE_ACTIVE_FROM"] = ConvertTimeStamp(time(), "FULL");
        }
        $arResult['MAIL_FILES'] = array();
        $obIBlockSection = new CIBlockSection();
        if ($arResult['IS_EDIT']) {// ветка Update
            if ($bSuccess = $obIBlockSection->Update($arResult['ITEM']['ID'], $arElementFields)) {
                $arResult['JSON']['id'] = $arResult['ITEM']['ID'];
                $arResult['JSON']['info']['messages'][] = GetMessage('RPDUFS_MSG_ELEMENT_UPDATED');
                $this->getItemData();
                $this->checkpoint('after'.$this->getItemType().'Update');// "обработчик" после обновления элемента
            } else {
                $this->setInfoError($this->getDbErrors($obIBlockSection->LAST_ERROR));
            }
        } else {// ветка Add
            $arElementFields['ACTIVE'] = $arParams['IS_ACTIVE'] ? 'Y' : 'N';
            if ($intItemId = $obIBlockSection->Add($arElementFields)) {
                $arResult['JSON']['id'] = $intItemId;
                $arResult['JSON']['info']['messages'][] = GetMessage('RPDUFS_MSG_ELEMENT_ADDED');
                $arParams['ITEM_ID'] = $intItemId;
                $this->getItemData();
                $this->checkpoint('after'.$this->getItemType().'Add');// "обработчик" после добавления элемента
            } else {// раздел инфоблока не добавлен, нужно вернуть ошибки
                $this->setInfoError($this->getDbErrors($obIBlockSection->LAST_ERROR));
            }
            $bSuccess = intval($arResult['ITEM']['ID']) > 0;
        }
        if ($arParams['SEND_MAIL'] && $bSuccess) {
            $this->preparePropertiesForMail();
            $arResult['MAIL_FIELDS'] = array_merge($arResult['VALUES']['F'], $arResult['VALUES']['P']);
            $arResult['MAIL_FIELDS']['ITEM_ID'] = $arResult['ITEM']['ID'];
            $arResult['MAIL_FIELDS']['EDIT_URL'] = $this->getEditUrl();
            $this->fillEmptyMailFields();
            $this->checkpoint('beforeMailSend');// "обработчик" перед отправкой почты
            $strMailType = $arResult['IS_EDIT'] ? $arParams['EDIT_MAIL_TYPE'] : $arParams['ADD_MAIL_TYPE'];
            $this->sendMail($arParams['SEND_IMMEDIATE'], $strMailType, $arResult['MAIL_FIELDS'], $arParams['DUPLICATE_MAIL'], $arResult['MAIL_FILES']);
        }
        return true;
    }

    public function delete()
    {
        $arResult = &$this->arResult;
        if ($arResult['JSON']['info']['error'] == true) {
            return false;
        }
        $obIBlockElement = new CIBlockSection();
        if ($bSuccess = $obIBlockElement->Delete($arResult['ITEM']['ID'])) {
            $arResult['ITEM'] = [];
            $strCheckpointName = 'after'.$this->getItemType().'Delete';
            $this->checkpoint($strCheckpointName);
        } else {
            $this->setInfoError($this->getDbErrors($obIBlockElement->LAST_ERROR));
        }
        return $bSuccess;
    }

    public function hasFileInputs()
    {
        $arResult = &$this->arResult;
        if (empty($arResult['FIELDS']['P'])) {
            return false;
        }
        foreach ($arResult['FIELDS']['P'] as &$arProperty) {
            if ($arProperty['USER_TYPE_ID'] == 'file') {
                return true;
            }
        }
        unset($arProperty);
        return false;
    }
}
