<?php

namespace Rapid\Dev\Component;

use CIBlockElement;
use CIBlockResult;
use CIBlockSection;
use Rapid\Dev\Helper\MyArray;

class MenuExt
{
    protected static function makeMenuItem($arItem, $bSections = false, $intParentLevel = 0, $modifierCallback = false)
    {
        $arItemRes = [
            htmlspecialcharsbx($arItem['~NAME']),
            $bSections ? $arItem['~SECTION_PAGE_URL'] : $arItem['~DETAIL_PAGE_URL'],
            [
            ],
            [
                "FROM_IBLOCK" => true,
                "IS_PARENT" => false,
                "DEPTH_LEVEL" => intval($intParentLevel),
                'SORT' => $arItem['SORT'],
                'ID' => $arItem['ID'],
            ],
        ];
        if (\is_callable($modifierCallback)) {
            $arItemRes = $modifierCallback($arItem, $arItemRes);
        }
        return $arItemRes;
    }

    protected static function makeMenuByItems(CIBlockResult $CIBlockResult, $bSections = false, $intParentLevel = 0)
    {
        $arItems = array();
        while ($arItem = $CIBlockResult->GetNext()) {
            $arItems[$arItem['ID']] = static::makeMenuItem($arItem, $bSections, $intParentLevel);
        }
        return $arItems;
    }

    protected static function getDbSections($arFilter)
    {
        return CIBlockSection::GetList(
            array('LEFT_MARGIN' => 'ASC'),
            $arFilter,
            false,
            array(
                'IBLOCK_ID',
                'IBLOCK_SECTION_ID',
                'ID',
                'DEPTH_LEVEL',
                'NAME',
                'SORT',
                'PICTURE',
                'SECTION_PAGE_URL',
                'UF_*',
            )
        );
    }

    protected static function getDbElements($arFilter, $sortOrder = 'ASC')
    {
        $arSort = array(
//            'LEFT_MARGIN' => $sortOrder,
//                'RIGHT_MARGIN' => $sortOrder,
            'IBLOCK_SECTION_ID' => $sortOrder,
            'SORT' => $sortOrder,
            'ID' => $sortOrder,
        );
        return CIBlockElement::GetList(
            $arSort,
            $arFilter,
            false,
            false,
            array(
                'IBLOCK_ID',
                'IBLOCK_SECTION_ID',
                'ID',
                'NAME',
                'SORT',
                'DETAIL_PAGE_URL',
            )
        );
    }

    /**
     * Строит элементы меню на основании разделов ИБ
     * @param array $arFilter
     * @return array
     */
    public static function makeMenuByIblockSections($arFilter)
    {
        $dbSections = static::getDbSections($arFilter);

        $arData = array();

        while ($arSection = $dbSections->GetNext()) {
            $arItemData = static::makeMenuItem($arSection, true, $arSection['DEPTH_LEVEL']);
            static::insertItem($arData, $arSection, $arItemData);
        }
        return $arData;
    }

    /**
     * Строит элементы меню на основании элементов ИБ
     * @param array $arFilter
     * @param integer $intParentLevel
     * @return array
     */
    public static function makeMenuByIblockElements($arFilter, $intParentLevel = 0)
    {
        $dbItems = static::getDbElements($arFilter);
        return self::makeMenuByItems($dbItems, 0, $intParentLevel);
    }

    protected static function insertItem(&$data, $arIblockItem, $arMenuItem)
    {
        $condition1 = $arIblockItem['IBLOCK_SECTION_ID'] > 0;
        $condition2 = $data[$arIblockItem['IBLOCK_SECTION_ID']];
        $condition3 = (($offset = MyArray::findArrayKeyPos($data, $arIblockItem['IBLOCK_SECTION_ID'])) !== false);
        if (
            $condition1
            && $condition2
            && $condition3
        ) {
            MyArray::insertAfter($data, $arIblockItem['IBLOCK_SECTION_ID'], array($arIblockItem['ID'] => $arMenuItem));
        } else {
            $data[$arIblockItem['ID']] = $arMenuItem;
        }
    }

    public static function makeMenuByIblockSectionsElements($arSectionsFilter, $arElementsFilter, $intParentLevel = 0, $sectionModifierCallback = false, $elementsModifierCallback = false)
    {
        $dbSections = static::getDbSections($arSectionsFilter);
        $dbElements = static::getDbElements($arElementsFilter, 'DESC');
        $arData = array();

        $arSectionLevels = array();

        while ($arSection = $dbSections->GetNext()) {
            $arSectionLevels[$arSection['ID']] = $arSection['DEPTH_LEVEL'];
            $arItemData = static::makeMenuItem($arSection, true, $intParentLevel + $arSection['DEPTH_LEVEL'], $sectionModifierCallback);
            static::insertItem($arData, $arSection, $arItemData);
        }
        while ($arElement = $dbElements->GetNext()) {
            $depthLevel = $intParentLevel;
            if ($arElement['IBLOCK_SECTION_ID'] && (($intSectionDepth = $arSectionLevels[$arElement['IBLOCK_SECTION_ID']]) > 0)) {
                $depthLevel += $intSectionDepth + 1;
            }
            $arItemData = static::makeMenuItem($arElement, false, $depthLevel, $elementsModifierCallback);
            static::insertItem($arData, $arElement, $arItemData);
        }
        return $arData;
    }
}
