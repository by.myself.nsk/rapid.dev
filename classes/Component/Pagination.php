<?php

namespace Rapid\Dev\Component;

use Bitrix\Main\Application;
use Bitrix\Main\Web\Uri;
use Rapid\Dev\Helper\SitePage;

class Pagination
{
    protected $pageWindow = 5;

    /** @var array */
    protected $result;
    protected $startPage;
    protected $endPage;

    public function __construct($result)
    {
        $this->result = $result;
        $this->calculateWindowPages();
    }

    /**
     * @return int
     */
    public function getPageWindow(): int
    {
        return $this->pageWindow;
    }

    /**
     * @param int $pageWindow
     * @return $this
     */
    public function setPageWindow(int $pageWindow)
    {
        $this->pageWindow = $pageWindow;
        $this->calculateWindowPages();
        return $this;
    }

    protected function calculateWindowPages()
    {
        $currentPage = $this->result["NavPageNomer"];
        $halfWindow = floor($this->getPageWindow() / 2);
        $this->startPage = max(1, $currentPage - $halfWindow);
        $this->endPage = min($this->result["NavPageCount"], $currentPage + $halfWindow);
    }

    public function getPagenParameterName()
    {
        return 'PAGEN_' . $this->result['NavNum'];
    }

    public function makeUrl($intPage)
    {
        $strPagen = $this->getPagenParameterName();

        $uri = new Uri(Application::getInstance()->getContext()->getRequest()->getRequestUri());
        $uri->deleteParams([$strPagen]);
        if ($intPage > 1) {
            $uri->addParams([$strPagen => $intPage]);
        }
        return $uri->getUri();
    }

    public function getPrevPageNumber()
    {
        return $this->result["NavPageNomer"] - 1;
    }

    public function getPrevPageLink()
    {
        return $this->makeUrl($this->getPrevPageNumber());
    }

    public function getNextPageNumber()
    {
        return $this->result["NavPageNomer"] + 1;
    }

    public function getNextPageLink()
    {
        return $this->makeUrl($this->getNextPageNumber());
    }

    public function getFirstPageNumber()
    {
        return 1;
    }

    public function getFirstPageLink()
    {
        return $this->makeUrl($this->getFirstPageNumber());
    }

    public function getLastPageNumber()
    {
        return $this->result["NavPageCount"];
    }

    public function getLastPageLink()
    {
        return $this->makeUrl($this->getLastPageNumber());
    }

    public function getMidBeforePageNumber()
    {
        return floor(($this->result["nStartPage"] - 1) / 2) + 1;
    }

    public function getMidBeforePageLink()
    {
        $intMidPage = $this->getMidBeforePageNumber();
        return $this->makeUrl($intMidPage);
    }

    public function getMidBeforePageText()
    {
        if ($this->result["nStartPage"] == 3) {
            return $this->getMidBeforePageNumber();
        }
        return '&hellip;';
    }

    public function getMidAfterPageNumber()
    {
        $arResult = &$this->result;
        return floor(($arResult["NavPageCount"] - $arResult["nEndPage"]) / 2) + $arResult["nEndPage"];
    }

    public function getMidAfterPageLink()
    {
        $intMidPage = $this->getMidAfterPageNumber();
        return $this->makeUrl($intMidPage);
    }

    public function getMidAfterPageText()
    {
        if ($this->result["nEndPage"] == ($this->result["NavPageCount"] - 2)) {
            return $this->getMidAfterPageNumber();
        }
        return '&hellip;';
    }

    public function needShowNav()
    {
        $arResult = &$this->result;
        if ($arResult["NavShowAlways"]) {
            return true;
        }
        return !($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false));
    }

    public function needShowPrevPage()
    {
        return $this->result["NavPageNomer"] > 1;
    }

    public function needShowNextPage()
    {
        return $this->result["NavPageNomer"] < $this->result["NavPageCount"];
    }

    public function needShowFirstPage()
    {
        return $this->result["nStartPage"] > 1;
    }

    public function needShowLastPage()
    {
        return $this->result["nEndPage"] < $this->result["NavPageCount"];
    }

    public function needShowMidBeforePage()
    {
        return $this->result["nStartPage"] > 2;
    }

    public function needShowMidAfterPage()
    {
        return $this->result["nEndPage"] < ($this->result["NavPageCount"] - 1);
    }

    public function getStartPageNumber()
    {
        return $this->result["nStartPage"];
    }

    public function getEndPageNumber()
    {
        return $this->result["nEndPage"];
    }

    public function getCurrentPageNumber()
    {
        return $this->result["NavPageNomer"];
    }

    public function getPageSize()
    {
        return $this->result['NavPageSize'];
    }

    public function getTotalCount()
    {
        return $this->result['NavRecordCount'];
    }

    public function getLastShownRecordNumber()
    {
        return $this->result['NavLastRecordShow'];
    }
}
