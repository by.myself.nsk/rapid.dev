<?php

namespace Rapid\Dev\Provider\GraphQL;

use Exception;
use GraphQL\Type\Schema;
use GraphQL\Type\SchemaConfig;

class GraphQL
{
    protected $schema;
    protected $request;
    protected $response;

    /**
     * @param $config
     * @return GraphQL
     */
    public function setSchema($config)
    {
        $this->schema = new Schema($config);
        return $this;
    }

    /**
     * @return Schema
     */
    protected function getSchema()
    {
        return $this->schema;
    }

    public function readRequest()
    {
        $input = file_get_contents('php://input');
        $arInput = json_decode($input, true);
        $this->request = $arInput['query'];
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    public function executeQuery()
    {
        try {
            $schema = $this->getSchema();
            $source = $this->getRequest();
            if (!$schema instanceof Schema) {
                throw new Exception();
            }
            if (!is_string($source)) {
                throw new Exception();
            }
            $this->response = \GraphQL\GraphQL::executeQuery(
                $schema,
                $source
            )->toArray();
        } catch (Exception $e) {
            // $e->getMessage();
        }
        return $this;
    }

    public function answer()
    {
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode($this->response);
    }

    public static function processRequest(SchemaConfig $schemaConfig)
    {
        $obClient = new self();
        $obClient->setSchema($schemaConfig);
        $obClient->readRequest();
        $obClient->executeQuery();
        $obClient->answer();
    }
}
