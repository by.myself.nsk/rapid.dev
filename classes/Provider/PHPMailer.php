<?

namespace Rapid\Dev\Provider;

use COption;
use Exception;

class PHPMailer
{
    /** @var \PHPMailer\PHPMailer\PHPMailer  */
    protected $mailer;
    protected $to;
    protected $subject;
    protected $message;
    protected $additionalHeaders;
    protected $additionalParameters;

    /**
     * MailerProvider constructor.
     * @param $to
     * @param $subject
     * @param $message
     * @param string $additionalHeaders
     * @param string $additionalParameters
     * @throws Exception
     */
    public function __construct($to, $subject, $message, $additionalHeaders='', $additionalParameters='')
    {
        if (!class_exists('PHPMailer\PHPMailer\PHPMailer')) {
            throw new Exception('install PHPMailer');
        }
        $this->mailer = new \PHPMailer\PHPMailer\PHPMailer(true);
        $this->to = $to;
        $this->subject = $subject;
        $this->message = $message;
        $this->additionalHeaders = $additionalHeaders;
        $this->additionalParameters = $additionalParameters;

        $arHeaders = self::parseAdditionalHeaders($this->additionalHeaders);
        $this
            ->setTo($this->to)
            ->setHeaders($arHeaders)
            ->setSubject($this->subject)
            ->setBody($this->message)
        ;
    }

    private static function parseAdditionalHeaders($strHeaders)
    {
        $arHeaderLines = preg_split("/\\r\\n|\\r|\\n/", $strHeaders);
        $arHeaders = [];
        foreach ($arHeaderLines as $strHeaderLine) {
            $arHeaderData = explode(':', $strHeaderLine, 2);
            $arHeaders[trim($arHeaderData[0])] = trim($arHeaderData[1]);
        }
        return $arHeaders;
    }

    /**
     * @param $to
     * @return $this
     * @throws \PHPMailer\PHPMailer\Exception
     */
    private function setTo($to)
    {
        if (is_string($to)) {
            $to = explode(',', $to);
        }
        if (is_array($to)) {
            $to = array_filter($to);
            foreach ($to as $address) {
                $address = filter_var($address, FILTER_SANITIZE_EMAIL);
                if (!filter_var($address, FILTER_VALIDATE_EMAIL)) {
                    continue;
                }
                $this->mailer->addAddress($address);
            }
        }
        return $this;
    }

    /**
     * @param $arHeaders
     * @return $this
     * @throws \PHPMailer\PHPMailer\Exception
     */
    private function setHeaders($arHeaders)
    {
        foreach ($arHeaders as $headerName => $value) {
            switch (ToLower($headerName)) {
                case 'from':
                    $this->setFrom($value);
                    break;
                case 'bcc':
                    $this->setBcc($value);
                    break;
                case 'cc':
                    $this->setCc($value);
                    break;
                case 'reply-to':
                    $this->setReplyTo($value);
                    break;
                case 'content-type':
                    $this->setContentType($value);
                    break;
                default:
                    $this->SetCustomHeader($headerName, $value);
                    break;
            }
        }
        return $this;
    }

    /**
     * @param $headerData
     * @return $this
     * @throws \PHPMailer\PHPMailer\Exception
     */
    private function setFrom($headerData)
    {
        $arFromData = self::parseSenderFrom($headerData);
        $arFromData['From'] = COption::GetOptionString('main', 'email_from', $arFromData['From']);
        $this->mailer->setFrom($arFromData['From'], $arFromData['FromName']);
        return $this;
    }

    /**
     * @param $headerData
     * @return $this
     * @throws \PHPMailer\PHPMailer\Exception
     */
    private function setBcc($headerData)
    {
        $addBccs = explode(',', $headerData);
        foreach ($addBccs as $addBcc) {
            $this->mailer->addBCC($addBcc);
        }
        return $this;
    }

    /**
     * @param $headerData
     * @return $this
     * @throws \PHPMailer\PHPMailer\Exception
     */
    private function setCc($headerData)
    {
        $addBccs = explode(',', $headerData);
        foreach ($addBccs as $addBcc) {
            $this->mailer->addCC($addBcc);
        }
        return $this;
    }

    /**
     * @param $headerData
     * @return $this
     * @throws \PHPMailer\PHPMailer\Exception
     */
    private function setReplyTo($headerData)
    {
        $this->mailer->addReplyTo($headerData);
        return $this;
    }

    /**
     * @param $headerData
     * @return $this
     */
    private function setContentType($headerData)
    {
        $this->mailer->ContentType = $headerData;
        return $this;
    }

    /**
     * @param $headerName
     * @param $headerData
     * @return $this
     */
    private function setCustomHeader($headerName, $headerData)
    {
        $this->mailer->AddCustomHeader($headerName, $headerData);
        return $this;
    }

    /**
     * @param $subject
     * @return $this
     */
    private function setSubject($subject)
    {
        $decodedSubject = self::decodeSubject($subject);
        $this->mailer->Subject = $decodedSubject ? self::encodeSubject($decodedSubject) : $subject;
        return $this;
    }

    private function setBody($body)
    {
        $mail = &$this->mailer;
        $mail->Body = $body;
        if ($this->isHtml()) {
            $this->addAltBody($body);
            if (!$this->bodyHasHtmlTag()) {
                $this->addHtmlTagToBody();
            }
        }
        return $this;
    }

    /**
     * @param string $charSet
     * @return PHPMailer
     */
    public function setCharSet($charSet)
    {
        $mail = &$this->mailer;
        $mail->CharSet = $charSet;
        return $this;
    }

    private function isHtml()
    {
        return strpos($this->mailer->ContentType, 'text/html') !== false;
    }

    private function addAltBody($body)
    {
        $mail = &$this->mailer;
        $mail->AltBody = HTMLToTxt($body);
    }

    private function bodyHasHtmlTag()
    {
        return strpos($this->mailer->Body, '<html>') !== false;
    }

    private function addHtmlTagToBody()
    {
        $mail = &$this->mailer;
        $mail->Body = '<html><body>' . $mail->Body . '</body></html>';
        return $this;
    }

    private static function parseSenderFrom($from)
    {
        $arSender = [
            'From' => '',
            'FromName' => '',
        ];
        if (!$from) {
            return $arSender;
        }
        if (preg_match('/((.+)<)?(.+@[^>]+)(>?)/', $from, $arMatches) === false) {
            return $arSender;
        }
        $arSender['From'] = trim($arMatches[3]);
        $arSender['FromName'] = trim($arMatches[2]);
        return $arSender;
    }

    private static function decodeSubject($subject)
    {
        preg_match_all('/=\?(.*)\?B\?(.*)\?=/', $subject, $arMatches);
        $strRes = '';
        foreach ($arMatches[2] as $strMatch) {
            $strRes .= base64_decode($strMatch);
        }
        return $strRes;
    }

    private static function encodeSubject($subject)
    {
        return '=?UTF-8?B?'.base64_encode($subject).'?=';
    }

    /**
     * @param $domain
     * @param $keyPath
     * @param string $selector
     * @return PHPMailer
     */
    public function setDkim($domain, $keyPath, $selector = 'mail')
    {
        $mail = &$this->mailer;
        $mail->DKIM_domain = $domain;
        $mail->DKIM_private = $keyPath;
        $mail->DKIM_passphrase = '';
        $mail->DKIM_selector = $selector;
        $mail->DKIM_identity = $mail->From;

        return $this;
    }

    public function setSmtp($username, $password, $host, $port = 587, $secure = 'tls', $auth = true)
    {
        $mail = &$this->mailer;
        $mail->Username = $username;
        $mail->Password = $password;
        $mail->Host = $host;
        $mail->Port = $port;
        $mail->SMTPSecure = $secure;
        $mail->SMTPAuth = $auth;
        $mail->isSMTP();
    }

    public function debugOutout($logger)
    {
        $mail = &$this->mailer;
        $mail->Debugoutput = $logger;
        return $this;
    }

    /**
     * @throws \PHPMailer\PHPMailer\Exception
     * @throws Exception
     */
    public function send()
    {
        $mail = &$this->mailer;
        $success = $mail->send();
        if (!$success) {
            throw new Exception($mail->ErrorInfo);
        }
        return true;
    }
}
