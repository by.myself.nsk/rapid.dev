<?php

namespace Rapid\Dev\Provider\Intervention;

use CFile;
use Exception;
use Intervention\Image\ImageManager;

class Image
{
    public static $DRIVER_GD = 'gd';
    public static $DRIVER_IMAGICK = 'imagick';
    protected $driver;
    /** @var  ImageManager */
    protected $imageManagerInstance;
    /** @var int */
    protected $quality;

    public function __construct()
    {
        $this->setDriver(self::$DRIVER_GD);
        $this->setQuality(95); // todo
    }

    /**
     * @param int $driver
     * @return Image
     */
    public function setDriver(string $driver): Image
    {
        $this->driver = $driver;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * @return ImageManager
     */
    public function getImageManagerInstance()
    {
        if ($this->imageManagerInstance) {
            return $this->imageManagerInstance;
        }
        $this->imageManagerInstance = new ImageManager(['driver' => $this->driver]);
        return $this->imageManagerInstance;
    }

    /**
     * @return int
     */
    public function getQuality(): int
    {
        return $this->quality;
    }

    /**
     * @param int $quality
     */
    public function setQuality(int $quality)
    {
        $this->quality = $quality;
    }



    public function test($fileId)
    {
        try {
            $this->checkComposerModule();
            $this->checkDriver();
            $manager = $this->getImageManagerInstance();


            $path = CFile::GetPath($fileId);
            $path = $_SERVER['DOCUMENT_ROOT'] . $path;

            $image = $manager->make($path);
            $srcExtension = $image->extension;
            $destExtension = 'webp';
            $fileName = $image->filename;
            $resizeWidth = '800';
            $resizeHeight = '600';

            $pathToSave = '/dev/img/intervention';
            $fullPathToSave = $_SERVER['DOCUMENT_ROOT'] . '/dev/img/intervention';
            $pathToSaveOrig = $pathToSave . '/' . $fileName . '.' . $srcExtension;
            $pathToSaveConvert = $pathToSave . '/' . $fileName . '.' . $destExtension;
            $fullPathToSaveOrig = $fullPathToSave . '/' . $fileName . '.' . $srcExtension;
            $fullPathToSaveConvert = $fullPathToSave . '/' . $fileName . '.' . $destExtension;

            CheckDirPath($fullPathToSaveOrig);
            CheckDirPath($fullPathToSaveConvert);

//$image->resize($resizeWidth, $resizeHeight); // скукоживает
            $image->resize($resizeWidth, $resizeHeight); // вырезает область по размеру
//$image->resize($resizeWidth, $resizeHeight, function(){});
            $image->save($fullPathToSaveOrig, 100);
            $image->encode('webp', 100)->save($fullPathToSaveConvert, 100);
            /**/
//$image = $manager->make('public/foo.jpg')->resize(300, 200);
//$image->save($pathToSaveOrig, 100);
//$image->encode('webp', 100)->save($pathToSaveConvert, 100);
            /*/
            ?>$image:<br/><?
            ?><pre><?var_dump($image)?></pre><?
            /**/
            ?><img src="<?=$pathToSaveOrig?>" alt=""><?
            ?><img src="<?=$pathToSaveConvert?>" alt=""><?
        } catch (Exception $e) {
            /*/
            ?>$e:<br/><?
            ?><pre><?var_dump($e)?></pre><?
            /**/
//             $e->getMessage();
        }
    }

    /**
     * @throws Exception
     */
    protected function checkComposerModule()
    {
        if (!class_exists('Intervention\Image\ImageManager')) {
            throw new Exception('install intervention/image composer module');
        }
    }

    /**
     * @throws Exception
     */
    protected function checkDriver()
    {
        switch ($this->driver) {
            case self::$DRIVER_GD:
                if (!extension_loaded('gd')) {
                    throw new Exception('gd not installed');
                }
                break;
            case self::$DRIVER_IMAGICK:
                if (!extension_loaded('imagick')) {
                    throw new Exception('imagick not installed');
                }
                break;
        }
    }

}
