<?php

namespace Rapid\Dev\Provider\Monolog;

use Monolog\Logger;

class Monolog
{
    protected static $arInstances = [];
    protected static $logDir = '/local/logs';

    /**
     * @param string $name
     * @return Logger
     */
    public static function getInstance(string $name = 'main'):Logger
    {
        $obInstance = &self::$arInstances[$name];
        if (!$obInstance) {
            $obInstance = new Logger($name);
        }
        return $obInstance;
    }

    /**
     * @return string
     */
    public static function getLogDir(): string
    {
        return $_SERVER['DOCUMENT_ROOT'] . self::$logDir;
    }

    /**
     * @param string $logDir
     */
    public static function setLogDir(string $logDir)
    {
        self::$logDir = self::fixSlashes($logDir);
    }

    public static function makePath(string $file)
    {
        return self::getLogDir() . self::fixSlashes($file);
    }

    protected static function fixSlashes($path)
    {
        return DIRECTORY_SEPARATOR . trim($path, "\\/");
    }
}
