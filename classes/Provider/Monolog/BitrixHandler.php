<?php

namespace Rapid\Dev\Provider\Monolog;

use Monolog\Logger;
use Monolog\Handler\AbstractProcessingHandler;
use Rapid\Dev\Helper\Site;

class BitrixHandler extends AbstractProcessingHandler
{
    protected function write(array $record)
    {
        $arLevels = Logger::getLevels();
        $strSeverity = array_key_exists($record['level_name'], $arLevels) ? $record['level_name'] : 'INFO';
        \CEventLog::Add([
            'SEVERITY' => $strSeverity,
            'AUDIT_TYPE_ID' => $record['channel'],         // собственный ID типа события
            'MODULE_ID' => 'project',                      // модуль, с которого происходит запись в лог
//            'ITEM_ID' => '',                             // ID объекта, в связи с которым происходит добавление (пользователь, элемент ИБ, ID сообщения, ...)
            'SITE_ID' => Site::getSiteIdOrDefaultSiteId(), //  ID сайта, к которому относится добавляемое событие
            'DESCRIPTION' => $record['message'],           // описание записи лога, или техническая информация
        ]);
    }
}
