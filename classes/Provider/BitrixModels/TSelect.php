<?php

namespace Rapid\Dev\Provider\BitrixModels;

trait TSelect
{
    public static function getSelect()
    {
        return static::$select;
    }
}
