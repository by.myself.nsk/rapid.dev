<?php

namespace Rapid\Dev\Provider\BitrixModels;

use CFile;
use CIBlock;
use Exception;
use Illuminate\Support\Str;
use Arrilot\BitrixModels\Models\ElementModel as ArrilotElementModel;
use Rapid\Dev\Helper\Cache;
use Rapid\Dev\Helper\ImageToWebp;

abstract class ElementModel extends ArrilotElementModel implements SortInterface, SelectInterface
{
    use TIblockModel, TSort, TSelect, SelectQueryTrait;

    protected static $sort = ['SORT' => 'ASC'];
    protected static $select = ['FIELDS', 'PROPS'];

    public static $fetchUsing = [
        'method' => 'GetNext',
        // эти параметры будут переданы в GetNext при вызове. $bTextHtmlAuto, $useTilda
        // $bTextHtmlAuto - преобразовывать в html-безопасный вид
        // $useTilda - с ключом ~ хранить оригинальные значения
        'params' => [true, false],
    ];

    protected static $fieldNamePattern = '/^PROPERTY_(.*)_VALUE$/';

    public function findAttributeKey($key)
    {
        if (array_key_exists($key, $this->fields)) {
            return $key;
        }
        $upper = ToUpper(Str::snake($key));
        if (array_key_exists($upper, $this->fields)) {
            return $upper;
        }
        $propertyKey = 'PROPERTY_'.$upper.'_VALUE';
        if (array_key_exists($propertyKey, $this->fields)) {
            return $propertyKey;
        }
        $propertyKey = 'PROPERTY_'.$upper;
        if (array_key_exists($propertyKey, $this->fields)) {
            return $propertyKey;
        }
        return false;
    }

    public function getPictures($field, $params, $isSingle = true)
    {
        if ($isSingle) {
            $intVal = (int)$this[$field];
            return self::resizeSinglePicture($intVal, $params);
        } else {
            $arRes = [];
            if (!is_array($this[$field])) {
                return $arRes;
            }
            foreach ($this[$field] as $intVal) {
                $intVal = (int)$intVal;
                $arRes[] = self::resizeSinglePicture($intVal, $params);
            }
            return $arRes;
        }
    }

    public static function resizeSinglePicture($intVal, $params)
    {
        try {
            if (!$intVal || !is_array($params) || !$params) {
                throw new Exception();
            }
            $arRes = [];
            foreach ($params as $key => $sizeParams) {
                $sizeX = $sizeParams[0];
                $sizeY = $sizeParams[1];
                $needConvertToWebP = $sizeParams[2];
                $resizeType = $sizeParams[3] ?: \BX_RESIZE_IMAGE_PROPORTIONAL;
                $needResize = $sizeX && $sizeY;
                /** @var ImageToWebp|CFile $class */
                $class = $needConvertToWebP ? '\Rapid\Dev\Helper\ImageToWebp' : 'CFile';

                $arResize = $class::ResizeImageGet($intVal, $needResize ? ['width' => $sizeX, 'height' => $sizeY] : [], $resizeType);
                $arRes[$key] = $arResize['src'];
            }
            return $arRes;
        } catch (Exception $e) {
            // $e->getMessage();
            return false;
        }
    }

    public static function getListPageUrl():string
    {
        $id = static::iblockId();
        $obCache = new Cache(__METHOD__, $id);
        return $obCache->get(function() use ($id) {
            $arIblock = CIblock::GetByID($id)->Fetch();
            return CIBlock::ReplaceDetailUrl($arIblock['LIST_PAGE_URL'], $arIblock);
        });
    }
}
