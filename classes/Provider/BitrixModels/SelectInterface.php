<?php

namespace Rapid\Dev\Provider\BitrixModels;

interface SelectInterface
{
    public static function getSelect();
}
