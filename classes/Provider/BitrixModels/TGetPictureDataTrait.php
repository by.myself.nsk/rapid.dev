<?php

namespace Rapid\Dev\Provider\BitrixModels;

use CFile;
use Exception;
use Illuminate\Support\Str;
use Rapid\Dev\Helper\Iblock\Iblock;
use Rapid\Dev\Helper\ImageToWebp;
use Rapid\Dev\Helper\Site;
use Rapid\Dev\Helper\SitePage;

trait TGetPictureDataTrait
{
    protected static $useWebp = true;

    public function makePictureData($value)
    {
        try {
//            $intVal = (int)$value;
//            if (!$intVal) {
//                throw new Exception();
//            }
//            $arResize = CFile::ResizeImageGet($intVal, []);
//            $arRes['ORIGINAL'] = $arResize['src'];
//            if (self::$useWebp) {
//                $arWebpResize = ImageToWebp::ResizeImageGet($intVal, []);
//                $arRes['webp_ORIGINAL'] = $arWebpResize['src'];
//            }
//
//            foreach (static::$pictureSizes as $key => $arSize) {
//                $type = $arSize[2] ?: \BX_RESIZE_IMAGE_PROPORTIONAL;
//                $strKey = is_numeric($key) ? $arSize[0] . '_' . $arSize[1] . '_' . ($type == BX_RESIZE_IMAGE_PROPORTIONAL ? 'P' : 'C') : $key;
//                $arResize = CFile::ResizeImageGet($intVal, ['width' => $arSize[0], 'height' => $arSize[1]], $type);
//                $arRes[$strKey] = $arResize['src'];
//
//                if (self::$useWebp) {
//                    $arWebpResize = ImageToWebp::ResizeImageGet($intVal, ['width' => $arSize[0], 'height' => $arSize[1]], $type);
//                    $arRes['webp_' . $strKey] = $arWebpResize['src'];
//                }
//            }
//            return $arRes;


        } catch (Exception $e) {
            return false;
        }
    }

}
