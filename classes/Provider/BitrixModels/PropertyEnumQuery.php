<?php

namespace Rapid\Dev\Provider\BitrixModels;

use Arrilot\BitrixModels\Queries\OldCoreQuery;
use CIBlock;
use Illuminate\Support\Collection;
use Exception;
use Illuminate\Support\Str;

/**
 * @method PropertyEnumQuery active()
 */
class PropertyEnumQuery extends OldCoreQuery
{
    /**
     * CIblock object or test double.
     *
     * @var object.
     */
    public static $cIblockObject;

    /**
     * Query sort.
     *
     * @var array
     */
    public $sort = ['SORT' => 'ASC'];

    /**
     * Iblock id.
     *
     * @var int
     */
    protected $iblockId;

    /**
     * Property code.
     *
     * @var string
     */
    protected $propertyCode;

    /**
     * List of standard entity fields.
     *
     * @var array
     */
    protected $standardFields = [
        'VALUE',
        'ID',
        'SORT',
        'DEF',
        'XML_ID',
        'EXTERNAL_ID',
        'CODE',
        'PROPERTY_ID',
        'IBLOCK_ID',
    ];

    /**
     * Constructor.
     *
     * @param object $bxObject
     * @param string|PropertyEnumModel $modelName
     */
    public function __construct($bxObject, $modelName)
    {
        static::instantiateCIblockObject();
        parent::__construct($bxObject, $modelName);

        $this->iblockId = $modelName::iblockId();
        $this->propertyCode = $modelName::propertyCode();
        $this->iblockVersion = $modelName::IBLOCK_VERSION ?: 2;
    }

    /**
     * Instantiate bitrix entity object.
     *
     * @throws Exception
     *
     * @return object
     */
    public static function instantiateCIblockObject()
    {
        if (static::$cIblockObject) {
            return static::$cIblockObject;
        }

        if (class_exists('CIBlock')) {
            return static::$cIblockObject = new CIBlock();
        }

        throw new Exception('CIblock object initialization failed');
    }


    /**
     * Get list of items.
     *
     * @return Collection
     */
    protected function loadModels()
    {
        $sort = $this->sort;
        $filter = $this->normalizeFilter();
        $select = $this->normalizeSelect();
        $queryType = 'PropertyEnumQuery::getList';
        $fetchUsing = $this->fetchUsing;
        $keyBy = $this->keyBy;

        $callback = function() use ($sort, $filter, $select) {
            $items = [];
            $rsItems = $this->bxObject->GetList($sort, $filter, $select);
            while ($arItem = $this->performFetchUsingSelectedMethod($rsItems)) {
                $this->addItemToResultsUsingKeyBy($items, new $this->modelName($arItem['ID'], $arItem));
            }
            return new Collection($items);
        };

        $cacheKeyParams = compact('sort', 'filter', 'select', 'queryType', 'keyBy', 'fetchUsing');

        return $this->handleCacheIfNeeded($cacheKeyParams, $callback);
    }

    protected function getPropertyId()
    {
        $mapping = $this->getPropertyCodeToIdMapping();
        $propertyId = $mapping[$this->propertyCode];
        return $propertyId;
    }

    protected function getPropertyCodeToIdMapping()
    {
        static $data;
        if ($data[$this->iblockId]) {
            return $data[$this->iblockId];
        }
        $rsProps = static::$cIblockObject->GetProperties($this->iblockId);
        while ($prop = $rsProps->Fetch()) {
            $data[$this->iblockId][$prop['CODE']] = $prop['ID'];
        }
        return $data[$this->iblockId];
    }

    /**
     * Get the first element with a given code.
     *
     * @param string $code
     *
     * @return PropertyEnumModel
     */
    public function getByCode($code)
    {
        $this->filter['=XML_ID'] = $code;

        return $this->first();
    }

    /**
     * Get the first element with a given external id.
     *
     * @param string $id
     *
     * @return PropertyEnumModel
     */
    public function getByExternalId($id)
    {
        $this->filter['EXTERNAL_ID'] = $id;

        return $this->first();
    }

    /**
     * Get count of elements that match $filter.
     *
     * @return int
     */
    public function count()
    {
        if ($this->queryShouldBeStopped) {
            return 0;
        }

        $filter = $this->normalizeFilter();
        $queryType = "PropertyEnum::count";

        $callback = function () use ($filter) {
            return (int) $this->bxObject->GetList(false, $filter, []);
        };

        return $this->handleCacheIfNeeded(compact('filter', 'queryType'), $callback);
    }

    /**
     * Normalize filter before sending it to getList.
     * This prevents some inconsistency.
     *
     * @return array
     */
    protected function normalizeFilter()
    {
        $this->filter['IBLOCK_ID'] = $this->iblockId;
        $this->filter['PROPERTY_ID'] = $this->getPropertyId();
        return $this->filter;
    }

    /**
     * Normalize select before sending it to getList.
     * This prevents some inconsistency.
     *
     * @return array
     */
    protected function normalizeSelect()
    {
        if ($this->fieldsMustBeSelected()) {
            $this->select = array_merge($this->standardFields, $this->select);
        }

        $this->select[] = 'ID';
        $this->select[] = 'IBLOCK_ID';

        return $this->clearSelectArray();
    }
}
