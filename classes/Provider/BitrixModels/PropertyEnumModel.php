<?php

namespace Rapid\Dev\Provider\BitrixModels;

use Arrilot\BitrixModels\Exceptions\ExceptionFromBitrix;
use Arrilot\BitrixModels\Models\BitrixModel;
use CIBlock;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use LogicException;

/**
 * PropertyEnumQuery methods
 * @method static static getByCode(string $code)
 * @method static static getByExternalId(string $id)
 *
 * Base Query methods
 * @method static Collection|static[] getList()
 * @method static static first()
 * @method static static getById(int $id)
 * @method static PropertyEnumQuery sort(string|array $by, string $order='ASC')
 * @method static PropertyEnumQuery order(string|array $by, string $order='ASC') // same as sort()
 * @method static PropertyEnumQuery filter(array $filter)
 * @method static PropertyEnumQuery addFilter(array $filters)
 * @method static PropertyEnumQuery resetFilter()
 * @method static PropertyEnumQuery select($value)
 * @method static PropertyEnumQuery keyBy(string $value)
 * @method static PropertyEnumQuery limit(int $value)
 * @method static PropertyEnumQuery offset(int $value)
 * @method static PropertyEnumQuery page(int $num)
 * @method static PropertyEnumQuery take(int $value) // same as limit()
 * @method static PropertyEnumQuery forPage(int $page, int $perPage=15)
 * @method static \Illuminate\Pagination\LengthAwarePaginator paginate(int $perPage = 15, string $pageName = 'page')
 * @method static \Illuminate\Pagination\Paginator simplePaginate(int $perPage = 15, string $pageName = 'page')
 * @method static PropertyEnumQuery stopQuery()
 * @method static PropertyEnumQuery cache(float|int $minutes)
 *
 * Scopes
 * @method static PropertyEnumQuery active()
 */
class PropertyEnumModel extends BitrixModel
{
    /**
     * Corresponding IBLOCK_ID
     *
     * @var int
     */
    const IBLOCK_ID = null;

    /**
     * IBLOCK version (1 or 2)
     *
     * @var int
     */
    const IBLOCK_VERSION = 2;

    /**
     * Bitrix entity object.
     *
     * @var object
     */
    public static $bxObject;

    /**
     * Corresponding object class name.
     *
     * @var string
     */
    protected static $objectClass = 'CIBlockPropertyEnum';

    /**
     * Iblock PropertiesData from Bitrix DB
     *
     * @var null|array
     */
    protected static $iblockPropertiesData = [];

    /** @var string */
    protected static $propertyCode;

    /**
     * Getter for corresponding iblock id.
     *
     * @throws LogicException
     *
     * @return int
     */
    public static function iblockId()
    {
        $id = static::IBLOCK_ID;
        if (!$id) {
            throw new LogicException('You must set IBLOCK_ID constant inside a model or override iblockId() method');
        }

        return $id;
    }

    public static function propertyCode()
    {
        return static::$propertyCode;
    }

    /**
     * Create new item in database.
     *
     * @param $fields
     *
     * @throws LogicException
     *
     * @return static|bool
     * @throws ExceptionFromBitrix
     */
    public static function create($fields)
    {
        if (!isset($fields['IBLOCK_ID'])) {
            $fields['IBLOCK_ID'] = static::iblockId();
        }

        return static::internalCreate($fields);
    }

    public static function internalDirectCreate($bxObject, $fields)
    {
        return $bxObject->add($fields);
    }

    /**
     * Fetches static::$iblockPropertiesData if it's not fetched and returns it.
     *
     * @return array
     */
    protected static function getCachedIblockPropertiesData()
    {
        $iblockId = static::iblockId();
        if (!empty(self::$iblockPropertiesData[$iblockId])) {
            return self::$iblockPropertiesData[$iblockId];
        }

        $props = [];
        $dbRes = CIBlock::GetProperties($iblockId, [], []);
        while($property = $dbRes->Fetch()) {
            $props[$property['CODE']] = $property;
        }

        return self::$iblockPropertiesData[$iblockId] = $props;
    }

    /**
     * Instantiate a query object for the model.
     *
     * @return PropertyEnumQuery
     */
    public static function query()
    {
        return new PropertyEnumQuery(static::instantiateObject(), get_called_class());
    }

    /**
     * Fill extra fields when $this->field is called.
     *
     * @return null
     */
    protected function afterFill()
    {
    }

    /**
     * Load all model attributes from cache or database.
     *
     * @return $this
     */
    public function load()
    {
        $this->getFields();

        return $this;
    }

    /**
     * Refresh model from database and place data to $this->fields.
     *
     * @return array
     */
    public function refresh()
    {
        return $this->refreshFields();
    }

    /**
     * Refresh element's fields and save them to a class field.
     *
     * @return array
     */
    public function refreshFields()
    {
        if ($this->id === null) {
            $this->original = [];
            return $this->fields = [];
        }

        $this->fields = static::query()->getById($this->id)->fields;

        $this->fieldsAreFetched = true;

        $this->original = $this->fields;

        return $this->fields;
    }

    /**
     * Determine whether the field should be stopped from passing to "update".
     *
     * @param string $field
     * @param mixed  $value
     * @param array  $selectedFields
     *
     * @return bool
     */
    protected function fieldShouldNotBeSaved($field, $value, $selectedFields)
    {
        $blacklistedFields = [
            'ID',
            'IBLOCK_ID',
        ];

        return (!empty($selectedFields) && !in_array($field, $selectedFields))
            || in_array($field, $blacklistedFields)
            || ($field[0] === '~');
        //|| (substr($field, 0, 9) === 'PROPERTY_');
    }

    /**
     * @param $fields
     * @param $fieldsSelectedForSave
     * @return bool
     */
    protected function internalUpdate($fields, $fieldsSelectedForSave)
    {
        $fields = $fields ?: [];
        $result = !empty($fields) ? static::$bxObject->update($this->id, $fields) : false;
        return $result;
    }

//----------------------------------------------------------------------------------------------------------------------

    public function findAttributeKey($key)
    {
        if (array_key_exists($key, $this->fields)) {
            return $key;
        }
        $upper = ToUpper(Str::snake($key));
        if (array_key_exists($upper, $this->fields)) {
            return $upper;
        }
        $propertyKey = 'UF_' . $upper;
        if (array_key_exists($propertyKey, $this->fields)) {
            return $propertyKey;
        }

        return false;
    }
}
