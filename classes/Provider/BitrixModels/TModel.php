<?php

namespace Rapid\Dev\Provider\BitrixModels;

use Illuminate\Support\Str;

trait TModel
{
    public function getFieldsSnake()
    {
        if (is_array($this->fields)) {
            return $this->fields;
        }
        return array();
    }

    public function getFieldsCamel()
    {
        $arSnake = $this->getFieldsSnake();
        $arCamel = [];
        if (!$arSnake) {
            return $arCamel;
        }
        foreach ($arSnake as $strSnakeKey => $val) {
            if (Str::endsWith($strSnakeKey, '_VALUE_ID')) {
                continue;
            }
            if (\preg_match(static::$fieldNamePattern, $strSnakeKey, $arMatches)) {
                $strSnakeKey = $arMatches[1];
            }
            $arCamel[Str::camel(ToLower($strSnakeKey))] = $val;
        }
        return $arCamel;
    }
}
