<?php

namespace Rapid\Dev\Provider\BitrixModels;

trait SelectQueryTrait
{
    public static function query()
    {
        if (method_exists(static::class, 'getSelect')) {
            return parent::query()->select(static::getSelect());
        }
        return parent::query();
    }
}
