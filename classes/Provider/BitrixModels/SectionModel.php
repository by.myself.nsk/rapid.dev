<?php

namespace Rapid\Dev\Provider\BitrixModels;

use Arrilot\BitrixModels\Models\SectionModel as ArrilotSectionModel;
use Illuminate\Support\Str;

abstract class SectionModel extends ArrilotSectionModel implements SortInterface, SelectInterface
{
    use TIblockSectionModel, TSort, TSelect, SelectQueryTrait;

    protected static $sort = ['SORT' => 'ASC'];
    protected static $select = ['FIELDS', 'PROPS'];

    public static $fetchUsing = [
        'method' => 'GetNext',
        // эти параметры будут переданы в GetNext при вызове. $bTextHtmlAuto, $useTilda
        // $bTextHtmlAuto - преобразовывать в html-безопасный вид
        // $useTilda - с ключом ~ хранить оригинальные значения
        'params' => [true, false],
    ];

    protected static $fieldNamePattern = '/^UF_(.*)$/';

    public static function hasMappingKey($key)
    {
        return array_key_exists($key, static::$mapping);
    }

    public function findAttributeKey($key)
    {
        if (array_key_exists($key, $this->fields)) {
            return $key;
        }
        $upper = ToUpper(Str::snake($key));
        if (array_key_exists($upper, $this->fields)) {
            return $upper;
        }
        $propertyKey = 'UF_' . $upper;
        if (array_key_exists($propertyKey, $this->fields)) {
            return $propertyKey;
        }

        return false;
    }
}
