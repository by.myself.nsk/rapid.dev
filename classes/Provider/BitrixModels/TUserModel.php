<?php

namespace Rapid\Dev\Provider\BitrixModels;

use Exception;
use Illuminate\Support\Str;

trait TUserModel
{
    use TModel;

    public function __get($name)
    {
        try {
            return parent::__get($name);
        } catch (Exception $e) {
            $upper = ToUpper(Str::snake($name));
            if (array_key_exists($upper, $this->fields)) {
                return $this->fields[$upper];
            }
            $propertyKey = 'UF_'.$upper;
            if (array_key_exists($propertyKey, $this->fields)) {
                return $this->fields[$propertyKey];
            }
            throw new $e;
        }
    }
}
