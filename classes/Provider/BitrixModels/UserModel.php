<?php

namespace Rapid\Dev\Provider\BitrixModels;

abstract class UserModel extends \Arrilot\BitrixModels\Models\UserModel implements SortInterface, SelectInterface
{
    use TUserModel, TSort, TSelect;

    protected static $sort = ['UF_SORT' => 'ASC'];
    protected static $select = ['FIELDS', 'PROPS'];

    public static $fetchUsing = [
        'method' => 'GetNext',
        // эти параметры будут переданы в GetNext при вызове. $bTextHtmlAuto, $useTilda
        // $bTextHtmlAuto - преобразовывать в html-безопасный вид
        // $useTilda - с ключом ~ хранить оригинальные значения
//        'params' => [true, false],
        'params' => [true, true],
    ];

    protected static $fieldNamePattern = '/^UF_(.*)$/';
}
