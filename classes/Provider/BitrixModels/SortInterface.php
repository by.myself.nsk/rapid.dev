<?php

namespace Rapid\Dev\Provider\BitrixModels;

interface SortInterface
{
    public static function getSort();
}
