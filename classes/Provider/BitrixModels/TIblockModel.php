<?php

namespace Rapid\Dev\Provider\BitrixModels;

use Illuminate\Support\Str;
use Rapid\Dev\Helper\Iblock\Iblock;
use Rapid\Dev\Helper\Site;
use Rapid\Dev\Helper\SitePage;

trait TIblockModel
{
    use TModel;

    protected static $siteId;
    protected static $entityDisplayName;
    protected static $mapping = [
//        'name' => 'NAME',
    ];

    /**
     * @return mixed
     */
    public static function getSiteId()
    {
        return static::$siteId ?: (SitePage::isAdmin() ? Site::getSiteByLangId(LANGUAGE_ID) : SITE_ID);
    }

    /**
     * @param $siteId
     */
    public static function setSiteId($siteId)
    {
        static::$siteId = $siteId;
    }

    /**
     * @return mixed
     */
    public static function getIblockCode()
    {
        return static::$iblockCode;
    }

    /**
     * @param mixed $iblockCode
     */
    public static function setIblockCode($iblockCode)
    {
        static::$iblockCode = $iblockCode;
    }

    public static function iblockId($siteId = false)
    {
        return Iblock::get(static::$iblockCode, $siteId ?: static::getSiteId());
    }

    public static function getEntityDisplayName()
    {
        return static::$entityDisplayName;
    }

    public function toArray()
    {
        $array = parent::toArray();

        if (is_iterable(static::$mapping)) {
            foreach (static::$mapping as $external => $internal) {
                $key = self::findAttributeKey($internal);
                if (!$key) {
                    if ($this->offsetExists($external)) {
                        $array[$external] = $this->getAccessor($external);
                    }
                    continue;
                }
                $array[$external] = $array[$key];
                unset($array[$internal]);
            }
        }

        return $array;
    }

    public function getAccessor($field)
    {
        $method = 'get' . studly_case($field) . 'Attribute';

        return method_exists($this, $method) ? $this->$method($field) : false;
    }

    public static function getOffsetFromMapping($offset)
    {
        return static::$mapping[$offset];
    }

    public function offsetGet($offset)
    {
        if (array_key_exists($offset, static::$mapping)) {
            $offset = static::$mapping[$offset];
        } else {
            $offset = self::findAttributeKey($offset);
        }
        return parent::offsetGet($offset);
    }

    public function offsetSet($offset, $value)
    {
        if (array_key_exists($offset, static::$mapping)) {
            $offset = static::getOffsetFromMapping($offset);
        } else {
            $offset = self::findAttributeKey($offset);
        }
        parent::offsetSet($offset, $value);
    }
}
