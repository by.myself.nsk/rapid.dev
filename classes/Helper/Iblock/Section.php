<?php

namespace Rapid\Dev\Helper\Iblock;

use CIBlockSection;
use CPHPCache;
use CUserFieldEnum;
use CUserTypeEntity;
use Exception;
use Rapid\Dev\Helper\Traits\AddUpdate;

class Section
{
    use AddUpdate;

    public static function getUserFieldEnum($iblockCode, $strUserFieldCode, $groupKey = 'ID', $valueCode = '')
    {
        $iblockId = intval($iblockCode) ?: Iblock::get($iblockCode);
        if (!$iblockId) {
            return false;
        }
        $obCache = new CPHPCache();
        $cacheID = $iblockId.'_'.$strUserFieldCode.'_'.$groupKey.'_'.$valueCode;
        $cachePath = '/my_section_enum/'.$cacheID;
        $arEnum = array();
        if ($obCache->InitCache(36000, $cacheID, $cachePath)) {
            $arEnum = $obCache->GetVars();
        } elseif ($obCache->StartDataCache()) {
            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache($cachePath);
            $strEntityId = 'IBLOCK_'.$iblockId.'_SECTION';
            if (strpos($strUserFieldCode, 'UF_') !== 0) {
                $strUserFieldCode = 'UF_'.$strUserFieldCode;
            }
            $arUserField = CUserTypeEntity::GetList(
                array(),
                array(
                    'ENTITY_ID' => $strEntityId,
                    'FIELD_NAME' => $strUserFieldCode
                )
            )->Fetch();
            if (!$arUserField) {
                $obCache->AbortDataCache();
                return false;
            }
            $obEnum = new CUserFieldEnum();
            $dbUserFieldValues = $obEnum->GetList(
                array(),
                array(
                    'USER_FIELD_ID' => $arUserField['ID'],
                )
            );
            $arEnum = array();
            while ($arUserFieldValue = $dbUserFieldValues->Fetch()) {
                if (empty($valueCode)) {
                    $arEnum[$arUserFieldValue[$groupKey]] = $arUserFieldValue;
                } else {
                    $arEnum[$arUserFieldValue[$groupKey]] = $arUserFieldValue[$valueCode];
                }
            }
            $CACHE_MANAGER->RegisterTag('iblock_id_'.$iblockId);
            $CACHE_MANAGER->RegisterTag("iblock_id_new");
            $CACHE_MANAGER->EndTagCache();
            $obCache->EndDataCache($arEnum);
        }
        return $arEnum;
    }

    /**
     * @param $arData
     * @param bool $intId
     * @param bool|CIBlockSection $obData
     * @return bool
     * @throws Exception
     */
    public static function addUpdate($arData, $intId = false, &$obData = false)
    {
        if (!is_object($obData)) {
            $obData = new CIBlockSection();
        }
        return self::_addUpdate($obData, $arData, $intId);
    }

}
