<?php

namespace Rapid\Dev\Helper\Iblock;

use CIBlock;
use CSite;
use Rapid\Dev\Helper\Cache;
use Rapid\Dev\Helper\SitePage;

class Iblock
{

    /**
     * Массив со списком всех инфоблоков
     * @var null|array
     */
    private static $arIblocks = null;

    /**
     * Формирование списка всех инфоблоков [код => ид] с кешированием
     */
    private static function getAll()
    {
        $obCache = new Cache(__METHOD__);
        self::$arIblocks = $obCache->get(function () {
            global $CACHE_MANAGER;
            $CACHE_MANAGER->RegisterTag('iblock_id_new');
            $arIblocks = array();
            $res = CIBlock::GetList(
                array('SORT' => 'ASC'),
                array('CHECK_PERMISSIONS' => 'N')
            );
            while ($arItem = $res->Fetch()) {
                $CACHE_MANAGER->RegisterTag('iblock_id_'.$arItem['ID']);
                $res1 = CIBlock::GetSite($arItem['ID']);
                while ($arSite = $res1->Fetch()) {
                    $arIblocks[$arSite['LID']][ToUpper($arItem['CODE'])] = $arItem['ID'];
                }
            }

            return $arIblocks;
        });

        return self::$arIblocks;
    }

    public static function getAllForLang($LID)
    {
        $data = self::getAll();
        return $data[$LID];
    }

    /**
     * Получение ИД инфоблока по коду
     * @param string $code Символьный код инфоблока
     * @param bool|string $LID
     * @return int
     */
    public static function get($code, $LID = false)
    {
        $code = ToUpper($code);
        if (self::$arIblocks === NULL) {
            self::getAll();
        }
        if (!$LID) {
            $LID = SitePage::isAdmin() ? CSite::GetDefSite() : SITE_ID;
        }
        if (array_key_exists($LID, self::$arIblocks) && array_key_exists($code, self::$arIblocks[$LID])) {
            return intval(self::$arIblocks[$LID][$code]);
        }
        return 0;
    }

    public static function checkRequiredCode($iblockCode)
    {
        $iblockCode = intval($iblockCode) ?: Iblock::get($iblockCode);
        $arIblock = CIBlock::GetArrayByID(
            $iblockCode,
            'FIELDS'
        );
        return $arIblock['CODE']['IS_REQUIRED'] == 'Y';
    }

    public static function flush()
    {
        self::$arIblocks = null;
    }
}
