<?php

namespace Rapid\Dev\Helper\Iblock;

use CIBlock;
use CIBlockElement;
use CIBlockProperty;
use CIBlockPropertyEnum;
use CUtil;
use Exception;
use Rapid\Dev\Helper\Cache;
use Rapid\Dev\Helper\Traits\AddUpdate;

class Element
{
    use AddUpdate;

    /**
     * Добавление или редактирование элемента с включенным документооборотом
     * @param int $elementID ИД элемента
     * @param array $arInpData Массив значений элемента
     * @param string $elementName Название сущности в именнительном падеже
     * @return int $elementID Ид нового или редактируемого элемента
     * @throws Exception
     */
    static public function save($elementID = 0, $arInpData = array(), $elementName = 'элемент')
    {
        global $USER;
        $el = new CIBlockElement;
        if ($elementID > 0) {
            $arInpData['MODIFIED_BY'] = $USER->GetID();

            if (!$el->Update($elementID, $arInpData, true)) {
                throw new Exception('Не удалось обновить ' . $elementName . '.');
            }
        } else {
            $arInpData['CREATED_BY'] = $USER->GetID();

            if (!$elementID = $el->Add($arInpData, true)) {
                throw new Exception('Не удалось добавить ' . $elementName . '.');
            }
        }

        return $elementID;
    }

    /**
     * Получение списка значений из свойства элемента типа "Список"
     * @param string $iblockCode Код инфоблока
     * @param string $propertyCode Код свойства
     * @param string $groupKey Код поля для ключа массива
     * @param string $valueCode Код поля для значения массива
     * @return array
     */
    static public function getPropertyEnum($iblockCode, $propertyCode, $groupKey = 'ID', $valueCode = '')
    {
        $iblockId = intval($iblockCode) ?: Iblock::get($iblockCode);
        if (!$iblockId) {
            return array();
        }
        $obCache = new Cache(__METHOD__, $iblockCode, $propertyCode, $groupKey, $valueCode);
        $arProperties = $obCache->get(function () use (
            $iblockId,
            $propertyCode,
            $groupKey,
            $valueCode
        ) {
            global $CACHE_MANAGER;
            $CACHE_MANAGER->RegisterTag('iblock_id_'.$iblockId);
            $arProperties = array();
            $res = CIBlockPropertyEnum::GetList(
                array('SORT' => 'ASC'),
                array('IBLOCK_ID' => $iblockId, 'CODE' => $propertyCode)
            );
            while ($arItem = $res->GetNext()) {
                if (empty($valueCode)) {
                    $arProperties[$arItem[$groupKey]] = $arItem;
                } else {
                    $arProperties[$arItem[$groupKey]] = $arItem[$valueCode];
                }
            }

            return $arProperties;
        });

        return $arProperties;
    }

    /**
     * Получение информации о свойсвте
     * @param string $iblockCode Код инфоблока
     * @param string $propertyCode Код свойства
     * @return array
     */
    static public function getPropertyInfo($iblockCode, $propertyCode)
    {
        $obCache = new Cache(__METHOD__, $iblockCode, $propertyCode);
        $arProperty = $obCache->get(function () use ($iblockCode, $propertyCode) {
            global $CACHE_MANAGER;
            $intIblockId = Iblock::get($iblockCode);
            $CACHE_MANAGER->RegisterTag('iblock_id_'.$intIblockId);
            $arProperty = array();
            $res = CIBlockProperty::GetList(
                array('SORT' => 'ASC'),
                array('IBLOCK_ID' => $intIblockId, 'CODE' => $propertyCode)
            );
            if ($arItem = $res->GetNext()) {
                $arProperty = $arItem;
            }

            return $arProperty;
        });

        return $arProperty;
    }

    /**
     * Получение дефолтного значения для свойства элемента
     * @param string $iblockCode Код инфоблока
     * @param string $propertyCode Код свойства
     * @return string
     */
    static public function getDefaultPropertyValue($iblockCode, $propertyCode)
    {
        $defaultValue = '';
        $arProperty = self::getPropertyInfo($iblockCode, $propertyCode);
        if ($arProperty) {
            // Если это список, то надо получить весь список и найти дефолтное значение
            if ($arProperty['~PROPERTY_TYPE'] == 'L') {
                $arPropertyList = self::getPropertyEnum($iblockCode, $propertyCode);
                foreach ($arPropertyList as $arProp) {
                    if ($arProp['DEF'] == 'Y') {
                        $defaultValue = $arProp['ID'];
                        break;
                    }
                }
            } else {
                $defaultValue = $arProperty['~DEFAULT_VALUE'];
            }
        }
        unset($res, $arProperty);

        return $defaultValue;
    }

    public static function generateUniqueCode($name, $iblockCode, $intExceptId = false, $arTranslitParams = array())
    {
        $iblockCode = intval($iblockCode) ?: Iblock::get($iblockCode);
        if (!is_array($arTranslitParams)) {
            $arTranslitParams = array();
        }
        $arTranslitParams = array_merge(
            array(
                "max_len" => 100,
                "change_case" => 'L',   // 'L' - toLower, 'U' - toUpper, false - do not change
                "replace_space" => '-',
                "replace_other" => '-',
                "delete_repeat_replace" => true,
            ),
            $arTranslitParams
        );
        $code = CUtil::translit($name, LANGUAGE_ID, $arTranslitParams);
        $suffix = '';
        $i = 0;
        while (self::getByCode($code . $suffix, $iblockCode, $intExceptId)) {
            $i++;
            $suffix = '-' . $i;
        }
        return $code . $suffix;
    }

    public static function getByCode($code, $iblockCode, $intExceptId = false)
    {
        $iblockCode = (int)$iblockCode ?: Iblock::get($iblockCode);
        return CIBlockElement::GetList(
            [],
            [
                'IBLOCK_ID' => $iblockCode,
                '!ID' => $intExceptId,
                'CODE' => $code
            ]
        )->Fetch();
    }

    /**
     * Получение простого списка элементов из инфоблока
     * @param string $iblockCode Код инфоблока
     * @param array $ids Массив ИД элементов для точной выборки
     * @param array $arrSelect Массив дополнительно возвращаемых полей
     * @return array
     */
    static public function getSimpleList($iblockCode, $ids = array(), $arrSelect = array())
    {
        $arItems = array();
        $arFilter = array('IBLOCK_ID' => Iblock::get($iblockCode), 'ACTIVE' => 'Y', 'PROPERTY_DELETE' => false);
        if (!empty($ids)) {
            $arFilter['ID'] = $ids;
        }
        $arSelect = array('ID', 'NAME', 'DETAIL_PAGE_URL', 'PROPERTY_CLIENT', 'PROPERTY_PROJECT', 'PROPERTY_MANAGERID');
        $res = CIBlockElement::GetList(
            array('NAME' => 'ASC'),
            $arFilter,
            false,
            false,
            array_merge($arSelect, $arrSelect)
        );
        while ($arItem = $res->GetNext()) {
            if (!is_array($ids)) {
                return $arItem;
            } else {
                $arItems[$arItem['ID']] = $arItem;
            }
        }

        return $arItems;
    }

    /**
     * @param $arData
     * @param bool $intId
     * @param bool|CIBlock $obData
     * @return bool
     * @throws Exception
     */
    public static function addUpdate($arData, $intId = false, &$obData = false)
    {
        if (!is_object($obData)) {
            $obData = new CIBlockElement();
        }
//        $arPropValues = $arData['PROPERTY_VALUES'];
//        unset($arData['PROPERTY_VALUES']);
        $intId = static::_addUpdate($obData, $arData, $intId);
        // остается проблема с обработчиками On.*IblockElement[Add|Update]
//        if ($intId && $arPropValues) {
//            CIBlockElement::SetPropertyValuesEx($intId, $arData['IBLOCK_ID'], $arPropValues);
//        }
        return $intId;
    }

}
