<?php

namespace Rapid\Dev\Helper;

use CUtil;

class MyString
{
    /**
     * Функция возвращает обработанный текст
     * @param string $text
     * @param bool $bSpecialChars
     * @return array|string
     */
    static public function textProcessing(&$text, $bSpecialChars = true)
    {
        if (is_array($text)) {
            array_walk($text, array(__CLASS__, 'textProcessing'));
        } else {
            $text = strip_tags($text);
            if ($bSpecialChars) {
                $text = htmlspecialchars($text);
            }
            $text = trim($text);
        }
        return $text;
    }

    /**
     * Функция склонения числительных в русском языке
     * @param int $number
     * @param array $titles
     * @param bool $withnum
     * @return string
     */
    static public function declineWord($number = 0, $titles = array(), $withnum = true, $lang = false)
    {
        if (!$lang) {
            $lang = LANGUAGE_ID;
        }
        $resNum = $number;
        if ($lang == 'ru') {
            if (floatval($number) != intval($number)) {
                $pos = 1;
            } else {
                $number = abs($number);
                if (!isset($titles[2])) {
                    $titles[2] = $titles[1];
                }
                $cases = array(2, 0, 1, 1, 1, 2);
                $pos = ($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)];
            }
        } else {
            $pos = ($number == 1) ? 0 : 1;
        }
        return (($withnum) ? $resNum . ' ' : '') . $titles[$pos];
    }

    public static function intval($strText)
    {
        $res = preg_replace('/[^\d]/', '', $strText);
        return is_numeric($res) ? intval($res) : $res;
    }

    public static function floatval($strText)
    {
        $res = preg_replace('/[^\d,.-]/', '', $strText);
        return is_numeric($res) ? floatval($res) : $res;
    }

    /**
     * Формат вывода денежных единиц
     * @param $price
     * @param int $decimals
     * @param string $dec_point
     * @param string $thousands_sep
     * @return string
     */
    static public function priceFormat($price, $decimals = 0, $dec_point = ',', $thousands_sep = ' ')
    {
        $price = self::floatval($price);
        return number_format($price, $decimals, $dec_point, $thousands_sep);
    }

    /**
     * Формат вывода денежных единиц с суффиксом
     * @param float $price Цена
     * @param int $intPrecision точность округления
     * @param bool $bRoundUp флаг округления вверх
     * @param array $arSuffixes Массив суффиксов цены. Первый элемент пустой, если единицы не указываются
     * @param $strLanguage
     * @return string
     */
    static public function priceFormatSuffix($price, $intPrecision = 1, $bRoundUp = false, $arSuffixes = array(), $strLanguage = LANGUAGE_ID)
    {
        $price = self::floatval($price);
        $intPrecision = intval($intPrecision);
        if (!is_array($arSuffixes) || empty($arSuffixes)) {
            switch (ToLower($strLanguage)) {
                default:
                case 'ru':
                    $arSuffixes = array(
                        '',
                        'т.',
                        'млн.',
                        'млрд.',
                    );
                    break;
                case 'en':
                    $arSuffixes = array(
                        '',
                        'k',
                        'm',
                        'Bn',
                    );
                    break;
            }
        }
        $intSuffixIndexMax = count($arSuffixes) - 1;
        $intSuffixIndex = 0;
        while ($price > 10 && $intSuffixIndex < $intSuffixIndexMax) {
            $price /= 1000;
            $intSuffixIndex++;
        }
        $mode = PHP_ROUND_HALF_DOWN;
        if ($bRoundUp) {
            $mode = PHP_ROUND_HALF_UP;
        }
        return round($price, $intPrecision, $mode) . ' ' . $arSuffixes[$intSuffixIndex];
    }

    /**
     * Форматирование телефона для ссылки
     * @param string $phone
     * @return string
     */
    static public function phoneLinkFormat($phone)
    {
        return preg_replace(
            ['/[^\d]*доб[^\d]*/i', '/[^0-9+,]/'],
            [',', ''],
            $phone
        );
    }

    /**
     * Заменяем br на пробел
     * @param string $string
     * @return string
     */
    static public function removeBr($string)
    {
        $string = str_replace('<br>', ' ', $string);
        $string = str_replace('<br/>', ' ', $string);
        $string = preg_replace("/[\s]+/", ' ', $string);

        return $string;
    }

    /**
     * Транслитерация строки
     * @param string $string
     * @return string
     */
    static public function translitString($string = '')
    {
        $params = array(
            'max_len' => '100',
            'change_case' => 'L',
            'replace_space' => '-',
            'replace_other' => '-',
            'delete_repeat_replace' => true,
        );

        return CUtil::translit($string, 'ru', $params);
    }

    /**
     * Очищает текст от html<br>Удобно, например, для вывода названия раздела/элемента без ссылки на него
     * @param $val
     * @return string
     */
    public static function valueClear($val)
    {
        return strip_tags(htmlspecialchars_decode($val));
    }

    /**
     * Форматирование кавычек у цитат
     * @param $text
     * @return mixed
     */
    static public function formatQuote($text)
    {
        $tiny = array('ни', 'не', 'и', 'но', 'а', 'или', 'да', 'как',
            'из-за', 'про', 'по', 'за', 'для', 'на',
            'до', 'при', 'меж', 'о', 'у', 'в', 'во',
            'с', 'со', 'от', 'ото', 'из', 'без', 'безо',
            'к', 'ко', 'об', 'обо', 'под', 'подо',
            'над', 'перед', 'передо');

        $text = preg_replace('~(\s)«~', '<span class="hpspace"> </span><span class="hpquote">«</span>', $text);
        $text = preg_replace('~[^>]«~', '<span class="hpquote">«</span>', $text);
        $text = preg_replace('~«~', '<span class="hpquote">«</span>', $text);
        $text = preg_replace('~(' .
            implode('|', $tiny) .
            ')\s<span class="hpspace"> </span><span class="hpquote">«</span>~',
            '\1 «', $text);

        return $text;
    }

    public static function isEmail($email)
    {
        if (!preg_match('|([a-z0-9_\.\-]{1,20})@([a-z0-9\.\-]{1,20})\.([a-z]{2,4})|is', $email)) {
            return false;
        }
        $strDomain = end(explode("@", $email));
        if (getmxrr($strDomain, $MXHost)) {
            return true;
        } else {
            return @fsockopen($strDomain, 25, $errno, $errstr, 30);
        }
    }

    public static function isPropertyTextHtml($arProperty)
    {
        return $arProperty['PROPERTY_TYPE'] == 'S' && !empty($arProperty['USER_TYPE']) && $arProperty['USER_TYPE'] == 'HTML';
    }

    public static function getDisplayPropertyTextHtml($value)
    {
        if (is_string($value)) {
            return $value;
        }
        if (!is_array($value) || !array_key_exists('TEXT', $value) || !array_key_exists('TYPE', $value)) {
            return false;
        }
        return FormatText($value['TEXT'], $value['TYPE']);
    }

    public static function getPropertyTextHtmlArray($text, $type = 'text')
    {
        $strType = ToUpper($type);
        if (!in_array($strType, ['TEXT', 'HTML'])) {
            $strType = 'TEXT';
        }
        return ['VALUE' => [
            'TEXT' => $text,
            'TYPE' => $strType,
        ]];
    }

    public static function firstCapital($str)
    {
        if (!$str) {
            return $str;
        }
        $str = mb_strtolower($str);
        $fc = mb_strtoupper(mb_substr($str, 0, 1));
        return $fc . mb_substr($str, 1);
    }

    public static function convertSnakeToCamel($strCode)
    {
        $arParts = explode('_', $strCode);
        $bFirst = true;
        foreach ($arParts as $strPart) {
            if ($bFirst) {
                $strCode = ToLower($strPart);
            } else {
                $strCode .= static::firstCapital($strPart);
            }
            $bFirst = false;
        }
        return $strCode;
    }

    public static function startsWith($haystack, $needle)
    {
        $intLength = mb_strlen($needle);
        return mb_substr($haystack, 0, $intLength) == $needle;
    }

    public static function endsWith($haystack, $needle)
    {
        $intLength = strlen($needle);
        return mb_substr($haystack, -$intLength) == $needle;
    }

    public static function cutAtStart($haystack, $needle)
    {
        $intLength = strlen($needle);
        if (mb_substr($haystack, 0, $intLength) != $needle) {
            return false;
        }
        return mb_substr($haystack, $intLength);
    }

    public static function cutAtEnd($haystack, $needle)
    {
        $intLength = strlen($needle);
        if (mb_substr($haystack, -$intLength) != $needle) {
            return false;
        }
        return mb_substr($haystack, 0, -$intLength - 1);
    }


    /**
     * Перевод десятичных координат в градусы
     * @param float $decVal Десятичная координата
     * @return string
     */
    static public function dec2dms($decVal)
    {
        $degrees = intval($decVal);
        $minutes = ($decVal - $degrees) * 60;
        $second = (($minutes - intval($minutes)) * 60);

        return $degrees . '°' . intval($minutes) . '’' . round($second, 2) . '”';
    }

    /**
     * Разбор координат из строки (долгота, широта)
     * @param string $coords Строка с координатами
     * @param string $delimiter Разделитель координат
     * @return array
     */
    static public function parseCoords($coords = '', $delimiter = ',')
    {
        $lon = 0;
        $lat = 0;
        if (!empty($coords)) {
            $coords = explode($delimiter, $coords);
            if (isset($coords[0])) {
                $lat = floatval($coords[0]);
            }
            if (isset($coords[1])) {
                $lon = floatval($coords[1]);
            }
        }

        return array(
            'lon' => $lon,
            'lat' => $lat
        );
    }

    /**
     * Конвертация тега <br> в символ переноса строки
     * @param string $text Текст
     * @return string
     */
    static public function br2nl($text)
    {
        $breaks = array('<br />', '<br>', '<br/>');

        return str_ireplace($breaks, "\n", $text);
    }
}
