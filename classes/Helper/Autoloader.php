<?php

namespace Rapid\Dev\Helper;

if (class_exists('Autoloader')) {
    return;
}

class Autoloader
{
    private static $instance;
    private $arPaths = [];

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct()
    {
        spl_autoload_register(__CLASS__.'::loader');
    }

    /**
     * Автозагрузка классов
     * @param string $className
     * @return bool
     */
    static public function loader($className)
    {
        if (class_exists($className)) {
            return true;
        }
        $obLoader = self::getInstance();
        return $obLoader->search($className);
    }

    public function addPath($strPath, $strParentNamespace = '\\')
    {
        $strPath = rtrim($strPath, DIRECTORY_SEPARATOR);
        $strParentNamespace = $this->checkNamespace($strParentNamespace);
        $this->arPaths[$strPath] = $strParentNamespace;
    }

    public function getPaths()
    {
        return $this->arPaths;
    }

    private static function checkNamespace($namespace)
    {
        if (!is_string($namespace)) {
            return '\\';
        }
        $namespace = trim($namespace);
        if (substr($namespace, 0, 1) != '\\') {
            $namespace = '\\' . $namespace;
        }
        if (substr($namespace, -1) != '\\') {
            $namespace = $namespace . '\\';
        }
        return $namespace;
    }

    private function search($className)
    {
        if (!$this->hasPaths()) {
            return false;
        }
        foreach ($this->arPaths as $strPath => $strNamespace) {
            $filename = $strPath
                . self::convertClassNameToDirectoryPart($className, $strNamespace);
            if (self::tryInclude($filename, $className)) {
                return true;
            }
        }
        return false;
    }

    private function hasPaths()
    {
        return is_array($this->arPaths) && !empty($this->arPaths);
    }

    private static function convertClassNameToDirectoryPart($className, $namespace)
    {
        $namespace = ltrim($namespace, '\\');
        $namespaceLength = strlen($namespace);
        if (substr($className, 0, $namespaceLength) === $namespace) {
            $className = substr($className, $namespaceLength);
        }
        return DIRECTORY_SEPARATOR . str_replace(['_', '\\'], DIRECTORY_SEPARATOR, $className) . '.php';
    }

    /**
     * Попытаться подключить файл с классом
     * @param string $filename путь к файлу с классом
     * @param string $className имя класса
     * @return bool
     */
    private static function tryInclude($filename, $className)
    {
        if (file_exists($filename)) {
            /** @noinspection PhpIncludeInspection */
            include_once($filename);
            if (class_exists($className)) {
                return true;
            }
        }
        return false;
    }
}
