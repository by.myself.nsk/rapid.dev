<?php

namespace Rapid\Dev\Helper;

use Bitrix\Main\Page\Asset;

class AssetManager
{
    protected static $salt = '';
    protected static $checkedUrls = [];

    public const TYPE_BX_D7 = 1; // вариант самый верный, но битрикс бажит
    public const TYPE_BX_OLD = 2; // вариант олдовый
    public const TYPE_HTML_TAGS_GET = 3; // вариант самый примитивный
    public const TYPE_HTML_TAGS_SHOW = 4; // вариант самый примитивный
    public const TYPE_HTML_TAGS_BUFFERED = 5; // вариант самый примитивный, но с отложенным выводом

    protected static $type = self::TYPE_HTML_TAGS_BUFFERED;

    protected static $cssUrls = [];
    protected static $jsUrls = [];

    /**
     * @return string
     */
    public static function getSalt(): string
    {
        return self::$salt;
    }

    /**
     * @param string $salt
     */
    public static function setSalt(string $salt): void
    {
        self::$salt = $salt;
    }

    /**
     * @return int
     */
    public static function getType(): int
    {
        return self::$type;
    }

    /**
     * @param int $type
     */
    public static function setType(int $type): void
    {
        self::$type = $type;
    }


    public static function addCss($url, $type = false)
    {
        if (!$type) {
            $type = self::getType();
        }
        switch ($type) {
            case self::TYPE_BX_D7:
                Asset::getInstance()->addCss($url);
                break;
            case self::TYPE_BX_OLD:
                global $APPLICATION;
                $APPLICATION->SetAdditionalCSS($url);
                break;
            case self::TYPE_HTML_TAGS_GET:
                return '<link rel="stylesheet" type="text/css" href="' . $url . '" media="all">';
                break;
            case self::TYPE_HTML_TAGS_SHOW:
                echo self::addCss($url, self::TYPE_HTML_TAGS_GET);
                break;
            case self::TYPE_HTML_TAGS_BUFFERED:
                self::$cssUrls[] = $url;
                break;
        }
    }

    public static function addJs($url, $type = false)
    {
        if (!$type) {
            $type = self::getType();
        }
        switch ($type) {
            case self::TYPE_BX_D7:
                Asset::getInstance()->addJs($url);
                break;
            case self::TYPE_BX_OLD:
                global $APPLICATION;
                $APPLICATION->AddHeadScript($url);
                break;
            case self::TYPE_HTML_TAGS_GET:
                return '<script src="' . $url . '"></script>';
                break;
            case self::TYPE_HTML_TAGS_SHOW:
                echo self::addJs($url, self::TYPE_HTML_TAGS_GET);
                break;
            case self::TYPE_HTML_TAGS_BUFFERED:
                self::$jsUrls[] = $url;
                break;
        }
    }

    public static function showCssBuffered()
    {
        global $APPLICATION;
        echo $APPLICATION->AddBufferContent([self::class, 'showCssImmediately']);
    }

    public static function showCssImmediately()
    {
        $return = [];
        foreach (self::$cssUrls as $url) {
            $return[] = self::addCss($url, self::TYPE_HTML_TAGS_GET);
        }
        return implode("\n", $return);
    }

    public static function showJsBuffered()
    {
        global $APPLICATION;
        echo $APPLICATION->AddBufferContent([self::class, 'showJsImmediately']);
    }

    public static function showJsImmediately()
    {
        $return = [];
        foreach (self::$jsUrls as $url) {
            $return[] = self::addJs($url, self::TYPE_HTML_TAGS_GET);
        }
        return implode("\n", $return);
    }

    public static function addString($string = null)
    {
        if (!$string) {
            return;
        }
        Asset::getInstance()->addString($string);
    }

    /**
     * Генерация CRC32 хеша для директории
     * @param string $path Директория для хеширования
     * @return string Захешированное название директории
     */
    protected static function hash($path)
    {
        return sprintf('%x', crc32($path . self::$salt));
    }

    public static function checkUrlForHash($url)
    {
        $paramsPos = strrpos($url, '?');
        $params = [];
        $urlWithoutParams = $url;
        if ($paramsPos !== false) {
            $urlWithoutParams = substr($url, 0, $paramsPos);
            $params = explode('&', substr($url, $paramsPos + 1));
        }
        $real_path = $urlWithoutParams;
        if (strpos($_SERVER['DOCUMENT_ROOT'], $real_path) === false) {
            $real_path = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . ltrim($real_path, '/');
        }

        if (self::$checkedUrls[$real_path]) {
            return self::$checkedUrls[$real_path];
        }
        if (($src = realpath($real_path)) !== false) {
            $hash = self::hash(dirname($src) . filemtime($src));
            $params[] = 'ts=' . $hash;
            $resultUrl = $urlWithoutParams . '?' . implode('&', $params);
        } else {
            $resultUrl = $url;
        }
        self::$checkedUrls[$real_path] = $resultUrl;
        return $resultUrl;
    }
}
