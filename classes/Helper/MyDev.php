<?php

namespace Rapid\Dev\Helper;

use Bitrix\Main\Localization\Loc;
use CFavorites;
use CFileMan;
use CIBlockProperty;
use CIblockType;
use CModule;

class MyDev
{
    public static function reInstallModule($strModuleID = 'rapid.dev', $strRoot = false)
    {
        $strModuleID = trim($strModuleID);
        if (!strlen($strModuleID)) {
            return false;
        }
        $arRoots = array(
            'local',
            'bitrix',
        );
        if (!$strRoot && is_array($arRoots) && !empty($arRoots)) {
            $bReinstalled = false;
            foreach ($arRoots as $strRootItem) {
                if ($bReinstalled = self::reInstallModule($strModuleID, $strRootItem)) {
                    break;
                }
            }
            return $bReinstalled;
        } elseif (!is_string($strRoot) || !in_array($strRoot, $arRoots)) {
            return false;
        } else {
            $strPathToModule = $_SERVER['DOCUMENT_ROOT'] . '/' . $strRoot . '/modules/' . $strModuleID . '/install/index.php';
            if (!file_exists($strPathToModule)) {
                return false;
            }
            /** @noinspection PhpIncludeInspection */
            include_once $strPathToModule;
            $strModuleClassName = str_replace('.', '_', $strModuleID);
            /** @var CModule $obModule */
            $obModule = new $strModuleClassName();
            if (IsModuleInstalled($strModuleID)) {
                $obModule->DoUninstall();
            }
            $obModule->DoInstall();
            return true;
        }
    }

    /**
     * Находит множественные свойства, хранящиеся в общей таблице свойств, и возвращает массив с ID инфоблоков и ID множественных свойств в них
     * @return array
     */
    public static function findWrongPropertyStorage()
    {
        $arWrongProperties = array();
        $dbWrongProperties = CIBlockProperty::GetList(array(), array('MULTIPLE' => 'Y', 'VERSION' => 1));
        while ($arWrongProperty = $dbWrongProperties->Fetch()) {
            $arWrongProperties[$arWrongProperty['IBLOCK_ID']][$arWrongProperty['ID']] = $arWrongProperty['NAME'];
        }
        return $arWrongProperties;
    }

    public static function clearCache()
    {
        BXClearCache(true);
        $GLOBALS["CACHE_MANAGER"]->CleanAll();
        $GLOBALS["stackCacheManager"]->CleanAll();
        $staticHtmlCache = \Bitrix\Main\Data\StaticHtmlCache::getInstance();
        $staticHtmlCache->deleteAll();
    }

    /**
     * Метод добавляет в избранное ссылки, указанные в массиве
     * @param array $arLinks Массив со ссылками. Ключ - ссылка, значение - название
     * @param bool|string $lang Язык админки, для которого будет создана ссылка. Если false, будет создана для всех языков
     * @param bool $bCheckDuplicate Параметр метода CFavorites::Add. При установленном в true проверяется дублирование методом IsExistDuplicate.
     * Если совпал хотя бы один из параметров: MENU_ID, URL или NAME, дублирование найдено
     * @return bool
     */
    public static function addAdminFavorites($arLinks, $lang = false, $bCheckDuplicate = false)
    {
        if (!is_array($arLinks) || empty($arLinks)) {
            return false;
        }
        global $USER;
        $arLangs = is_string($lang) && !empty($lang) ? array($lang) : Site::getLangList('LID');
        $bAdded = false;
        foreach ($arLinks as $strUrl => $strName) {
            foreach ($arLangs as $strLang) {
                $bAdded = $bAdded ?: CFavorites::Add(
                    array(
                        'NAME' => $strName,
                        'URL' => $strUrl,
                        'LANGUAGE_ID' => $strLang,
                        'COMMON' => 'N',
                        'USER_ID' => $USER->GetID(),
                        'CREATED_BY' => $USER->GetID(),
                        'MODIFIED_BY' => $USER->GetID(),
                        'LOGIN' => $USER->GetLogin(),
                        'C_LOGIN' => $USER->GetLogin(),
                        'M_LOGIN' => $USER->GetLogin(),
                        'USER_NAME' => $USER->GetFullName(),
                        'C_USER_NAME' => $USER->GetFullName(),
                        'M_USER_NAME' => $USER->GetFullName(),
                        'TIMESTAMP_X' => ConvertTimeStamp(false, 'FULL'),
                        'DATE_CREATE' => ConvertTimeStamp(false, 'FULL'),
                    ),
                    $bCheckDuplicate
                );
            }
        }
        return $bAdded;
    }

    public static function getFavoritesList()
    {
        $dbFavorites = CFavorites::GetList();
        $arFavorites = array();
        while ($arFavorite = $dbFavorites->Fetch()) {
            $arFavorites[] = $arFavorite;
        }
        return $arFavorites;
    }
}
