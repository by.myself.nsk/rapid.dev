<?php

namespace Rapid\Dev\Helper;

/**
 * Class Benchmark
 *
 * @usage
 * Benchmark::mark('first');
 * // CODE
 * Benchmark::mark('second');
 * echo "Затрачено времени: Benchmark::getTimeUsage('first', 'second')";
 * echo "Затраты памяти в точке 'first': Benchmark::getMemoryUsage('first')";
 * echo "Затраты памяти в точке 'second': Benchmark::getMemoryUsage('second')";
 */

class Benchmark
{

    protected static $PRECISION = 2;
    protected static $arMarks = array();

    public static function setPrecision($intPrecision)
    {
        self::$PRECISION = $intPrecision;
    }

    public static function addMark($strMarkName)
    {
        self::$arMarks[$strMarkName] = array(
            'NAME' => $strMarkName,
            'TIME' => microtime(true),
//            'MEMORY' => memory_get_usage(),
        );
    }

    public static function formatTime($fTime, $intPrecision)
    {
        $intNamePos = 0;
        $arNames = array(
            'с',
            'мс',
            'мкс',
            'нс',
        );
        while ($fTime < 1) {
            $fTime *= 1000;
            $intNamePos++;
        }
        return round($fTime, $intPrecision).' '.$arNames[$intNamePos];
    }

    public static function getTimeUsage($strOldMark, $strNewMark, $format = true)
    {
        $fTimeDiff = self::$arMarks[$strNewMark]['TIME'] - self::$arMarks[$strOldMark]['TIME'];
        return $format ? self::formatTime($fTimeDiff, self::$PRECISION) : $fTimeDiff;
    }

    public static function getMemoryUsage($strMarkName = '')
    {
        if (!isset(self::$arMarks[$strMarkName]['memory'])) {
//            $intMemory = memory_get_usage();
            $intMemory = 0;
        } else {
            $intMemory = self::$arMarks[$strMarkName]['memory'];
        }
        return File::formatSize($intMemory);
    }

    public static function getDiffMemoryUsage($strOldMark, $strNewMark)
    {
        $intMemoryDiff = self::$arMarks[$strNewMark]['memory'] - self::$arMarks[$strOldMark]['memory'];
        return File::formatSize($intMemoryDiff);
    }

    public static function printStatistics()
    {
        $arLastMark = false;
        $arFirstMark = false;

        $mFirst = reset(self::$arMarks);
        $mLast = end(self::$arMarks);
        $totalTime = self::getTimeUsage($mFirst['NAME'], $mLast['NAME'], false);

        ?>
        <table border="1">
            <tr>
                <th>метки</th>
                <th>время</th>
                <th>%</th>
            </tr>
            <?
            foreach (self::$arMarks as &$arMark) {
                if (!$arLastMark) {
                    $arFirstMark = $arLastMark = &$arMark;
                    continue;
                }
                ?>
                <tr>
                    <td><?=$arLastMark['NAME'].' - '.$arMark['NAME']?></td>
                    <td><?=self::getTimeUsage($arLastMark['NAME'], $arMark['NAME'])?></td>
                    <td><?
                        $currentTime = self::getTimeUsage($arLastMark['NAME'], $arMark['NAME'], 0);
                        $percent = $totalTime ? $currentTime / $totalTime * 100 : 0;
                        echo round($percent, self::$PRECISION);
                        ?></td>
                </tr>
                <?
                $arLastMark = &$arMark;
            }
            unset($arMark);
            ?>
            <tr>
                <th><?=$arFirstMark['NAME'].' - '.$arLastMark['NAME']?></th>
                <th><?=self::getTimeUsage($arFirstMark['NAME'], $arLastMark['NAME'])?></th>
                <th></th>
            </tr>
        </table>
        <?
    }

    public static function writeStatisticsToFile($dirPath)
    {
        global $APPLICATION;

        $dirPath = rtrim($dirPath, \DIRECTORY_SEPARATOR);
        CheckDirPath($dirPath);
        $dirPath .= DIRECTORY_SEPARATOR;
        $filename = time() . '.html';

        ob_start();
        self::printStatistics();
        $data = ob_get_clean();

        $header = '<html><header></header><body><div>' . $APPLICATION->GetCurDir() . '</div>';
        $footer = '</body></html>';
        file_put_contents($dirPath . $filename, $header . $data . $footer);
    }
}
