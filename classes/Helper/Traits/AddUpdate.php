<?php

namespace Rapid\Dev\Helper\Traits;

use CIBlock;
use CIBlockElement;
use CIBlockProperty;
use CIBlockPropertyEnum;
use CIBlockSection;
use CIBlockType;
use CLanguage;
use CSite;
use CUser;
use CUserTypeEntity;
use Exception;

trait AddUpdate
{
    /**
     * @param CIBlockType|CIBlock|CIBlockElement|CIBlockSection|CIBlockProperty|CIBlockPropertyEnum|CUserTypeEntity|CSite|CLanguage|CUser $object
     * @param $arData
     * @param bool $intId
     * @return bool
     * @throws Exception
     */

    protected static function _addUpdate(&$object, $arData, $intId = false)
    {
        if (!is_array($arData)) {
            return false;
        }
        try {
//            if ($intId > 0) { // у сайта id не числовой
            if ($intId != false) {
                if (!$object->Update($intId, $arData)) {
                    throw new Exception($object->LAST_ERROR);
                }
            } else {
                if (!$intId = $object->Add($arData)) {
                    throw new Exception($object->LAST_ERROR);
                }
            }
            return $intId;
        } catch (Exception $e) {
            return false;
        }
    }
}
