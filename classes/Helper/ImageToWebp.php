<?php

namespace Rapid\Dev\Helper;

use CBXVirtualIo;
use CFile;
use COption;
use Exception;

/**
 * Class ImageToWebp
 * @package Rapid\Dev\Helper
 */
class ImageToWebp
{
    private static $destinationExtension = '.webp';
    private $source = null;
    private $destination = null;
    private $size;
    private $resizeType;
    private $quality;

    public function __construct()
    {
        $this->setQuality(self::getQualityFromOption());
    }

    /**
     * @return string
     */
    private static function getDestinationExtension(): string
    {
        return self::$destinationExtension;
    }

    /**
     * @param null $source
     * @return ImageToWebp
     */
    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return null
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param null $destination
     * @return ImageToWebp
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
        return $this;
    }

    /**
     * @return null
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param mixed $size
     * @return ImageToWebp
     */
    public function setSize($size)
    {
        $this->size = $this->checkSizeArray($size);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $resizeType
     * @return ImageToWebp
     */
    public function setResizeType($resizeType)
    {
        $this->resizeType = $resizeType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResizeType()
    {
        return $this->resizeType;
    }

    /**
     * @param mixed $quality
     * @return ImageToWebp
     */
    public function setQuality($quality)
    {
        $this->quality = $quality;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuality()
    {
        return $this->quality;
    }

    public function getDestinationPathFromSourcePath()
    {
        return self::replaceExtension($this->getSource());
    }

    public static function replaceExtension($path)
    {
        return File::getName($path, false) . self::getDestinationExtension();
    }

    public function setDestinationPathFromSourcePath()
    {
        $this->setDestination($this->getDestinationPathFromSourcePath());
        return $this;
    }

    private static function getQualityFromOption()
    {
        return COption::GetOptionString('main', 'image_resize_quality', 95);
    }

    protected function getImageResource()
    {
        $source = self::checkDocumentRoot($this->source);
        $extension = strtolower(strrchr($source, '.'));
        switch ($extension) {
            case '.jpg':
            case '.jpeg':
                $img = @imagecreatefromjpeg($source);
                break;
            case '.gif':
                $img = @imagecreatefromgif($source);
                break;
            case '.png':
                $img = @imagecreatefrompng($source);
                break;
            default:
                $img = false;
                break;
        }

        if ($img) {
            imagepalettetotruecolor($img);
            imagealphablending($img, true);
            imagesavealpha($img, true);
        }
        return $img;
    }

    private function resize($resource)
    {
        // основано на методе CFile::ResizeImageFile()
        $source = self::checkDocumentRoot($this->source);
        $arSourceFileSizeTmp = CFile::GetImageSize($source);
        $resizeType = $this->getResizeType();
        CFile::ScaleImage($arSourceFileSizeTmp[0], $arSourceFileSizeTmp[1], $this->getSize(), $resizeType, $bNeedCreatePicture, $arSourceSize, $arDestinationSize);
        if (!$bNeedCreatePicture) {
            return $resource;
        }

        if (CFile::IsGD2()) {
            $picture = ImageCreateTrueColor($arDestinationSize["width"], $arDestinationSize["height"]);
            if ($arSourceFileSizeTmp[2] == IMAGETYPE_PNG) {
                $transparentcolor = imagecolorallocatealpha($picture, 0, 0, 0, 127);
                imagefilledrectangle($picture, 0, 0, $arDestinationSize["width"], $arDestinationSize["height"], $transparentcolor);

                imagealphablending($picture, false);
                imagecopyresampled($picture, $resource, 0, 0, $arSourceSize["x"], $arSourceSize["y"], $arDestinationSize["width"], $arDestinationSize["height"], $arSourceSize["width"], $arSourceSize["height"]);
                imagealphablending($picture, true);
            } elseif ($arSourceFileSizeTmp[2] == IMAGETYPE_GIF) {
                imagepalettecopy($picture, $resource);

                //Save transparency for GIFs
                $transparentcolor = imagecolortransparent($resource);
                if ($transparentcolor >= 0 && $transparentcolor < imagecolorstotal($resource)) {
                    $RGB = imagecolorsforindex($resource, $transparentcolor);
                    $transparentcolor = imagecolorallocate($picture, $RGB["red"], $RGB["green"], $RGB["blue"]);
                    imagecolortransparent($picture, $transparentcolor);
                    imagefilledrectangle($picture, 0, 0, $arDestinationSize["width"], $arDestinationSize["height"], $transparentcolor);
                }

                imagecopyresampled($picture, $resource, 0, 0, $arSourceSize["x"], $arSourceSize["y"], $arDestinationSize["width"], $arDestinationSize["height"], $arSourceSize["width"], $arSourceSize["height"]);
            } else {
                imagecopyresampled($picture, $resource, 0, 0, $arSourceSize["x"], $arSourceSize["y"], $arDestinationSize["width"], $arDestinationSize["height"], $arSourceSize["width"], $arSourceSize["height"]);
            }
        } else {
            $picture = ImageCreate($arDestinationSize["width"], $arDestinationSize["height"]);
            imagecopyresized($picture, $resource, 0, 0, $arSourceSize["x"], $arSourceSize["y"], $arDestinationSize["width"], $arDestinationSize["height"], $arSourceSize["width"], $arSourceSize["height"]);
        }

        return $picture;
    }

    public function convert()
    {
        $dest = self::checkDocumentRoot($this->getDestination());
        CheckDirPath($dest);
        $rs = $this->getImageResource();
        if (!$rs) {
            return false;
        }
        $rs = $this->resize($rs);
        return imagewebp(
            $rs,
            $dest,
            $this->getQuality()
        );
    }

    private static function checkDocumentRoot($path)
    {
        if (strpos($path, $_SERVER['DOCUMENT_ROOT']) === false) {
            $path = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . trim($path, DIRECTORY_SEPARATOR);
        }
        return $path;
    }

    public static function resizeImageGet($file, $arSize, $resizeType = BX_RESIZE_IMAGE_PROPORTIONAL, $quality = false)
    {
        // основано на CFile::ResizeImageGet (сильно упрощено)
        $file = self::checkFileArray($file);
        if (!$file) {
            return false;
        }
        $arSize = self::checkSizeArray($arSize);
        $resizeType = self::checkResizeType($resizeType);
        $quality = self::checkQuality($quality);

        $uploadDirName = COption::GetOptionString("main", "upload_dir", "upload");
        $cacheImageFile = "/".$uploadDirName."/resize_cache/".$file["SUBDIR"]."/".$arSize["width"]."_".$arSize["height"]."_".$resizeType."_".$quality."/".$file["FILE_NAME"];
        $cacheImageFile = self::replaceExtension($cacheImageFile);
        $io = CBXVirtualIo::GetInstance();
        static $cache = [];
        if (isset($cache[$cacheImageFile])) {
            return $cache[$cacheImageFile];
        } elseif (!file_exists($io->GetPhysicalName($_SERVER["DOCUMENT_ROOT"].$cacheImageFile))) {
            $obImage = new self();
            $success = $obImage
                ->setSource($file["SRC"])
                ->setDestination($cacheImageFile)
                ->setSize($arSize)
                ->setResizeType($resizeType)
                ->setQuality($quality)
                ->convert()
            ;
        }

        $cache[$cacheImageFile] = [
            "src" => $cacheImageFile,
        ];
        return $cache[$cacheImageFile];
    }

    private static function checkFileArray($file)
    {
        if (!is_array($file) && intval($file) > 0) {
            $file = CFile::GetFileArray($file);
        }
        if (!is_array($file) || !array_key_exists("FILE_NAME", $file) || strlen($file["FILE_NAME"]) <= 0) {
            return false;
        }
        return $file;
    }

    private static function checkSizeArray($arSize)
    {
        if (!is_array($arSize)) {
            $arSize = array();
        }
        if (!array_key_exists("width", $arSize) || intval($arSize["width"]) <= 0) {
            $arSize["width"] = 0;
        }
        if (!array_key_exists("height", $arSize) || intval($arSize["height"]) <= 0) {
            $arSize["height"] = 0;
        }
        $arSize["width"] = intval($arSize["width"]);
        $arSize["height"] = intval($arSize["height"]);

        return $arSize;
    }

    private static function checkResizeType($resizeType)
    {
        if ($resizeType !== BX_RESIZE_IMAGE_EXACT && $resizeType !== BX_RESIZE_IMAGE_PROPORTIONAL_ALT) {
            $resizeType = BX_RESIZE_IMAGE_PROPORTIONAL;
        }
        return $resizeType;
    }

    private static function checkQuality($quality)
    {
        try {
            if (!is_numeric($quality)) {
                throw new Exception();
            }
            $quality = (int)$quality;
            if ($quality < 0 || $quality > 100) {
                throw new Exception();
            }
        } catch (Exception $e) {
            $quality = self::getQualityFromOption();
        }
        return $quality;
    }
}
