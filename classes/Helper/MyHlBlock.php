<?php

namespace Rapid\Dev\Helper;

use Bitrix\Highloadblock as HL;
use Exception;

class MyHlBlock
{
    /**
     * Массив со списком всех HL-блоков
     * @var null|array
     */
    private static $arHlBlocks = NULL;

    /**
     * Формирование списка всех HL-блоков [код => ид] с кешированием
     * @throws \Bitrix\Main\ArgumentException
     */
    private static function getAll()
    {
        self::$arHlBlocks = [];
        $dbHlBlocks = HL\HighloadBlockTable::getList();
        while ($arHlBlock = $dbHlBlocks->fetch()) {
            self::$arHlBlocks['CODE'][ToUpper($arHlBlock['NAME'])] = $arHlBlock['ID'];
            self::$arHlBlocks['TABLE_NAME'][ToUpper($arHlBlock['TABLE_NAME'])] = $arHlBlock['ID'];
        }
    }

    /**
     * Получение ИД HL-блока по коду
     * @param string $code Символьный код HL-блока
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function get($code)
    {
        $code = ToUpper($code);
        if (self::$arHlBlocks === NULL) {
            self::getAll();
        }
        if (array_key_exists($code, self::$arHlBlocks['CODE'])) {
            return intval(self::$arHlBlocks['CODE'][$code]);
        }
        return 0;
    }

    /**
     * Получение ИД HL-блока по коду
     * @param string $tableName название таблицы инфоблока
     * @return int
     */
    public static function getByTableName($tableName)
    {
        try {
            if (!is_string($tableName) || empty($tableName)) {
                throw new Exception();
            }
            if (self::$arHlBlocks === null) {
                self::getAll();
            }
            $tableName = ToUpper($tableName);
            if (!array_key_exists($tableName, self::$arHlBlocks['TABLE_NAME'])) {
                throw new Exception();
            }
            return (int)self::$arHlBlocks['TABLE_NAME'][$tableName];
        } catch (Exception $e) {
            return 0;
        }
    }

    /**
     * Возвращает класс сущности HL-блока
     * @param $intId
     * @return \Bitrix\Main\Entity\DataManager|bool
     * @throws \Bitrix\Main\SystemException
     */
    public static function getEntityClass($intId)
    {
        if (!$hlblock = HL\HighloadBlockTable::getById($intId)->fetch()) {
            return false;
        }
        $entity = HL\HighloadBlockTable::compileEntity($hlblock); //генерация класса
        $entityClass = $entity->getDataClass();
        return $entityClass;
    }
}
