<?php

namespace Rapid\Dev\Helper;

use CPHPCache;
use Exception;

class Cache
{
    const CACHETIME_SECONDS_HOUR = 3600;
    const CACHETIME_SECONDS_DAY = 86400;
    const CACHETIME_SECONDS_MONTH = 2592000;
    const CACHETIME_SECONDS_YEAR = 31536000;
    const CACHETIME_MINUTES_HOUR = 60;
    const CACHETIME_MINUTES_DAY = 1440;
    const CACHETIME_MINUTES_MONTH = 43200;
    const CACHETIME_MINUTES_YEAR = 525600;

    private $obCache = null;
    private $cacheTime = self::CACHETIME_SECONDS_YEAR;
    private $cacheId;
    private $cachePath;
    const PARAM_SEPARATOR = '_';

    /**
     * @return int
     */
    public function getCacheTime(): int
    {
        return $this->cacheTime;
    }

    /**
     * @param int $cacheTime
     * @return Cache
     */
    public function setCacheTime(int $cacheTime): Cache
    {
        $this->cacheTime = $cacheTime;
        return $this;
    }

    public static function cleanAll()
    {
        BXClearCache(true);
        $GLOBALS["CACHE_MANAGER"]->CleanAll();
        $GLOBALS["stackCacheManager"]->CleanAll();
        $staticHtmlCache = \Bitrix\Main\Data\StaticHtmlCache::getInstance();
        $staticHtmlCache->deleteAll();
    }

    public static function cleanByTags($tags)
    {
        if (!is_array($tags)) {
            $tags = (array)$tags;
        }
        foreach ($tags as $tag) {
            $GLOBALS["CACHE_MANAGER"]->ClearByTag($tag);
        }
    }

    public function __construct($method)
    {
        $args = func_get_args();
        array_shift($args);
        $this->obCache = new CPHPCache();
        $this->setCachePath($method, $args);
        $this->setCacheId();
    }

    public function get($logic)
    {
        $result = null;
        $cacheTime = $this->getCacheTime();
        if ($this->obCache->InitCache($cacheTime, $this->cacheId, $this->cachePath)) {
            $result = $this->obCache->GetVars();
        } elseif (is_callable($logic) && $this->obCache->StartDataCache($cacheTime, $this->cacheId,
                $this->cachePath)
        ) {
            $GLOBALS["CACHE_MANAGER"]->StartTagCache($this->cachePath);
            try {
                $result = $logic();
                $GLOBALS["CACHE_MANAGER"]->EndTagCache();
                $this->obCache->EndDataCache($result);
            } catch (Exception $e) {
                $this->abortDataCache();
            }
        }
        return $result;
    }

    /**
     * @deprecated использовать Rapid\Dev\Helper\Cache::cleanByTags
     */
    public function clean()
    {
        $this->obCache->Clean($this->cacheId, $this->cachePath);
    }

    /**
     * @param $method
     * @param array $args
     */
    public function setCachePath($method, $args = [])
    {
        $method = str_replace(['\\', '::'], DIRECTORY_SEPARATOR, $method);
        $strPath = implode(DIRECTORY_SEPARATOR, [
            Site::getSiteIdOrDefaultSiteId(),
            $method,
            md5(serialize($args))
        ]);
        $strPath = preg_replace('/\\' . DIRECTORY_SEPARATOR . '{2,}/', DIRECTORY_SEPARATOR, $strPath);
        $this->cachePath = $strPath;
    }

    /**
     * @return mixed
     */
    public function getCachePath()
    {
        return $this->cachePath;
    }

    public function setCacheId()
    {
        $this->cacheId = str_replace(DIRECTORY_SEPARATOR, self::PARAM_SEPARATOR,
            trim($this->cachePath, DIRECTORY_SEPARATOR));
    }

    /**
     * @return mixed
     */
    public function getCacheId()
    {
        return $this->cacheId;
    }

    public function abortDataCache()
    {
        $this->obCache->AbortDataCache();
    }
}
