<?php

namespace Rapid\Dev\Helper;

use CIBlockFormatProperties;
use CSite;
use DateTime;
use function MakeTimeStamp;

class Date
{

    /**
     * Генерация "умной" даты, например если даты начала и конца интервала имеют одинаковый месяц и год,
     * то выводить "22 -- 24 мая 2011", а если только год, то "22 мая -- 01 июня 2011".
     * @param string $date_from Даты от (d.m.Y)
     * @param string $date_to Даты до (d.m.Y)
     * @return string
     */
    static public function getIntervalDate($date_from = '', $date_to = '')
    {
        // UNIX - timestamp
        $tms_from = MakeTimeStamp($date_from, 'DD.MM.YYYY');
        $tms_to = MakeTimeStamp($date_to, 'DD.MM.YYYY');

//        $fromMonth = date('M', $tms_from);
//        $toMonth = date('M', $tms_to);
        $fromMonth = ToLower(CIBlockFormatProperties::DateFormat('M', $tms_from)); // F
        $toMonth = ToLower(CIBlockFormatProperties::DateFormat('M', $tms_to)); // F


        // Разбиваем дату ДО
        $date_to = ParseDateTime($date_to, FORMAT_DATETIME);

        $gen_date = '';
        // Если даты не совпадают
        if ($tms_from && $tms_from != $tms_to) {
            // Разбиваем дату ОТ
            $date_from = ParseDateTime($date_from, FORMAT_DATETIME);

            $end_from = '';
            // Если года равны
            if (($date_from['YYYY'] == $date_to['YYYY'])) {
                // А месяцы нет
                if (intval($date_from['MM']) != intval($date_to['MM'])) {
//                    $end_from = '.'.$date_from['MM'];
                    $end_from = ' ' . $fromMonth;
                    //$end_from = ' ' . ToLower(GetMessage('MONTH_'.intval($date_from['MM']).'_S'));
                }
            } else {
                $end_from = ' ' . $fromMonth . ' ' . $date_from['YYYY'];
                //$end_from = ' ' . ToLower(GetMessage('MONTH_'.intval($date_from['MM']).'_S')) . ' ' . $date_from['YYYY'];
            }

            $gen_date .= $date_from['DD'] . $end_from;
            if (is_array($date_to)) $gen_date .= ' &mdash; ';
        }

        if (is_array($date_to)) {
            $gen_date .= $date_to['DD'] . ' ' . $toMonth . ' ' . $date_to['YYYY'];
            //$gen_date .= $date_to['DD'].' '.ToLower(GetMessage('MONTH_'.intval($date_to['MM']).'_S')).' '.$date_to['YYYY'];
        }

        return $gen_date;
    }

    /**
     * Форматирование даты для вывода
     * @param string $date Дата (d.m.Y)
     * @return string
     */
    static public function formatDate($date = '')
    {
        $format_date = '';
        if (!empty($date)) {
            $date = ParseDateTime($date, FORMAT_DATETIME);
            //$year = ($date['YYYY'] != date('Y')) ? ' '.$date['YYYY'] : '';
            $format_date = $date['DD'] . ' ' . ToLower(GetMessage('MONTH_' . intval($date['MM']) . '_S')) . ' ' . $date['YYYY'];
        }
        return $format_date;
    }

    /**
     * Формирование массива для фильтрации по дате
     * @param string $date_from Даты от (d.m.Y)
     * @param string $date_to Даты до (d.m.Y)
     * @param string $c_field_name_from Название поля с датой от
     * @param string $c_field_name_to Название поля с датой до
     * @return array
     */
    static public function getFilterDate($date_from = '', $date_to = '', $c_field_name_from = 'DATE_ACTIVE_FROM', $c_field_name_to = 'DATE_ACTIVE_TO')
    {
        $field_name_from = $field_name_to = $c_field_name_from;

        if (empty($date_from) && empty($date_to)) return array();

        $c = false;

        if (!empty($date_from)) {
            $date_from = ParseDateTime($date_from, FORMAT_DATETIME);
            if (empty($date_to)) {
                $date_to = $date_from;
                $c = true;
            }
        }
        if (!$c && !empty($date_to)) {
            $date_to = ParseDateTime($date_to, FORMAT_DATETIME);
            $field_name_to = $c_field_name_to;
            if (empty($date_from)) {
                $date_from = $date_to;
                $field_name_from = $c_field_name_to;
            }
        }

        if ($field_name_from != 'DATE_ACTIVE_FROM' && $field_name_from != 'DATE_ACTIVE_TO') {
            $date_from = $date_from['YYYY'] . '-' . $date_from['MM'] . '-' . $date_from['DD'];
        } else {
            $date_from = $date_from['DD'] . '.' . $date_from['MM'] . '.' . $date_from['YYYY'];
        }

        if ($field_name_to != 'DATE_ACTIVE_TO' && $field_name_to != 'DATE_ACTIVE_FROM') {
            $date_to = $date_to['YYYY'] . '-' . $date_to['MM'] . '-' . $date_to['DD'];
        } else {
            $date_to = $date_to['DD'] . '.' . $date_to['MM'] . '.' . $date_to['YYYY'];
        }

        if ($field_name_from != $field_name_to) {
            return array(
                ">=" . $field_name_from => array(false, $date_from . ' 00:00:00'),
                "<=" . $field_name_to => array(false, $date_to . ' 23:59:59'),
            );
        }

        return array(
            ">=" . $field_name_from => $date_from . ' 00:00:00',
            "<=" . $field_name_to => $date_to . ' 23:59:59',
        );
    }

    /**
     * Поверка даты на актуальность, чтобы нельзя было поставить дату из будущего
     * @param string $date Дата (d.m.Y)
     * @return bool
     */
    static public function checkActualDate($date)
    {
        $tms = MakeTimeStamp($date, 'DD.MM.YYYY');
        $tms_today = time();
        return (bool)($tms <= $tms_today);
    }

    /**
     * Возвращает текущую дату в формате сайта
     * @param string $strFormat
     * @return false|string
     */
    public static function getCurrentDate($strFormat = "SHORT")
    {
        static $date;
        if ($date) {
            return $date;
        }
        global $DB;
        $date = date($DB->DateFormatToPHP(CSite::GetDateFormat($strFormat)), time());
        return $date;
    }

    /**
     * Сравнивает две даты, которые могут быть как в формате фильтра, так и в формате сайта
     * @param string $strDate1 Дата 1
     * @param string $strDate2 Дата 2
     * @return mixed 0, если даты равны; 1, если $strDate2 > $strDate1; иначе - -1
     */
    public static function compareDates($strDate1, $strDate2)
    {
        if (($r = static::getDiffDays($strDate1, $strDate2)) != 0) {
            $r = $r > 0 ? 1 : -1;
        }
        return $r;
    }

    /**
     * Сравнивает две даты, которые могут быть как в формате фильтра, так и в формате сайта.
     * @param string $strDate1 Дата 1
     * @param string $strComparison Логический оператор сравнения
     * @param string $strDate2 Дата 2
     * @return bool
     */
    public static function compareDatesBool($strDate1, $strComparison, $strDate2)
    {
        $r = static::getDiffDays($strDate1, $strDate2);
        switch ($strComparison) {
            default:
            case '=':
                return $r == 0;
            case '<':
                return $r > 0;
            case '<=':
                return $r >= 0;
            case '>':
                return $r < 0;
            case '>=':
                return $r <= 0;
            case '!=':
                return $r != 0;
        }
    }

    /**
     * Возвращает разницу в днях между двумя датами
     * @param $strFrom
     * @param $strTo
     * @return mixed
     */
    public static function getDiffDays($strFrom, $strTo)
    {
        $strDateFrom = date_create($strFrom);
        $strDateTo = date_create($strTo);
        $obDateInterval = date_diff($strDateFrom, $strDateTo);
        return $obDateInterval->invert ? -$obDateInterval->days : $obDateInterval->days;
    }

    /**
     * Вывод времени оставления коммента
     * @param int $date
     * @param string $format
     * @return string
     */
    static function ago($date, $format = FORMAT_DATETIME)
    {
        $timestamp = $date;
        $now = time();
        if (is_string($date)) {
            $timestamp = MakeTimeStamp($date, $format);
        }
        $diff = $now - $timestamp;
        $diff = abs($diff);
        if ($diff < 60) {
            return "Только что";
        } else {
            if ($diff < 3600) {
                $relative_number = floor($diff / 60);
                return MyString::declineWord($relative_number, array('минуту', 'минуты', 'минут')).' назад';
            } else {
                if ($diff < 86400) {
                    $relative_number = floor($diff / 60 / 60);
                    return MyString::declineWord($relative_number, array('час', 'часа', 'часов')).' назад';
                } else {
                    if ($diff < 2592e3) {
                        $relative_number = floor($diff / 60 / 60 / 24);
                        if ($relative_number == 1) {
                            return "Вчера";
                        }
                    }
                }
            }
        }
        $dateStr = ToLower(CIBlockFormatProperties::DateFormat('j F', $timestamp));
        if (date('Y', $timestamp) != date('Y', $now)) {
            $dateStr .= ' '.date('Y', $timestamp);
        }
        $dateStr .= ' в '.date('H:i', $timestamp);
        return $dateStr;
    }


    /**
     * Конвертирует дату из строки в формате, который использует фильтр (DD-MM-YYYY), в формат сайта
     * @param $strDateFromSession
     * @return string
     */
    public static function convertDateFromFilterToSite($strDateFromSession)
    {
        global $DB;
        return $DB->FormatDate($strDateFromSession, "DD-MM-YYYY", CSite::GetDateFormat("SHORT"));
    }

    /**
     * Переводит дату из формата сайта в произвольный формат
     * @param $strDate
     * @param string $strFormat
     * @return string
     */
    public static function convertDateFromSiteToCustom($strDate, $strFormat = "d.m.Y")
    {
        $intTimestamp = MakeTimeStamp($strDate);
        return CIBlockFormatProperties::DateFormat($strFormat, $intTimestamp);
    }

    /**
     * Переводит дату из формата сайта в формат сайта. Суть - разница форматов SHORT и FULL
     * @param $strDate
     * @param string $strFormat
     * @return string
     */
    public static function convertDateFromSiteToSite($strDate, $strFormat = 'SHORT')
    {
        global $DB;
        if (!$intTimestamp = MakeTimeStamp($strDate)) {
            return '';
        }
        return date($DB->DateFormatToPHP(CSite::GetDateFormat($strFormat)), $intTimestamp);
    }

    public static function parseTime($strTime)
    {
//        date_parse
        preg_match_all('/\d+/', $strTime, $arMatches);
        return array(
            'h' => $arMatches[0][0] ?: '00',
            'm' => $arMatches[0][1] ?: '00',
            's' => $arMatches[0][2] ?: '00',
        );
    }

    public static function convertToSql($date)
    {
        global $DB;
        return trim($DB->CharToDateFunction($date, 'SHORT'), '\'');
    }

    public static function getDateDiff($from, $to = false)
    {
        $obFrom = (new DateTime())->setTimestamp(MakeTimeStamp($from));
        if ($to) {
            $obTo = (new DateTime())->setTimestamp(MakeTimeStamp($to));
        } else {
            $obTo = (new DateTime())->setTime(0, 0);
        }
        return $obTo->diff($obFrom);
    }

    /**
     * @param \DateInterval $obDiff
     * @return string
     */
    public static function formatDateDiff($obDiff)
    {
        $arRes = array();
        if ($obDiff->y) {
            $arRes[] = MyString::declineWord($obDiff->y, array('год', 'года', 'лет'));
        }
        if ($obDiff->m) {
            $arRes[] = MyString::declineWord($obDiff->m, array('месяц', 'месяца', 'месяцев'));
        }
        if ($obDiff->d) {
            $arRes[] = MyString::declineWord($obDiff->d, array('день', 'дня', 'дней'));
        }
        if (empty($arRes)) {
            $strRes = 'сегодня';
        } else {
            $strRes = implode(' ', $arRes);
            if ($obDiff->invert) {
                $strRes .= ' назад';
            } else {
                $strRes = 'через ' . $strRes;
            }
        }
        return $strRes;
    }

    public static function getStartOfWeek($date)
    {
        if (!is_numeric($date)) {
            $date = MakeTimeStamp($date);
        }
        $obDate = (new DateTime())->setTimestamp($date)->setTime(0, 0);
        $dow = $obDate->format('N');
        $intStartOfWeek = $obDate->getTimestamp();
        if ($dow != 1) {
            $intStartOfWeek = $obDate->modify('last monday')->getTimestamp();
        }
        return $intStartOfWeek;
    }

    public static function getEndOfWeek($date)
    {
        if (!is_numeric($date)) {
            $date = MakeTimeStamp($date);
        }
        $intStartOfWeek = static::getStartOfWeek($date);
        $intEndOfWeek = (new DateTime())->setTimestamp($intStartOfWeek)->modify('+6 days')->getTimestamp();
        return $intEndOfWeek;
    }
}
