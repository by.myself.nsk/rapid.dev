<?php

namespace Rapid\Dev\Helper;

use Exception;

/**
 * @method login($username, $password)
 * @method pwd()
 * @method chdir($directory)
 * @method pasv($bPassiveMode)
 * @method mdtm($file)
 * @method close()
 * */
class MyFtp
{
    protected $conn;

    protected static function parseUrl($url)
    {
        preg_match("/^(ftp:\/\/)?([^\/]+)(.*)$/i", $url, $match);
        $arRes = array(
            'protocol' => $match[1],
            'domain' => $match[2],
            'path' => $match[3],
        );
        return $arRes;
    }

    public function __construct($url, $bSSL = false)
    {
        $arParts = static::parseUrl($url);
        $url = $arParts['domain'];
        if ($bSSL) {
            $this->conn = ftp_ssl_connect($url);
        } else {
            $this->conn = ftp_connect($url);
        }
    }

    /**
     * @param $func
     * @param $args
     * @return mixed
     * @throws Exception
     */
    public function __call($func, $args)
    {
        if (strstr($func, 'ftp_') == false) {
            $func = 'ftp_' . $func;
        }
        if (!function_exists($func)) {
            throw new Exception('FTP function does not exist');
        }
        array_unshift($args, $this->conn);
        return call_user_func_array($func, $args);
    }

    public function isDir($file)
    {
        return ftp_size($this->conn, $file) === -1;
    }

    public function nlist($directory = '')
    {
        return ftp_nlist($this->conn, $directory);
    }

    public function get($localFile, $remoteFile, $mode = FTP_ASCII)
    {
        return ftp_get($this->conn, $localFile, $remoteFile, $mode);
    }
}
