<?php

namespace Rapid\Dev\Helper;

use Bitrix\Main\Localization\Loc;

class MyLangFiles
{
    public static function loadFiles($filepaths, $filename = 'lang.php')
    {
        if (is_string($filepaths) && !empty($filepaths)) {
            $filepaths = array($filepaths);
        }
        if (!is_array($filepaths)) {
            return false;
        }
        if (!is_string($filename) || empty($filename)) {
            $filename = 'lang.php';
        }
        foreach ($filepaths as $filepath) {
            if (strpos($filepath, $_SERVER['DOCUMENT_ROOT']) !== 0) {
                $filepath = $_SERVER['DOCUMENT_ROOT'].$filepath;
            }
            $filepath = rtrim($filepath, '/').'/';
            $strFilename = $filepath.$filename;
            Loc::loadLanguageFile($strFilename);
        }
        return true;
    }

    public static function getLangFiles()
    {
        return Loc::getIncludedFiles();
    }
}
