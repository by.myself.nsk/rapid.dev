<?php

namespace Rapid\Dev\Helper;

use CAllLanguage;
use CSite;

class Site extends CSite
{
    public static function getSiteIdOrDefaultSiteId()
    {
        if (SitePage::isAdmin()) {
            return CSite::GetDefSite();
        }
        return SITE_ID;
    }

    public static function getCurrentSiteName()
    {
        $strSiteId = self::getSiteIdOrDefaultSiteId();
        $arSite = CSite::GetByID($strSiteId)->Fetch();
        return $arSite['SITE_NAME'];
    }

    public static function getLangList($strValue = false, $strKey = false)
    {
        $dbLangs = CAllLanguage::GetList($by = "lid", $order = "desc");
        $arLangs = array();
        while ($arLang = $dbLangs->Fetch()) {
            if ($strKey) {
                $arLangs[$arLang[$strKey]] = $strValue ? $arLang[$strValue] : $arLang;
            } else {
                $arLangs[] = $strValue ? $arLang[$strValue] : $arLang;
            }
        }
        return $arLangs;
    }

    public static function getSiteByLangId($strLangId, $strValueCode = 'ID')
    {
        $arSite = CSite::GetList(
            $by,
            $order,
            array(
                'LANGUAGE_ID' => $strLangId
            )
        )->Fetch();
        return $arSite[$strValueCode] ?: $arSite;
    }

    public static function getDefaultSiteLanguage()
    {
        static $lang = '';
        if ($lang) {
            return $lang;
        }
        if ($arSite = CSite::GetList(
            $by,
            $order,
            array(
                'DEFAULT' => 'Y'
            )
        )->Fetch()
        ) {
            $lang = $arSite['LANGUAGE_ID'];
        }
        return $lang;
    }

    /**
     * Возвращает папку сайта. Если находимся в публичке, то все однозначно. Если в админке, то определяет сайт по языку, и возвращает его папку.
     * @param bool|string $strSiteId ID сайта. Если не указан, возьмет текущий
     * @return mixed
     */
    public static function getSiteDir($strSiteId = false)
    {
        if ($strSiteId == false) {
            if (!SitePage::isAdmin()) {
                return SITE_DIR;
            }
            return static::getSiteByLangId(LANGUAGE_ID, 'DIR');
        } else {
            $dbSites = CSite::GetList($by, $order);
            $arSitesDirs = array();
            while ($arSite = $dbSites->Fetch()) {
                $arSitesDirs[$arSite['LID']] = $arSite['DIR'];
            }
            return $arSitesDirs[$strSiteId] ?: static::getSiteDir(false);
        }
    }

    /**
     * Возвращает папку сайта. Если находимся в публичке, то все однозначно. Если в админке, то определяет сайт по языку, и возвращает его папку.
     * @param bool|string $strSiteId ID сайта. Если не указан, возьмет текущий
     * @return mixed
     */
    public static function getSiteDocumentRoot($strSiteId = false)
    {
        if (!$strSiteId) {
            $strSiteId = SitePage::isAdmin() ? static::getSiteByLangId(LANGUAGE_ID, 'DIR') : SITE_DIR;
        }
        $dbSites = CSite::GetList($by, $order);
        $arSitesDirs = array();
        while ($arSite = $dbSites->Fetch()) {
            $arSitesDirs[$arSite['LID']] = $arSite['DOC_ROOT'];
        }
        return $arSitesDirs[$strSiteId];
    }
}
