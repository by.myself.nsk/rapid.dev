<?php

namespace Rapid\Dev\Helper;

use Bitrix\Main\Context;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Web\Json;
use CMain;
use CSite;
use Exception;

class SitePage
{
    public static function setDefaultSeoData($arItem)
    {
        $name = $arItem['~NAME'] ?: $arItem['NAME'];

        self::setTitles($name, $name);
    }

    /**
     * @param $arSeoData - SEO-данные. Стандартные компоненты кладут их в ключ массива ["IPROPERTY_VALUES"]
     * @param string $strSeoType Тип SEO-данных. Используется для построения ключа массива. ELEMENT или SECTION. Регистронезависимый.
     * @param string $strComponentName Имя компонента, в котором устанавливается заголовок. Необязательный.
     * @return bool
     */
    public static function setSeoData($arSeoData, $strSeoType = 'ELEMENT', $strComponentName = '')
    {
        if (!is_array($arSeoData) || empty($arSeoData)) {
            return false;
        }
        $strSeoType = ToUpper($strSeoType);
        if (!in_array($strSeoType, array('ELEMENT', 'SECTION'))) {
            $strSeoType = 'ELEMENT';
        }
        $arComponentName = null;
        if (!empty($strComponentName)) {
            $arComponentName = array('COMPONENT_NAME' => $strComponentName);
        }
        global $APPLICATION;
        if (!empty($arSeoData[$strSeoType . '_PAGE_TITLE'])) {
            $APPLICATION->SetTitle(htmlspecialchars_decode($arSeoData[$strSeoType . '_PAGE_TITLE']), $arComponentName);
        }
        foreach (array('TITLE', 'KEYWORDS', 'DESCRIPTION') as $strProp) {
            $data = $arSeoData[$strSeoType . '_META_' . $strProp];
            $data = htmlspecialchars_decode($data);
            $data = strip_tags($data);
            if (!empty($data)) {
                $APPLICATION->SetPageProperty(ToLower($strProp), $data, $arComponentName);
            }
        }
        return true;
    }

    public static function setTitles($strPageTitle, $strBrowserTitle = '', $strComponentName = '')
    {
        if (!is_string($strPageTitle)) {
            return false;
        }
        global $APPLICATION;
        $arComponentName = null;
        if (!empty($strComponentName)) {
            $arComponentName = array('COMPONENT_NAME' => $strComponentName);
        }
        $APPLICATION->SetTitle($strPageTitle, $arComponentName);
        if (is_string($strBrowserTitle)) {
            $APPLICATION->SetPageProperty('title', $strBrowserTitle, $arComponentName);
        }
        return true;
    }

    /**
     * @param string|array $actionValue
     * @param string $strActionName
     * @param bool $bPostOnly
     * @return bool
     */
    static public function isAjax($actionValue = null, $strActionName = 'action', $bPostOnly = false)
    {
        if (ToLower($_SERVER['HTTP_X_REQUESTED_WITH']) != '' || !isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
            return false;
        }
        if ($bPostOnly && $_SERVER['REQUEST_METHOD'] != 'POST') {
            return false;
        }
        if (is_string($actionValue)) {
            $actionValue = array($actionValue);
        }

        $requestActionValue = $bPostOnly ? $_POST[$strActionName] : $_REQUEST[$strActionName];

        if ($strActionName && !isset($requestActionValue)) {
            return false;
        }
        if (!empty($actionValue)) {
            if (!isset($requestActionValue) || !in_array($requestActionValue, $actionValue)) {
                return false;
            }
        }
        return $_REQUEST[$strActionName] ?: true;
    }

    /**
     * Отправка данных в json
     * @param mixed $response
     */
    static public function sendAjaxJson($response) {
        global $APPLICATION;
        while (ob_get_level() > 0) {
            ob_end_clean();
        }
        $APPLICATION->RestartBuffer();
        header('Content-Type: application/json');
        echo Json::encode(
            $response,
            JSON_HEX_TAG
            | JSON_HEX_AMP
            | JSON_HEX_APOS
            | JSON_HEX_QUOT
            | JSON_UNESCAPED_UNICODE
            | JSON_PRETTY_PRINT
            | JSON_INVALID_UTF8_SUBSTITUTE
        );
        CMain::FinalActions();
        die();
    }

    /**
     * Отправка данных в text/html
     * @param mixed $response
     */
    static public function sendAjaxText($response) {
        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        header('Content-Type: text/html');
        echo $response;
        CMain::FinalActions();
        die();
    }

    public static function getCurParamArray()
    {
        return $_GET;
    }

    public static function getCurParam($arAdd = array(), $arKillBefore = array(), $arKillAfter = array())
    {
        if (!is_array($arAdd)) {
            $arAdd = array();
        }
        if (!is_array($arKillBefore)) {
            $arKillBefore = array();
        }
        if (!is_array($arKillAfter)) {
            $arKillAfter = array();
        }
        $arCurParam = self::getCurParamArray();
        $arCurParam = array_diff_key($arCurParam, array_flip($arKillBefore));
        $arCurParam = array_merge($arCurParam, $arAdd);
        $arCurParam = array_diff_key($arCurParam, array_flip($arKillAfter));
        return http_build_query($arCurParam);
    }

    public static function GetCurPageParam($arAdd = array(), $arKillBefore = array(), $arKillAfter = array())
    {
        $strQuery = self::GetCurParam($arAdd, $arKillBefore, $arKillAfter);
        global $APPLICATION;
        $strCurPage = $APPLICATION->GetCurPage();
        $strResult = $strCurPage . ($strQuery ? '?' . $strQuery : '');
        return $strResult;
    }

    public static function redirectPagination()
    {
        $arParams = self::getCurParamArray();
        if (!is_array($arParams) || empty($arParams)) {
            return;
        }
        $arKill = array();
        foreach ($arParams as $strKey => $strValue) {
            if (preg_match('/PAGEN_\d/i', $strKey) && $strValue == 1) {
                $arKill[] = $strKey;
            }
        }
        if (!is_array($arKill) || empty($arKill)) {
            return;
        }
        $strRedirect = self::GetCurPageParam(array(), $arKill, true);
        LocalRedirect($strRedirect, '301 Moved permanently');
    }

    public static function isAuth($bCheckAuthorize = true)
    {
        global $USER;
        global $APPLICATION;
        $bRes = (
            (defined('NEED_AUTH') && NEED_AUTH == 'Y' && !$USER->IsAuthorized())
            || ($_REQUEST['forgot_password'] == 'yes')
            || ($_REQUEST['change_password'] == 'yes')
            || ($_REQUEST['register'] == 'yes')
            || ($_REQUEST['confirm_registration'] == 'yes')
            || ($bCheckAuthorize && !$USER->CanDoFileOperation('fm_view_file', array(SITE_ID, $APPLICATION->GetCurPage(true))))
        );
        return $bRes;
    }

    /**
     * Метод устанавливает OG-параметры с помощью метода AddHeadString. Важно помнить о кешировании страниц!
     * @param array $arData Массив с og-параметрами. Ключи: TITLE, SITE_NAME, URL, DESCRIPTION, IMAGE
     * <br>SITE_NAME и URL подставляются по умолчанию
     */
    public static function addHeadOg($arData = array())
    {
        global $APPLICATION;
        $arDefaultData = array(
            'TITLE' => '',
            'SITE_NAME' => Site::getCurrentSiteName(),
            'URL' => $APPLICATION->GetCurPage(),
            'DESCRIPTION' => '',
            'IMAGE' => '',
        );
        $arData = array_merge($arDefaultData, $arData);
        foreach ($arData as $key => $val) {
            if ($key == 'IMAGE') {
                if ($val && strpos($val, $_SERVER['SERVER_NAME']) === false) {
                    $val = 'http://' . $_SERVER['SERVER_NAME'] . $val;
                }
                $APPLICATION->AddHeadString('<link rel="image_src" href="' . $arData['IMAGE'] . '" />');
            }
            if (in_array($key, ['NAME', 'SITE_NAME', 'DESCRIPTION'])) {
                $val = htmlspecialchars($val);
            }
            $APPLICATION->AddHeadString('<meta property="og:' . ToLower($key) . '" content="' . $val . '" />');
        }
        $APPLICATION->AddHeadString('<meta property="og:type" content="website" />');
    }

    public static function isAdmin()
    {
        // константа определяется не всегда так, как ожидается.
        // например, в новой админке аякс-запрос отрабатывает без константы
        $bAdmin = CSite::InDir('/bitrix/') && !CSite::InDir('/bitrix/templates/');

        return ((defined('ADMIN_SECTION') && ADMIN_SECTION === true) || $bAdmin);
    }

    /**
     * Определение протокола
     * @return string
     */
    static public function getProtocol()
    {
        $isSecure = false;
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $isSecure = true;
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            $isSecure = true;
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
            $isSecure = true;
        } elseif (isset($_SERVER['SERVER_PORT']) && ($_SERVER['SERVER_PORT'] == '443')) {
            $isSecure = true;
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_PORT']) && ($_SERVER['HTTP_X_FORWARDED_PORT'] == '443')) {
            $isSecure = true;
        }

        return ($isSecure ? 'https' : 'http') . '://';
    }

    public static function getServerName($bWithProtocol = false)
    {
        return ($bWithProtocol ? static::getProtocol() : '').$_SERVER['SERVER_NAME'];
    }

    public static function setLastModifiedFromBxDate($timestampX)
    {
        $time = DateTime::createFromUserTime($timestampX);
        $ts = $time->getTimestamp();
        if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= $ts) {
            header('HTTP/1.1 304 Not Modified');
            die;
        }
        Context::getCurrent()->getResponse()->setLastModified($time);
    }

    public static function checkUrlStatus($url, $followLocation = true)
    {
        $result = false;
        try {
            $curlInit = curl_init($url);
            if (!$curlInit) {
                throw new Exception();
            }
            curl_setopt($curlInit,CURLOPT_CONNECTTIMEOUT,10);
            curl_setopt($curlInit,CURLOPT_HEADER,true);
            curl_setopt($curlInit,CURLOPT_NOBODY,true);
            curl_setopt($curlInit,CURLOPT_RETURNTRANSFER,true);
            if ($followLocation) {
                curl_setopt($curlInit,CURLOPT_FOLLOWLOCATION,true);
            }

            curl_exec($curlInit);
            $result = curl_getinfo($curlInit, CURLINFO_HTTP_CODE);
            curl_close($curlInit);
        } catch (Exception $e) {
            // $e->getMessage();
        }
        return $result;
    }

    public static function findFirstNon404Page($domain, $url)
    {
        $maxIterations = substr_count($url, '/');

        $urlStatus = false;

        $resultUrl = $url;
        $stepCount = 0;
        do {
            $urlStatus = SitePage::checkUrlStatus($domain . $resultUrl);
            if ($urlStatus == 200) {
                break;
            }
            $resultUrl = rtrim(dirname($resultUrl), '/') . '/';
            $stepCount++;
        } while ($stepCount < $maxIterations);

        return $urlStatus == 200 ? $resultUrl : false;
    }

    public static function getLogoutLink()
    {
        global $APPLICATION;
        return $APPLICATION->GetCurPageParam("logout=yes&".bitrix_sessid_get(), [
                "login",
                "logout",
                "register",
                "forgot_password",
                "change_password"]
        );
    }
}
