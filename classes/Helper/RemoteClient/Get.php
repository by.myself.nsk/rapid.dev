<?php

namespace Rapid\Dev\Helper\RemoteClient;

class Get extends RemoteClient
{

    /**
     * @param string $additionalParts
     * @param array $params
     * @param bool|true $bMergeWithSettings
     * @return string
     */
    protected function makeRequestGet($additionalParts = '', $params = array(), $bMergeWithSettings = true)
    {
        if (is_array($additionalParts)) {
            $additionalParts = implode('/', $additionalParts);
        }
        $url = $this->makeUrl($additionalParts);
        if (is_array($params) && $bMergeWithSettings) {
            $params = array_merge($this->requestParams, $params);
        }
        if (!empty($params)) {
            $url .= '?' . http_build_query($params);
        }
        return @file_get_contents($url);
    }

}
