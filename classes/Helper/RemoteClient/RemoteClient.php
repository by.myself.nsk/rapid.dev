<?php

namespace Rapid\Dev\Helper\RemoteClient;

class RemoteClient
{
    protected $serviceUrl = '';
    protected $requestParams = array();

    /**
     * Метод проверяет доступность сайта
     * @param $url
     * @return bool
     */
    public static function isSiteAvailable($url)
    {
        // проверка на валидность представленного url
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            return false;
        }
        // создаём curl подключение
        $cl = curl_init($url);
        curl_setopt($cl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($cl, CURLOPT_HEADER, true);
        curl_setopt($cl, CURLOPT_NOBODY, true);
        curl_setopt($cl, CURLOPT_RETURNTRANSFER, true);
        // получаем ответ
        $response = curl_exec($cl);
        curl_close($cl);
        if ($response) {
            return true;
        }
        return false;
    }

    /**
     * @param string $serviceUrl протокол и доменное имя сервера, к которому необходимо подключиться
     * @param array $params передаваемые параметры
     */
    public function __construct($serviceUrl, $params = array())
    {
        $serviceUrl = rtrim($serviceUrl, '/');
        if (empty($serviceUrl)) {
            return false;
        }
        $this->serviceUrl = $serviceUrl;
        $this->requestParams = $params;
        return true;
    }

    /**
     * @param string|array $additionalParts строка или массив строк с дополнительными параметрами url
     * @return bool|string
     */
    protected function makeUrl($additionalParts = '')
    {
        $url = $this->serviceUrl;
        if (!empty($url)) {
            $url .= '/';
        }
        if (is_array($additionalParts)) {
            $additionalParts = implode('/', $additionalParts);
        }
        $url .= $additionalParts;
        return preg_replace('/([^:])\/{2,}/', '$1/', $url);
    }
}
