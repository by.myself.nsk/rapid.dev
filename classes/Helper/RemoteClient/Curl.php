<?php

namespace Rapid\Dev\Helper\RemoteClient;


class Curl extends RemoteClient
{
    /**
     * @param string $additionalParts
     * @param array $params
     * @param bool|true $bMergeWithSettings
     * @return mixed
     */
    protected function makeRequestGet($additionalParts = '', $params = array(), $bMergeWithSettings = true)
    {
        if (is_array($additionalParts)) {
            $additionalParts = implode('/', $additionalParts);
        }
        $url = $this->makeUrl($additionalParts);
        if (is_array($params) && $bMergeWithSettings) {
            $params = array_merge($this->requestParams, $params);
        }
        if (!empty($params)) {
            $url .= '?' . http_build_query($params);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /**
     * @param string $additionalParts
     * @param array $params
     * @param bool|true $bMergeWithSettings
     * @return mixed
     */
    protected function makeRequestPost($additionalParts = '', $params = array(), $bMergeWithSettings = true)
    {
        if (is_array($additionalParts)) {
            $additionalParts = implode('/', $additionalParts);
        }
        $url = $this->makeUrl($additionalParts);
        if (is_array($params) && $bMergeWithSettings) {
            $params = array_merge($this->requestParams, $params);
        }
        $params = http_build_query($params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CRLF, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
