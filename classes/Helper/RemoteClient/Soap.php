<?php

namespace Rapid\Dev\Helper\RemoteClient;

use Exception;
use SoapClient;
use SoapFault;

class Soap extends RemoteClient
{
    protected $obClient;

    /**
     * @param string $serviceUrl
     * @param array $params
     */
    public function __construct($serviceUrl, $params = array())
    {
        if (!parent::__construct($serviceUrl, $params)) {
            return false;
        }

        $arDefaultParams = array(
            'trace' => true,
            'exceptions' => true,
            'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
            'cache_wsdl' => 0,
        );
        $this->obClient = new SoapClient(
            $this->makeUrl(),
            array_merge($arDefaultParams, $this->requestParams)
        );
        return true;
    }

    /**
     * @return array|Exception|SoapFault
     */
    public function getFunctions()
    {
        try {
            return $this->obClient->__getFunctions();
        } catch (SoapFault $e) {
            return $e;
        }
    }

    /**
     * @param $requestName
     * @param array $methodParams
     * @return Exception|SoapFault
     */
    public function makeRequest($requestName, $methodParams = array())
    {
        try {
            $obResult = $this->obClient->$requestName($methodParams);
        } catch (SoapFault $e) {
            return $e;
        }
        return $obResult;
    }
}
