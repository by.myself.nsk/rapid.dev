<?php

namespace Rapid\Dev\Helper;

class MyArray
{
    public static function findArrayKeyPos($arData, $key)
    {
        if (!\array_key_exists($key, $arData)) {
            return false;
        }
        $keys = array_flip(array_keys($arData));
        return $keys[$key];
    }

    public static function insertAfter(&$arData, $key, $arInsert)
    {
        $pos = static::findArrayKeyPos($arData, $key);
        if ($pos === false) {
            return $arData;
        }
        $pos++;
        $arFirst = array_slice($arData,0, $pos, true);
        $arSecond = array_slice($arData, $pos, null, true);
        $arData = $arFirst + $arInsert + $arSecond;
        return $arFirst + $arInsert + $arSecond;
    }
}
