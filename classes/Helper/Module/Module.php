<?php

namespace Rapid\Dev\Helper\Module;

use CModule;
use Exception;

/**
 * Класс для создания класса модуля. Важно указать в константе MODULE_ID идентификатор модуля
 */
class Module
{
    /** @var Options $obOptions */
    // все поля нужно переопределить у наследников, иначе будет работать некорректно
    /**
     * @var string
     */
    protected static $MODULE_ID = '';
    protected static $filePath = __DIR__;
    /**
     * @var Options $obOptions
     */
    protected static $obOptions;
    public static $arOptions = array();

    public static function init($strModuleId = '')
    {
        if (!static::$MODULE_ID) {
            if (!is_string($strModuleId) || !$strModuleId) {
                $strModulePath = static::getModulePath();
                $strModuleId = substr($strModulePath, strrpos($strModulePath, '/') + 1);
            }
            static::$MODULE_ID = $strModuleId;
        }
        $strOptionFile = $_SERVER['DOCUMENT_ROOT'].getLocalPath('modules/'.static::$MODULE_ID.'/.options.php');
        if (file_exists($strOptionFile) && is_file($strOptionFile)) {
            static::$obOptions = new Options($strOptionFile, static::$MODULE_ID, true);
        }
        return true;
    }

    public static function getModulePath($absolutePath = true)
    {
        if ($absolutePath) {
            return preg_replace('/(.*)(modules\/[^\/]+)(.*)$/', '$1$2', static::$filePath);
        } else {
            return preg_replace('/^.*(\/(local|bitrix)\/modules\/[^\/]+)(.*)$/', '$1', static::$filePath);
        }
    }

    public static function getOptionData($name)
    {
        if (!static::$obOptions) {
            static::init();
        }
        $arKeys = static::$obOptions->explodeOptionId($name);
        return static::$obOptions->getOption($arKeys[0], $arKeys[1]);
    }

    public static function getOption($name)
    {
        if (!static::$obOptions) {
            static::init();
        }
        $arKeys = static::$obOptions->explodeOptionId($name);
        return static::$obOptions->getOptionValue($arKeys[0], $arKeys[1]);
    }

    public static function getOptionsList()
    {
        // вернем ассоциативный массив опция => значение
        if (!static::$obOptions) static::init();
        $arOptions = static::$obOptions->getOptionsTree();
        $arResult = array();
        if (!empty($arOptions)) {
            foreach ($arOptions as &$arTab) {
                if (!empty($arTab['FIELDS'])) {
                    foreach ($arTab['FIELDS'] as &$arOption) {
                        $arResult[$arOption['ID']] = $arOption['VALUE'];
                    }
                    unset($arOption);
                }
            }
            unset($arTab);
        }
        return $arResult;
    }

    public static function getModuleId()
    {
        return static::$MODULE_ID;
    }

    public static function getModuleEvents($strModuleName, $strEventName)
    {
        $dbEvents = GetModuleEvents($strModuleName, $strEventName);
        $arEvents = array();
        while ($arEvent = $dbEvents->Fetch()) {
            $arEvents[] = $arEvent;
        }
        return $arEvents;
    }

    /**
     * @param $mid
     * @param $page
     * @return bool
     * @throws Exception
     */
    public static function routeAdminPage($mid, $page)
    {
        $mid = trim($mid);
        $page = trim($page);
        if (!strlen($mid)) {
            throw new Exception('mid is empty');
        }
        if (!strlen($page)) {
            throw new Exception('page is empty');
        }
        $arRoots = array(
            'local',
            'bitrix',
        );
        $bIncluded = false;
        foreach ($arRoots as $strRoot) {
            $strFile = $_SERVER['DOCUMENT_ROOT'].'/'.$strRoot.'/modules/'.$mid.'/admin/'.$page.'.php';
            if (!file_exists($strFile)) {
                continue;
            }
            /** @noinspection PhpIncludeInspection */
            include $strFile;
            $bIncluded = true;
            break;
        }
        if (!$bIncluded) {
            throw new Exception('page was not included');
        }
        return true;
    }

    public static function isModuleInstalled($moduleId)
    {
        try {
            $info = CModule::CreateModuleObject($moduleId);
            if (!$info) {
                throw new Exception();
            }
            return $info->IsInstalled();
        } catch (Exception $e) {
            // $e->getMessage();
            return false;
        }
    }
}
