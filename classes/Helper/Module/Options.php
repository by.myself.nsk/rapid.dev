<?php

namespace Rapid\Dev\Helper\Module;

use COption;
use CAdminTabControl;
use Rapid\Dev\Helper\MyString;
use Rapid\Dev\Helper\SitePage;

/**
 * Class MyModuleOptions Класс для работы со страницей настроек модуля
 */
class Options
{
    // в файле настройки разложены по вкладкам
    // после чтения файла настройки нужно сложить в линейный массив
    // также нужно сформировать массив вкладок для вывода
    protected $bLightInit;
    protected $strModuleId;
    protected $arOptionsTree;
    protected $arTabs;
    /**
     * @var CAdminTabControl
     */
    protected $obAdminTabControl;

    /**
     * @param array|string $data Массив с параметрами вкладок и опций, или строка - абсолютный путь к файлу, который вернет такой массив
     * @param string $strModuleId id модуля, для которого создаются опции. Необязательный. По умолчанию будет прочитан из $_REQUEST['mid'].
     * @param bool $bLightInit Если установлен в true, будет произведена краткая инициализация опций - только опции с параметром SAVE = Y.
     * Для использования на странице админки нужна полная инициализация.
     * Верно в случае, если используется в стандартном файле модуля options.php
     */
    function __construct($data, $strModuleId = '', $bLightInit = false)
    {
        $this->bLightInit = $bLightInit;
        if (!$strModuleId) {
            $strModuleId = $_REQUEST['mid'];
        }
        $this->strModuleId = $strModuleId;
        $this->addOptionsData($data);
        if (!$bLightInit) {
            $this->getTabs();
            $this->getAdminTabControl();
        }
        return $this;
    }

    public function addOptionsData($data)
    {
        $arNewData = array();
        if (is_array($data)) {
            $arNewData = $data;
        } elseif (is_string($data) && file_exists($data) && is_file($data)) {
            /** @noinspection PhpIncludeInspection */
            $arNewData = include $data;
        }
        if (!is_array($this->arOptionsTree)) {
            $this->arOptionsTree = array();
        }
        foreach ($arNewData as $strTabCode => $arTab) {
            if (!array_key_exists($strTabCode, $this->arOptionsTree)) {
                $this->addTabData($strTabCode, $arTab);
            }
            foreach ($arTab["FIELDS"] as $intOptionKey => $arField) {
                $this->addOptionData($strTabCode, $arField, false);
            }
        }
        $this->setCurrentValues();

        return true;
    }

    public function addTabData($strTabCode, $arData)
    {
        $this->arOptionsTree[$strTabCode] = $arData;
        $this->arOptionsTree[$strTabCode]['FIELDS'] = array();
    }

    public function addOptionData($strTabCode, $arField, $bSetCurrentValues = true)
    {
        $arField = array_merge(
            array(
                'TAB_CODE' => $strTabCode,
                'ID' => $strTabCode.'_'.$arField['CODE'],
            ),
            $arField
        );
        switch ($arField['TYPE']) {
            case 'LIST':
            case 'CHECKBOX':
            case 'RADIO':
                if ($arField['MULTIPLE'] == 'Y' && empty($arField['MULTIPLE_CNT'])) {
                    $arField['MULTIPLE_CNT'] = 1;
                }
                if ($arField['TYPE'] == 'LIST' && (empty($arField['LIST_TYPE']) || !in_array($arField['LIST_TYPE'],
                            array('L', 'C')))) {
                    $arField['LIST_TYPE'] = 'L';
                }
                if (empty($arField['VALUES']) && ($arField['MULTIPLE'] != 'Y' || $arField['MULTIPLE_CNT'] == 1) && ($arField['TYPE'] == 'CHECKBOX' || ($arField['TYPE'] == 'LIST' && $arField['LIST_TYPE'] == 'C'))) {
                    $arField['VALUES'] = array('Y' => '');
                }
                break;
            case 'TEXTAREA':
                if (empty($arField['ROWS'])) {
                    $arField['ROWS'] = 3;
                }
                if (empty($arField['COLS'])) {
                    $arField['COLS'] = 40;
                }
                break;
            case 'INFO':
                if (empty($arField['ALIGN']) || !in_array($arField['ALIGN'],
                        array('left', 'center', 'right'))) {
                    $arField['ALIGN'] = 'left';
                }
                break;
        }
        if (!$this->bLightInit || $arField['SAVE'] == 'Y') {
            $arTab["FIELDS"][$arField['CODE']] = $arField;
        }
        $this->arOptionsTree[$strTabCode]['FIELDS'][$arField['CODE']] = $arField;
        if ($bSetCurrentValues) {
//            $this->setCurrentValues();
            $this->setCurrentValue($strTabCode, $arField['CODE']);
        }
    }

    public function sortTabs()
    {
        uasort($this->arOptionsTree, function($a, $b){ return $a['SORT'] > $b['SORT'];});
    }

    public function getAdminTabControl($bRefresh = false)
    {
        if(!(empty($this->obAdminTabControl) || $bRefresh))
            return $this->obAdminTabControl;
        return $this->obAdminTabControl = new CAdminTabControl("obAdminTabControl", $this->getTabs());
    }

    /**
     * Разбивает ID опции на код вкладки и код опции
     * @param string $strOptionCode ID опции
     * @return array|bool false в случае неудачи, иначе - массив, где [0] - код вкладки, [1] - код опции
     */
    public function explodeOptionId($strOptionCode)
    {
        $intDivPos = strpos($strOptionCode, '_');
        if(empty($strOptionCode) || $intDivPos === false)
            return false;
        return array(
            substr($strOptionCode, 0, $intDivPos),
            substr($strOptionCode, $intDivPos + 1)
        );
    }

    /**
     * Устанавливает указанной опции значение из $_REQUEST или из поля DEFAULT
     * @param $strTabCode
     * @param $strOptionCode
     * @return bool
     */
    public function setCurrentValue($strTabCode, $strOptionCode)
    {
        $arOption = &$this->arOptionsTree[$strTabCode]['FIELDS'][$strOptionCode];
        if (empty($arOption)) {
            return false;
        }
        if (
            $arOption['TYPE'] == 'LIST'
            && isset($_REQUEST[$strTabCode.'_'.$strOptionCode])
//            && !array_key_exists($_REQUEST[$strTabCode.'_'.$strOptionCode], $arOption['VALUES'])
        ) {
            if (is_array($_REQUEST[$strTabCode.'_'.$strOptionCode])) { // значение в request - множественное
                if (!array_intersect_key(array_flip($_REQUEST[$strTabCode.'_'.$strOptionCode]), $arOption['VALUES'])) {
                    return false;
                }
            } else {
                if (!array_key_exists($_REQUEST[$strTabCode.'_'.$strOptionCode], $arOption['VALUES'])) {
                    return false;
                }
            }
        }
        if (isset($_REQUEST[$strTabCode.'_'.$strOptionCode])) {
            $arOption['VALUE'] = $_REQUEST[$strTabCode.'_'.$strOptionCode];
        } elseif ($arOption['SAVE'] == 'Y') {
            // здесь, по идее, должно быть чтение опции из базы
            // проверка на необходимость чтения из базы делается уже внутри метода
            $this->loadOption($arOption);
        } elseif (isset($arOption['DEFAULT'])) {
            $arOption['VALUE'] = $arOption['DEFAULT'];
        } elseif ($arOption['IS_REQUIRED'] == 'Y') {
            $arOption['VALUE'] = key($arOption['VALUES']);
        }
        return true;
    }

    /**
     * Заполняет массив опций значениями из $_REQUEST или из полей DEFAULT
     */
    public function setCurrentValues()
    {
        foreach ($this->arOptionsTree as $strTabCode => &$arOptionTab) {
            foreach ($arOptionTab['FIELDS'] as &$arOption) {
                if (!$this->bLightInit && !$this->checkOptionDependences($strTabCode, $arOption['CODE'])) {
                    continue;
                }
                $this->setCurrentValue($strTabCode, $arOption['CODE']);
            }
        }
    }

    /**
     * Возвращает массив опций в виде дерева
     * @return array
     */
    public function getOptionsTree()
    {
        return $this->arOptionsTree;
    }

    /**
     * Формирует массив вкладок для конструктора класса CAdminTabControl
     * @param bool|false $bRefresh
     * @param bool|false $bAssoc
     * @return mixed
     */
    public function getTabs($bRefresh = false, $bAssoc = false)
    {
        if (!(empty($this->arTabs) || $bRefresh)) {
            if ($bAssoc) {
                return $this->arTabs;
            } else {
                return array_values($this->arTabs);
            }
        }
        if (!empty($this->arOptionsTree)) {
            foreach ($this->arOptionsTree as $strTabKey => &$arTab) {
                $this->arTabs[$strTabKey] = array(
                    'DIV' => $strTabKey,
                    'TAB' => $arTab['TAB_NAME'],
                    'TITLE' => $arTab['TAB_TITLE'],
                    'ICON' => $arTab['TAB_ICON'],
                    'SORT' => $arTab['SORT'],
                );
            }
            unset($arTab);
        }
        if ($bAssoc) {
            return $this->arTabs;
        } else {
            return array_values($this->arTabs);
        }
    }

    public function getOption($strTabCode, $strOptionCode)
    {
        $arTab = &$this->arOptionsTree[$strTabCode];
        if (empty($arTab)) {
            return false;
        }
        return $arTab['FIELDS'][$strOptionCode];
    }

    /**
     * @param $strTabCode
     * @param $strOptionCode
     * @return mixed
     */
    public function getOptionValue($strTabCode, $strOptionCode)
    {
        $arOption = &$this->getOption($strTabCode, $strOptionCode);
        if (empty($arOption)) {
            return false;
        }
        return $arOption['VALUE'];
    }

    /**
     * Устанавливает значение $value для параметра $strParamCode у опции с кодом $strOptionCode (из вкладки $strTabCode)
     * @param string $strTabCode
     * @param string $strOptionCode
     * @param string $strParamCode
     * @param $value
     * @param bool $bRefresh Если установлен в true, повторно проведется процесс инициализации значения (VALUE из DEFAULT и $_REQUEST)
     * @return bool
     */
    public function setOptionParamValue($strTabCode, $strOptionCode, $strParamCode, $value, $bRefresh = false)
    {
        if (empty($strTabCode) || empty($strOptionCode) || empty($strParamCode)) {
            return false;
        }
        $arTab = &$this->arOptionsTree[$strTabCode];
        if (empty($arTab)) {
            return false;
        }
        $arTreeOption = &$arTab['FIELDS'][$strOptionCode];
        if (empty($arTreeOption)) {
            return false;
        }
        $arTreeOption[$strParamCode] = $value;
        if ($bRefresh) {
            $this->setCurrentValue($strTabCode, $strOptionCode);
        }
        return true;
    }

    /**
     * Выводит опцию в представлении, зависимом от ее типа
     * @param array $arOption Массив с параметрами опции
     * @return bool
     */
    public function showOption(&$arOption)
    {
        if (!is_array($arOption) || empty($arOption['TYPE'])) {
            return false;
        }
        // вывод "нестандартных" блоков - заголовки и подобное
        switch ($arOption['TYPE']) {
            case 'CUSTOM':
                echo htmlspecialchars_decode($arOption['VALUE']);
                return true;
            case 'HEADING':
                ?><tr class="heading">
                    <td colspan="2"><b><?=$arOption['VALUE']?></b></td>
                </tr><?
                return true;
            case 'INFO':
                ?><tr>
                    <td colspan="2" align="center">
                        <div class="adm-info-message-wrap" align="<?=$arOption['ALIGN']?>">
                            <div class="adm-info-message"><?=htmlspecialchars_decode($arOption['VALUE'])?></div>
                        </div>
                    </td>
                </tr><?
                return true;
        }
        $strMultiple = '';
        if ($arOption['MULTIPLE'] == 'Y') {
            $strMultiple = ' multiple';
        }
        if (intval($arOption['MULTIPLE_CNT'])) {
            $strMultiple .= ' size="'.$arOption['MULTIPLE_CNT'].'"';
        }
        $strId = (!isset($arOption['MULTIPLE']) || $arOption['MULTIPLE'] != 'Y') ? ' id="'.$arOption['ID'].'"' : '';
        $strName = ' name="'.$arOption['ID'].((!isset($arOption['MULTIPLE']) || $arOption['MULTIPLE'] != 'Y') ? '"' : '[]"');
        $strHidden = !empty($arOption['HIDDEN']) ? ' hidden' : '';

        $strClass = $arOption['REFRESH'] == 'Y' ? ' class="js-refresh-change"' : '';

        $strCommonOptionData = $strId.$strName.$strMultiple.$strHidden.$strClass;
        if ($arOption['MULTIPLE'] == 'Y' && !is_array($arOption['VALUE'])) {
            $arOption['VALUE'] = array($arOption['VALUE']);
        }
        $currentOptionValue = $arOption['VALUE'];
        ?><tr>
            <td width="40%" nowrap<?=($arOption['TYPE']=="TEXTAREA") ? ' class="adm-detail-valign-top"' : ''?>><?
                if (!empty($arOption['NAME'])) {
                    $strRequired = $arOption['IS_REQUIRED'] == 'Y' ? ' class="adm-required-field"' : '';
                    if ($arOption['MULTIPLE'] != 'Y') {
                        ?><label for="<?=$arOption['ID']?>"<?=$strRequired?>><?=$arOption['NAME']?>:</label><?
                    } else {
                        ?><span<?=$strRequired?>><?=$arOption['NAME']?></span><?
                    }
                }
            ?></td>
            <td width="60%"><?
                switch ($arOption['TYPE']) {
                    case 'PASSWORD':
                    case 'TEXT':
                    default:
                        $strType = 'text';
                        if($arOption['TYPE'] == 'PASSWORD')
                            $strType = 'password';
                        ?><input<?=$strCommonOptionData?> type="<?=$strType?>" value="<?=$currentOptionValue?>"><?
                        break;
                    case 'TEXTAREA':
                        ?><textarea<?=$strCommonOptionData?> rows="<?=$arOption['ROWS']?>" cols="<?=$arOption['COLS']?>"><?=$currentOptionValue?></textarea><?
                        break;
                    case 'RADIO':
                    /** @noinspection PhpMissingBreakStatementInspection */
                    case 'CHECKBOX':
                        $arOption['LIST_TYPE'] = 'C';
                    case 'LIST':
                        if ($arOption['LIST_TYPE'] == 'L') {
                            ?><select<?=$strCommonOptionData?>><?
                            $bSelectedExists = false;
                            ob_start();
                            foreach ($arOption['VALUES'] as $strValue => $strName) {
                                if ($arOption['MULTIPLE'] == 'Y') {
                                    if (!is_array($currentOptionValue)) {
                                        $currentOptionValue = array($currentOptionValue);
                                    }
                                    $bSelected = in_array($strValue, $currentOptionValue);
                                } else {
                                    $bSelected = $currentOptionValue == $strValue;
                                }
                                $bSelectedExists = $bSelectedExists || $bSelected;
                                ?><option value="<?=$strValue?>" <?=$bSelected ? ' selected' : ''?>><?=$strName?></option><?
                            }
                            $strOptions = ob_get_clean();
                            if ($arOption['IS_REQUIRED'] != 'Y') {
                                ?><option value="" <?=!$bSelectedExists ? ' selected' : ''?>>(Не установлено)</option><?
                            }
                            echo $strOptions;
                            ?></select><?
                        } elseif ($arOption['LIST_TYPE'] == 'C') {
                            $bHideLabel = count($arOption['VALUES']) == 1;
                            $strInputType = ($arOption['MULTIPLE'] == 'Y' || count($arOption['VALUES']) == 1) ? 'checkbox' : 'radio';
                            foreach ($arOption['VALUES'] as $strKey => $strValue) {
                                $bChecked = $currentOptionValue == $strKey || (is_array($currentOptionValue) && in_array($strKey, $currentOptionValue));
                                ?><div><?
                                    if ($strInputType == 'checkbox') {
                                        ?><input<?=$strName?> id="<?=$arOption['ID'].'_'.$strKey?>" type="hidden" value="" checked/><?
                                    }
                                    ?><input<?=$strCommonOptionData?> id="<?=$arOption['ID'].'_'.$strKey?>" type="<?=$strInputType?>" value="<?=$strKey?>"<?=$bChecked ? ' checked' : ''?>/><?
                                    if (!$bHideLabel) {
                                        ?><label for="<?=$arOption['ID'].'_'.$strKey?>"><?=$strValue?></label><?
                                    }
                                ?></div><?
                            }
                        }
                        break;
                    case 'BUTTON':
                        ?><input type="button"<?=$strCommonOptionData?> value="<?=htmlspecialchars($currentOptionValue)?>"><?
                        break;
                }
            ?></td>
        </tr><?
        return true;
    }

    /**
     * @param string $strTabCode Код вкладки, опции которой будут выводиться
     */
    public function showTabOptions($strTabCode)
    {
        foreach ($this->arOptionsTree[$strTabCode]['FIELDS'] as $arOption) {
//            if($arOption) надо бы сделать checkOptionArray, который проверяет его корректность
            $bCheck = $this->checkOptionDependences($strTabCode, $arOption['CODE']);
            if ($bCheck) {
                $this->showOption($arOption);
            }
        }
    }

    /**
     * Проверяет зависимость опции от других опций.
     * Значения опций из массива DEPENDENCES должны быть не пустыми,
     * а значения опций из массива DEPENDENCES_E должны быть пустыми
     * @param string $strTabCode
     * @param string $strOptionCode
     * @return bool
     */
    public function checkOptionDependences($strTabCode, $strOptionCode)
    {
        $arOption = &$this->getOption($strTabCode, $strOptionCode);
        // если с описанием поля что-то не так, не выводим его
        if (empty($arOption)) {
            return false;
        }
        // если нет зависимостей, разрешим вывод
        if (empty($arOption['DEPENDENCES']) && empty($arOption['DEPENDENCES_E'])) {
            return true;
        }
        if (is_string($arOption['DEPENDENCES'])) {
            $arOption['DEPENDENCES'] = array($arOption['DEPENDENCES']);
        }
        if (is_string($arOption['DEPENDENCES_E'])) {
            $arOption['DEPENDENCES_E'] = array($arOption['DEPENDENCES_E']);
        }
        if (!empty($arOption['DEPENDENCES'])) {
            foreach ($arOption['DEPENDENCES'] as $strDependence) {
                $arCodes = $this->explodeOptionId($strDependence);
                $depVal = $this->getOptionValue($arCodes[0], $arCodes[1]);
                if ($arCodes == false || empty($depVal)) {
                    return false;
                }
            }
        }
        if (!empty($arOption['DEPENDENCES_E'])) {
            foreach ($arOption['DEPENDENCES_E'] as $strDependence) {
                // параметр не указан - false
                // параметр указан и не пуст - false
                $arCodes = $this->explodeOptionId($strDependence);
                if ($arCodes == false) {
                    return false;
                }
                $depOptionVal = $this->getOptionValue($arCodes[0], $arCodes[1]);
//                if(!isset($depOptionVal) || (isset($depOptionVal) && !empty($depOptionVal)))
                if (!empty($depOptionVal)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Сохраняет данные формы в случае, если установлен флаг $_POST[save]
     */
    public function saveOptions()
    {
        global $APPLICATION;
        $strModuleID = $_REQUEST['mid'];
        $strGroupRight = $APPLICATION->GetGroupRight($strModuleID);
        if ($strGroupRight < 'W') {
            return false;
        }
        if (!(isset($_POST['save']) || isset($_POST['apply']) || isset($_POST['RestoreDefaults']))) {
            return false;
        }
        $arOptionsTree = &$this->getOptionsTree();
        if (!empty($arOptionsTree)) {
            foreach ($arOptionsTree as $strTabCode => &$arTab) {
                if (!empty($arTab['FIELDS'])) {
                    foreach ($arTab['FIELDS'] as &$arOption) {
                        if ($arOption['SAVE'] != 'Y') {
                            continue;
                        }
                        $strValue = $_POST[$arOption['ID']];
                        switch ($arOption['TYPE']) {
                            case 'CHECKBOX':
                                $strValue = $strValue == 'Y' ? 'Y' : 'N';
                                break;
                            case 'TEXTAREA':
                                //break;
                            case 'TEXT':
                                //break;
                            default:
                                MyString::textProcessing($strValue);
                        }
                        COption::SetOptionString($strModuleID, $arOption['ID'],
                            isset($_POST['RestoreDefaults']) ? $arOption['DEFAULT'] : $strValue, $arOption['DEFAULT']);
                    }
                    unset($arOption);
                }
            }
            unset($arTab);
        }
        LocalRedirect($APPLICATION->GetCurPage()."?mid=".urlencode($strModuleID)."&page=".urlencode($_REQUEST['page'])."&lang=".urlencode(LANGUAGE_ID)."&".$this->obAdminTabControl->ActiveTabParam());
        return true; // for IDE silence
    }

    public function loadOptions()
    {
        $arOptions = &$this->getOptionsTree();
        if (!empty($arOptions['FIELDS'])) {
            foreach ($arOptions['FIELDS'] as &$arOption) {
                $this->loadOption($arOption);
            }
            unset($arOption);
        }
    }

    /**
     * Загружает значение опции из базы, в случае его пустоты - подставляет дефолтное значение
     * @param array $arOption Массив с описанием опции, передается и изменяется по ссылке
     * @return bool
     */
    public function loadOption(&$arOption)
    {
        if ($arOption['SAVE'] != 'Y')
            return false;
        $arOption['VALUE'] = htmlspecialchars_decode(COption::GetOptionString($this->strModuleId, $arOption['ID'], $arOption['DEFAULT']));
        return true;
    }

    /**
     * Показывает общую для всей формы подсказку
     * @param string $strText Текст для вывода. Можно передавать HTML.
     */
    public function showCommonNote($strText)
    {
        echo BeginNote();
        echo htmlspecialchars_decode($strText);
        echo EndNote();
    }

    /**
     * Выводит форму со всеми вкладками и их опциями
     * @param string $strFormId Идентификатор, который будет установлен для формы
     * @param string $strFormWrapperId Идентификатор обертки формы. Нужен для работы ajax
     * @param array|bool $arButtons Массив с идентификаторами кнопок формы для вывода. Возможные значения: save, apply, default, refresh
     */
    public function showForm($strFormId, $strFormWrapperId, $arButtons = false)
    {
        global $APPLICATION;
        $strModuleID = $_REQUEST['mid'];
        $this->obAdminTabControl->Begin();
        $strAction = ''; // MySitePage::getCurPageParam(true, array('lang' => LANGUAGE_ID))
        ?><form id="<?=$strFormId?>" method="post" action="<?=$strAction?>"><?
            echo bitrix_sessid_post();
            $arOptions = &$this->getOptionsTree();
            $this->obAdminTabControl->SetSelectedTab();
            foreach ($arOptions as $strTabCode => $arTab) {
                $this->obAdminTabControl->BeginNextTab();
                $this->showTabOptions($strTabCode);
            }
            if (is_array($arButtons)) {
                $this->obAdminTabControl->Buttons();
                if (in_array('save', $arButtons)) {
                    ?><input type="submit" name="save" value="<?= GetMessage("MAIN_SAVE") ?>"
                             title="<?= GetMessage("MAIN_OPT_SAVE_TITLE") ?>" class="adm-btn-save"><?
                }
                if (in_array('apply', $arButtons)) {
                    ?><input type="submit" name="apply" value="<?= GetMessage("MAIN_OPT_APPLY") ?>"
                             title="<?= GetMessage("MAIN_OPT_APPLY_TITLE") ?>"><?
                }
                if (in_array('default', $arButtons)) {
                    ?><input type="submit" name="RestoreDefaults" title="<?
                    echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>" OnClick="return confirm('<?
                    echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")) ?>')" value="<?
                    echo GetMessage("MAIN_RESTORE_DEFAULTS") ?>"><?
                }
                if (in_array('refresh', $arButtons)) {
                    ?><input type="button" title="Обновить" class="js-refresh-click" value="Обновить"><?
                }
            }
            $this->obAdminTabControl->End();
        ?></form><?
        if (!SitePage::isAjax()) {
            ?><script>
            var $wrapper = $('#' + <?=json_encode($strFormWrapperId)?>);

            function makeRequest(action, additionalData, callback) {

                var $form = $('#' + <?=json_encode($strFormId)?>);
                if (!$form.length)
                    return false;
                var $actionInput = $form.find('input[name="action"]');
                if (!$actionInput.length) {
                    $form.append('<input type="hidden" name="action" value="' + action + '">');
                }
                else {
                    $actionInput.attr('value', action);
                }
                var $data = $form.serializeArray();

                if (additionalData !== undefined) {
                    for (var key in additionalData) {
                        $data.push({name: key, value: additionalData[key]});
                    }
                }

                $.ajax({
                    url: $form.attr('action'),
                    dataType: "json",
                    data: $data,
                    type: "POST",
                    success: function (answ) {
                        $wrapper.html(answ.html);
                        if (answ.location.length) {
                            history.pushState(null, '', answ.location);
                        }
                    }
                }).done(function () {
                    if (typeof callback === 'function') {
                        callback();
                    }
                });
            }
            $(function(){
                $wrapper.on('change', '.js-refresh-change', function(){
                    makeRequest('refresh');
                });
                $wrapper.on('click', ' .js-refresh-click', function(){
                    makeRequest('refresh');
                });
            });
        </script><?
        }
    }
}
