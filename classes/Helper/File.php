<?php

namespace Rapid\Dev\Helper;

use CFile;
use Exception;

class File
{
    /**
     * Функция возвращает расширение файла
     * @param string $filename Название файла
     * @param bool $bWithDot
     * @return string
     */
    public static function getExtension($filename, $bWithDot = true)
    {
        return substr($filename, strrpos($filename, '.') + (!$bWithDot));
    }

    /**
     * Функция возвращает имя файла без расширения
     * @param string $filename Название файла
     * @param bool|true $bBaseName
     * @return string
     */
    public static function getName($filename, $bBaseName = true)
    {
        if ($bBaseName) {
            $filename = basename($filename);
        }
        return substr($filename, 0, strrpos($filename, '.'));
    }

    public static function getPostFiles($files, $strFilename = false)
    {
        $arFiles = Array();
        foreach ($files as $strPropertyName => $arFile) {
            foreach ($arFile as $strFileKey => $obFileParams) {
                if (!is_array($obFileParams)) {
                    continue;
                }
                foreach ($obFileParams as $intParamIterator => $strParamValue) {
                    $arFile[$intParamIterator][$strFileKey] = $strParamValue;
                }
                unset($arFile[$strFileKey]);
            }
            $arFiles[$strPropertyName] = $arFile;
        }
        return $arFiles[$strFilename] ?: $arFiles;
    }

    /**
     * Форматирование размера файла
     * @param int $size Размер в байтах
     * @return string
     */
    public static function formatSize($size = 0)
    {
        return CFile::FormatSize($size);
    }

    /**
     * Генерация массива для сохранения файла
     * @param array $arTmpFile Массив "Файл" из $_FILES
     * @param int $fileValueId ИД свойства где хранится файл
     * @param int $deleteFlag Флаг на удаление (0 - нет, 1 - да)
     * @param string $fileErrorTitle Название файла для ошибки в именительном падеже
     * @return array
     * @throws Exception
     */
    public static function formatSaveArray($arTmpFile, $fileValueId, $deleteFlag, $fileErrorTitle = 'файл')
    {
        $arFile = array();
        if (isset($arTmpFile) && $arTmpFile['error'] !== UPLOAD_ERR_NO_FILE) {
            // Сохранение файла
            if ($arTmpFile['error'] || !is_uploaded_file($arTmpFile['tmp_name'])) {
                throw new Exception('Не удалось загрузить ' . $fileErrorTitle . '. [' . $arTmpFile['error'] . ']');
            }
            $arFile = array('VALUE' => $arTmpFile);
        } elseif ($fileValueId > 0 && $deleteFlag == 1) {
            // Удаление файла
            $arFile = array(
                $fileValueId => array(
                    'VALUE' => array('del' => 'Y')
                )
            );
        }
        return $arFile;
    }

    /**
     * Функция перезаписывает содержимое файла, выполняя str_replace
     * @param string $strPathToFile путь к файлу
     * @param array $arReplaces массив замен вида "старое значение" => "новое значение"
     */
    public static function translateFile($strPathToFile, &$arReplaces = Array())
    {
        file_put_contents(
            $strPathToFile,
            str_replace(
                array_keys($arReplaces),
                array_values($arReplaces),
                file_get_contents($strPathToFile)
            )
        );
    }

    /**
     * <p>Копирует файлы и каталоги. Возвращает "true" при успешном завершении копирования и "false" - в противном случае.</p>
     * <p>Аналогичен стандартному методу, за исклчением того, что в параметр игнора можно передавать не строку, а массив</p>
     *
     *
     *
     * @param string $path_from Абсолютный путь к каталогу (файлу), содержимое которой нужно
     * скопировать.
     *
     *
     *
     * @param string $path_to Абсолютный путь к каталогу, в который надо скопировать. Путь
     * указанный в данном параметре будет предварительно проверен
     * функцией <a href="http://dev.1c-bitrix.ru/api_help/main/functions/file/checkdirpath.php">CheckDirPath</a>.
     *
     *
     *
     * @param bool $ReWrite = true Перезаписывать ли существующие файлы.<br> Необязательный
     * параметр, по умолчанию равен "true" (перезаписывать).
     *
     *
     *
     * @param bool $Recursive = false Копировать ли рекурсивно все подкаталоги и файлы входящие в путь
     * указанный в параметре <i>from</i>.<br> Необязательный параметр, по
     * умолчанию равен "false" (копировать не рекурсивно).
     *
     *
     *
     * @param bool $bDeleteAfterCopy = false Удалить ли путь указанный в параметре <i>from</i> после того как
     * копирование будет завершено (т.е. по сути - сделать перенос
     * каталогов и файлов).<br> Необязательный параметр, по умолчанию
     * равен "false" (не удалять).
     *
     *
     *
     * @param array $arExclude = "" Если данный параметр задан, то из копирования будут исключены
     * файлы / папки, название которых начинается со значения данного
     * параметра.
     *
     * @return bool
     *
     *
     * <h4>Example</h4>
     * <pre>
     * &lt;?
     * // копируем файлы из папки /temp1/ в папку /temp2/
     * <b>CopyDirFiles</b>($_SERVER["DOCUMENT_ROOT"]."/temp1", $_SERVER["DOCUMENT_ROOT"]."/temp2");
     * ?&gt;
     *
     * Если копируется один файл, то нужно строго указать не только источник, но и приемник. Имя файла-приемника может отличаться от имени источника.
     *
     * CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/temp1/file.ex", $_SERVER["DOCUMENT_ROOT"]."/temp2/file.ex");
     * </pre>
     *
     *
     *
     * <h4>See Also</h4>
     * <ul><li> <a href="http://dev.1c-bitrix.ru/api_help/main/reference/cfile/copyfile.php">CFile::CopyFile</a> </li></ul><a
     * name="examples"></a>
     *
     *
     * @static
     * @link http://dev.1c-bitrix.ru/api_help/main/functions/file/copydirfiles.php
     * @author Bitrix
     */
    public static function copyDirFiles($path_from, $path_to, $ReWrite = true, $Recursive = false, $bDeleteAfterCopy = false, $arExclude = array())
    {
        if (strpos($path_to . "/", $path_from . "/") === 0 || realpath($path_to) === realpath($path_from)) {
            return false;
        }
        if (is_string($arExclude) && !empty($arExclude)) {
            $arExclude = array($arExclude);
        }
        if (is_dir($path_from)) {
            CheckDirPath($path_to . "/");
        } elseif (is_file($path_from)) {
            $p = bxstrrpos($path_to, "/");
            $path_to_dir = substr($path_to, 0, $p);
            CheckDirPath($path_to_dir . "/");
            if (file_exists($path_to) && !$ReWrite) {
                return false;
            }
            @copy($path_from, $path_to);
            if (is_file($path_to)) {
                @chmod($path_to, BX_FILE_PERMISSIONS);
            }
            if ($bDeleteAfterCopy) {
                @unlink($path_from);
            }
            return true;
        } else {
            return true;
        }
        if ($handle = @opendir($path_from)) {
            while (($file = readdir($handle)) !== false) {
                if ($file == "." || $file == "..") {
                    continue;
                }
                //----------------------------------------------------------
                // if (strlen($strExclude)>0 && substr($file, 0, strlen($strExclude))==$strExclude)
                //     continue;
                //----------------------------------------------------------
                $strPath = $path_to;
                $bContinue = false;
                if (!empty($arExclude)) {
                    foreach ($arExclude as $strFileExclude) {
                        if (strpos($strPath . '/' . $file, $strFileExclude) !== false) {
                            $bContinue = true;
                            break;
                        }
                    }
                }
                if ($bContinue) {
                    continue;
                }
                //----------------------------------------------------------
                if (is_dir($path_from . "/" . $file) && $Recursive) {
                    self::CopyDirFiles($path_from . "/" . $file, $path_to . "/" . $file, $ReWrite, $Recursive, $bDeleteAfterCopy, $arExclude);
                    if ($bDeleteAfterCopy) {
                        @rmdir($path_from . "/" . $file);
                    }
                } elseif (is_file($path_from . "/" . $file)) {
                    if (file_exists($path_to . "/" . $file) && !$ReWrite) {
                        continue;
                    }
                    @copy($path_from . "/" . $file, $path_to . "/" . $file);
                    @chmod($path_to . "/" . $file, BX_FILE_PERMISSIONS);
                    if ($bDeleteAfterCopy) {
                        @unlink($path_from . "/" . $file);
                    }
                }
            }
            @closedir($handle);
            if ($bDeleteAfterCopy) {
                @rmdir($path_from);
            }
            return true;
        }
        return false;
    }

    /**
     * Метод возвращает список файлов в директории
     * @param string $strDirectory Директория, в которой нужно найти файлы
     * @param bool $arNameFilter Массив с фильтром имен файлов
     * @param bool $bFilterExclude Тип фильтрации имен. Если false, то найденные совпадения будут оставлены в результирующем массиве, иначе - удалены из него.
     * @return array|bool
     */
    public static function getFileList($strDirectory, $arNameFilter = false, $bFilterExclude = false)
    {
        if (strpos($strDirectory, $_SERVER['DOCUMENT_ROOT']) === false) {
            $strDirectory = $_SERVER['DOCUMENT_ROOT'] . '/' . ltrim($strDirectory, '/');
        }
        if (!file_exists($strDirectory) || !is_dir($strDirectory)) {
            return false;
        }
        if (!is_array($arNameFilter) || empty($arNameFilter)) {
            $arNameFilter = array(
                '.',
                '..',
                '.section.php'
            );
            $bFilterExclude = true;
        }
        $arResult = scandir($strDirectory);
        foreach ($arResult as $intKey => $strDir) {
            if (!($bFilterExclude xor in_array($strDir, $arNameFilter))) {
                unset($arResult[$intKey]);
            }
        }
        return $arResult;
    }

    public static function getFileListRecursive($strDirectory)
    {
        if (strpos($strDirectory, $_SERVER['DOCUMENT_ROOT']) === false) {
            $strDirectory = $_SERVER['DOCUMENT_ROOT'] . '/' . trim($strDirectory, '/');
        }
        if (!file_exists($strDirectory) || !is_dir($strDirectory)) {
            return array();
        }
        $arFiles = array();
        $arDirContent = File::getFileList($strDirectory);
        foreach ($arDirContent as $intKey => $strDir) {
            $strFullPath = $strDirectory . DIRECTORY_SEPARATOR . $strDir;
            if (!is_dir($strFullPath)) {
                $arFiles[] = $strFullPath;
            } else {
                $arChildFiles = self::getFileListRecursive($strFullPath);
                $arFiles = array_merge($arFiles, $arChildFiles);
            }
        }
        return $arFiles;
    }

    public static function getFileTreeRecursive($strDirectory)
    {
        if (strpos($strDirectory, $_SERVER['DOCUMENT_ROOT']) === false) {
            $strDirectory = $_SERVER['DOCUMENT_ROOT'] . '/' . trim($strDirectory, '/');
        }
        if (!file_exists($strDirectory) || !is_dir($strDirectory)) {
            return array();
        }
        $arDirContent = File::getFileList($strDirectory);
        $arFiles = array();
        foreach ($arDirContent as $intKey => $strDir) {
            $strFullPath = $strDirectory . DIRECTORY_SEPARATOR . $strDir;
            if (!is_dir($strFullPath)) {
                $arFiles[] = $strFullPath;
            } else {
                $arFiles[] = array(
                    'FILE_NAME' => $strDir,
                    'SUBDIR' => $strDirectory,
                    'FULL_PATH' => $strFullPath,
                    'FILES' => self::getFileTreeRecursive($strFullPath)
                );
            }
        }
        return $arFiles;
    }

    public static function UpdateFileContentType($ID, $size)
    {
        global $DB;
        $DB->Query("UPDATE b_file SET CONTENT_TYPE='" . $DB->ForSql($size, 255) . "' WHERE ID=" . intval($ID));
        CFile::CleanCache($ID);
    }

    public static function UpdateFileSize($ID, $size)
    {
        global $DB;
        $DB->Query("UPDATE b_file SET FILE_SIZE='" . $DB->ForSql($size, 20) . "' WHERE ID=" . intval($ID));
        CFile::CleanCache($ID);
    }

    /**
     * Проверяет существование файла, при успехе - подключает его
     * @param string $strFile Путь до файла. Может быть как с DOCUMENT_ROOT, так и без него.
     * @param bool $bOnce Если true, подключение будет с помощью функции include_once, иначе - include
     * @return bool
     */
    public static function includeIfExists($strFile, $bOnce = false)
    {
        $strFile = $_SERVER['DOCUMENT_ROOT'].'/'.ltrim(str_replace($_SERVER['DOCUMENT_ROOT'], '', $strFile), '/');
        if (file_exists($strFile)) {
            if ($bOnce) {
                /** @noinspection PhpIncludeInspection */
                include_once $strFile;
            } else {
                /** @noinspection PhpIncludeInspection */
                include $strFile;
            }
            return true;
        }
        return false;
    }

    public static function readCsv($path)
    {
        $rs = fopen($path, 'r');
        $arLines = [];
        while ($line = fgetcsv($rs)) {
            $arLines[] = $line;
        }
        return $arLines;
    }
}
