<?php

namespace Rapid\Dev\Helper;

use CGroup;
use CUser;
use Exception;
use Rapid\Dev\Helper\Traits\AddUpdate;

class User
{
    use AddUpdate;

    /**
     * Получение ИД пользователя
     * @return int|bool
     */
    static public function getId()
    {
        global $USER;
        if (!is_object($USER)) {
            return false;
        }

        return (int)($USER->IsAuthorized() ? $USER->GetID() : false);
    }

    /**
     * Получение инфы о пользователе по ИД
     * @param int $id
     * @return array
     */
    static public function getById($id = 0)
    {
        if ($id <= 0) {
            $id = self::getId();
        }
        $arUser = false;
        if ($id > 0) {
            $arUser = CUser::GetByID($id)->Fetch();
        }

        return $arUser;
    }

    /**
     * Получить полное имя пользователя
     * @param array $arUser
     * @param bool $bNameFirst
     * @param bool $bUseSurname
     * @return string
     */
    static public function getFullName($arUser = [], $bNameFirst = false, $bUseSurname = true)
    {
        if (empty($arUser)) {
            return '';
        }
        $arRes = array();
        if (!$bNameFirst) {
            $arRes[] = $arUser['LAST_NAME'];
        }
        $arRes[] = $arUser['NAME'];
        if ($bUseSurname) {
            $arRes[] = $arUser['SECOND_NAME'];
        }
        if ($bNameFirst) {
            $arRes[] = $arUser['LAST_NAME'];
        }
        return trim(preg_replace('/\s+/', ' ', implode(' ', $arRes)));

    }


    /**
     * Получение списка всех пользователей
     */
    static public function getAllUsers()
    {
        $arUsers = array();
        $by = 'name';
        $order = 'asc';
        $res = CUser::GetList($by, $order, array(), array('SELECT' => array("UF_*")));
        while ($arUser = $res->Fetch()) {
            $arUsers[$arUser['ID']] = $arUser;
        }
        unset($by, $order, $res);

        return $arUsers;
    }

    /**
     * Получить email группы пользователей
     * @param array $id
     * @return string
     */
    static public function getGroupEmails($id = array(1))
    {
        $arUserEmail = array();
        $oUserElement = CUser::GetList($by = 'id', $order = 'desc', array('GROUPS_ID' => $id, 'ACTIVE' => 'Y'));
        while ($arItem = $oUserElement->GetNext()) {
            array_push($arUserEmail, $arItem['EMAIL']);
        }

        return implode(',', $arUserEmail);
    }

    /**
     * Получание IP адреса пользователя
     * @return string
     */
    static public function getIp()
    {
        $ips = array();
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ips[] = trim(strtok($_SERVER['HTTP_X_FORWARDED_FOR'], ','));
        }

        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ips[] = $_SERVER['HTTP_CLIENT_IP'];
        }

        if (isset($_SERVER['REMOTE_ADDR'])) {
            $ips[] = $_SERVER['REMOTE_ADDR'];
        }

        if (isset($_SERVER['HTTP_X_REAL_IP'])) {
            $ips[] = $_SERVER['HTTP_X_REAL_IP'];
        }

        // проверяем ip-адреса на валидность начиная с приоритетного.
        foreach ($ips as &$ip) {
            // если ip валидный обрываем цикл и возвращаем его
            if (preg_match("#^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$#", $ip)) {
                return $ip;
            }
        }

        return '';
    }

    /**
     * Получение ИД группы по коду группы
     * @param (string) $code Код группы
     * @return bool|int ИД группы
     */
    static public function getGroupIdByCode($code)
    {
        $obCache = new Cache(__METHOD__, $code);
        return $obCache->get(function() use ($code) {
            $dbItems = CGroup::GetList($by = 'c_sort', $order = 'asc', array('STRING_ID' => $code));
            if ($arGroup = $dbItems->Fetch()) {
                return $arGroup['ID'];
            }
            return false;
        });
    }

    /**
     * Модификации запросов для пользователей
     * @return bool
     */
    static public function afterEdit()
    {
        global $APPLICATION;
        try {
            Cache::cleanByTags('users');
        } catch (Exception $e) {
            $APPLICATION->throwException($e->getMessage());

            return false;
        }

        return true;
    }

    public static function isAdmin()
    {
        global $USER;
        $arGroupId = self::getGroupIdByCode('admin');

        return $arGroupId && in_array($arGroupId, $USER->GetUserGroupArray());
    }

    /**
     * @param $arData
     * @param bool $intId
     * @param bool|CUser $obData
     * @return bool
     * @throws Exception
     */
    public static function addUpdate($arData, $intId = false, &$obData = false)
    {
        if (!is_object($obData)) {
            $obData = new CUser();
        }
        return static::_addUpdate($obData, $arData, $intId);
    }

    public static function getGroupsArray($strKeyCode = '', $strValueCode = '')
    {
        $dbGroups = CGroup::GetList(
            $by = 'id',
            $order = 'asc',
            array(
                'ACTIVE' => 'Y'
            )
        );
        $arGroups = array();
        while($arGroup = $dbGroups->Fetch())
        {
            if($strKeyCode)
            {
                $arGroups[$arGroup[$strKeyCode]] = $strValueCode ? $arGroup[$strValueCode] : $arGroup;
            }
            else
            {
                $arGroups[] = $strValueCode ? $arGroup[$strValueCode] : $arGroup;
            }
        }
        return $arGroups;
    }
}
