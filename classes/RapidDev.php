<?php
namespace Rapid\Dev;

use Rapid\Dev\Helper\Module\Module;
use Rapid\Dev\Helper\MyDev;

class RapidDev extends Module
{
    protected static $MODULE_ID = 'rapid.dev';
    protected static $filePath = __DIR__; // для определения директории модуля
    protected static $obOptions;
    public static $arOptions = array();

    public static function addEventHandlers($strClassName)
    {
        if (!class_exists($strClassName)) {
            return;
        }
        $arClassMethods = get_class_methods($strClassName);
        if (empty($arClassMethods)) {
            return;
        }
        foreach ($arClassMethods as &$strMethod) {
            if (!preg_match('/^(.+)_On(.+)$/', $strMethod, $arMatches)) {
                continue;
            }
            $strFromModuleId = $arMatches[1];
            $strEventId = 'On'.$arMatches[2];
            AddEventHandler($strFromModuleId, $strEventId, array($strClassName, $strMethod));
        }
        unset($strMethod);
    }
}
