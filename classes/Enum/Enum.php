<?php
namespace Rapid\Dev\Enum;

/**
 * Class Enum
 * @property int id
 * @property string value
 * @property string xmlId
 * @property int sort
 * @property bool isDefault
 */
abstract class Enum
{
    /** @var  int */
    protected $id;
    /** @var  string */
    protected $value;
    /** @var  string */
    protected $xmlId;
    /** @var  int */
    protected $sort;
    /** @var  bool */
    protected $isDefault;
    /** @var string Ключ ассоциативного массива */
    protected $assocKey = 'id';

    public $raw = [];

    protected $querySort = array('SORT' => 'ASC');

    /**
     * Магический метод для возвращения значения запрашиваемого атрибута
     *
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        if (property_exists($this, $name) && in_array($name, array('id', 'value', 'xmlId', 'sort', 'isDefault'))) {
            return $this->$name;
        }
        throw new \Exception('Неверное поле '.$name.' класса '.static::class);
    }

    /**
     * @param array $querySort
     * @return $this
     */
    public function setQuerySort($querySort)
    {
        $this->querySort = $querySort;
        return $this;
    }

    /**
     * @param int $id
     * @return $this
     */
    protected function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $value
     * @return $this
     */
    protected function setValue($value)
    {
        $this->value = trim($value);
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $xmlId
     * @return $this
     */
    protected function setXmlId($xmlId)
    {
        $this->xmlId = trim($xmlId);
        return $this;
    }

    /**
     * @return string
     */
    public function getXmlId()
    {
        return $this->xmlId;
    }

    /**
     * @param int $sort
     * @return $this
     */
    protected function setSort($sort)
    {
        $this->sort = intval($sort);
        return $this;
    }

    /**
     * @return int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @return bool
     */
    public function isDefault()
    {
        return $this->isDefault;
    }

    /**
     * @param bool $isDefault
     * @return $this
     */
    protected function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault == 'Y';
        return $this;
    }

    /**
     * @param string $assocKey
     * @return $this
     */
    public function setAssocKey($assocKey)
    {
        $this->assocKey = $assocKey;
        return $this;
    }

    public function __toString()
    {
        return $this->value;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'value' => $this->value,
            'xmlId' => $this->xmlId,
            'sort' => $this->sort,
            'isDefault' => $this->isDefault,
        ];
    }

    /**
     * @return static[]
     */
    abstract public function getAll();

    public function getById($strId)
    {
        $arValues = $this->setAssocKey('id')->getAll();
        return $arValues[(int)$strId];
    }

    public function getByXmlId($strXmlId)
    {
        $arValues = $this->setAssocKey('xmlId')->getAll();
        return $arValues[$strXmlId];
    }
}
