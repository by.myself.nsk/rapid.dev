<?php

namespace Rapid\Dev\Enum;

use Exception;
use Rapid\Dev\Enum\Item\ItemInterface;
use Rapid\Dev\Enum\Item\Property as EnumProperty;

class Listing implements ListingInterface
{
    /** @var string Ключ ассоциативного массива */
    protected $assocKey = 'id';

    /** @var ItemInterface[] */
    protected $list = [];

    /**
     * @var array
     */
    protected $querySort = array('SORT' => 'ASC');

    /**
     * @param $arProperty
     * @param string $assocKey
     * @return Listing
     * @throws Exception
     */
    public static function createFromProperty($arProperty, $assocKey = 'id')
    {
        $obList = new static();

        $arList = [];
        switch ($arProperty['PROPERTY_TYPE']) {
            case 'L':
                $arList = EnumProperty::getList(['SORT' => 'ASC'], ['IBLOCK_ID' => $arProperty['IBLOCK_ID'], 'CODE' => $arProperty['CODE']], $assocKey);
                break;
            case 'E':
                $arList = \Rapid\Dev\Enum\Item\IblockElement::getList(['SORT' => 'ASC'], ['IBLOCK_ID' => (int)$arProperty['LINK_IBLOCK_ID']], $assocKey);
                break;
            case 'G':
                $arList = \Rapid\Dev\Enum\Item\IblockSection::getList(['SORT' => 'ASC'], ['IBLOCK_ID' => (int)$arProperty['LINK_IBLOCK_ID']], $assocKey);
                break;
            case 'S':
                switch ($arProperty['USER_TYPE']) {
                    case 'directory':
                    case 'directory_id':
//                        $obItem = (new HlBlock())->setHlBlockTblName($arProperty['USER_TYPE_SETTINGS']['TABLE_NAME'])->getById($arProperty['VALUE']);
                        // тут есть проблема с указанием id hl-блока
//                    $arList = \Rapid\Dev\Enum\Item\HlBlock::getList([], ['IBLOCK_ID' => (int)$arProperty['LINK_IBLOCK_ID']], $assocKey);
                        break;
                    case 'UserID':
                        $arList = \Rapid\Dev\Enum\Item\User::getList([], [], $assocKey);
                        break;
                }
                break;
            default:
                throw new Exception('Property is not enum');
                break;
        }
        $obList->setList($arList);
        return $obList;
    }

    public static function createFromUserField($arField, $assocKey = 'id')
    {
        // должен создать объект self, заполнить его list, и вернуть
        // list заполняется объектами ItemInterface в зависимости от параметров в $arField
    }

    /**
     * @param array $querySort
     * @return $this
     */
    public function setQuerySort($querySort)
    {
        $this->querySort = $querySort;
        return $this;
    }

    /**
     * @param string $assocKey
     * @return $this
     */
    public function setAssocKey($assocKey)
    {
        $this->assocKey = $assocKey;
        return $this;
    }

    public function getList()
    {
        return $this->list;
    }

    public function setList($arList)
    {
        $this->list = $arList;
    }

    public function addToList($obItem)
    {
        $this->list[$obItem->{$this->assocKey}] = $obItem;
    }

    public function prepareToSelect()
    {
        $arRes = [];
        foreach ($this->list as $item) {
//            $arRes[$item->{$this->assocKey}] = $item->getValue();
            $arRes[$item->{$this->assocKey}] = [
                'id' => $item->{$this->assocKey},
                'value' => $item->getValue(),
            ];
        }
        return $arRes;
    }

    public function keys()
    {
        return array_keys($this->list);
    }
}
