<?php
namespace Rapid\Dev\Enum\ObjectField;

class UserField extends ObjectField
{
    public function __construct($entityId, $userFieldCode, $value)
    {
        $arUserField = \CUserTypeEntity::GetList(array(), array('ENTITY_ID' => $entityId, 'FIELD_NAME' => $userFieldCode))->Fetch();
        switch ($arUserField['USER_TYPE_ID']) {
            case 'enumeration': // список
                $this->object = (new \Rapid\Dev\Enum\Item\UserField())->setEntityId('USER')->setPropertyCode($userFieldCode)->getById($value);
                break;
            case 'iblock_section': // привязка к разделам
                $this->object = (new \Rapid\Dev\Enum\Item\IblockSection())->setIblockId($arUserField['SETTINGS']['IBLOCK_ID'])->getById($value);
                break;
            case 'iblock_element': // привязка к элементам
                $this->object = (new \Rapid\Dev\Enum\Item\IblockElement())->setIblockId($arUserField['SETTINGS']['IBLOCK_ID'])->getById($value);
                break;
            case 'hlblock':
                $this->object = (new \Rapid\Dev\Enum\Item\HlBlock())->setHlBlockCode($arUserField['SETTINGS']['HLBLOCK_ID'])->getById($value);
                break;
        }
    }

    public static function canRepresentArray($arProperty)
    {
        if (!is_array($arProperty) || !array_key_exists('USER_TYPE_ID', $arProperty)) {
            return false;
        }
        return in_array($arProperty['USER_TYPE_ID'], ['enumeration', 'iblock_section', 'iblock_element']);
    }
}
