<?php
namespace Rapid\Dev\Enum\ObjectField;

use Exception;
use Rapid\Dev\Enum\Item\HlBlock;
use Rapid\Dev\Enum\Item\IblockElement;
use Rapid\Dev\Enum\Item\IblockSection;
use Rapid\Dev\Enum\Item\Property as EnumProperty;
use Rapid\Dev\Enum\Item\User;

class Property extends ObjectField
{
    public function __construct($arProperty)
    {
        if (!$arProperty) {
            return;
        }
        try {
            switch ($arProperty['PROPERTY_TYPE']) {
                case 'L':
                    $this->object = (new EnumProperty())->setIblockId($arProperty['IBLOCK_ID'])->setPropertyCode($arProperty['CODE'])->getById($arProperty['VALUE']);
                    break;
                case 'E':
                    $this->object = (new IblockElement())->setIblockId($arProperty['LINK_IBLOCK_ID'])->getById($arProperty['VALUE']);
                    break;
                case 'G':
                    $this->object = (new IblockSection())->setIblockId($arProperty['LINK_IBLOCK_ID'])->getById($arProperty['VALUE']);
                    break;
                case 'S':
                    switch ($arProperty['USER_TYPE']) {
                        case 'directory':
                        case 'directory_id':
                            $this->object = (new HlBlock())->setHlBlockTblName($arProperty['USER_TYPE_SETTINGS']['TABLE_NAME'])->getById($arProperty['VALUE']);
                            break;
                        case 'UserID':
                            $this->object = (new User())->getById($arProperty['VALUE']);
                            break;
                    }
                    break;
                default:
                    throw new Exception('Property is not enum');
                    break;
            }
        } catch (Exception $e) {
            // $e->getMessage();
        }
    }

    public static function canRepresentArray($arProperty)
    {
        if (!is_array($arProperty) || !array_key_exists('PROPERTY_TYPE', $arProperty) || !array_key_exists('USER_TYPE', $arProperty)) {
            return false;
        }
        return in_array($arProperty['PROPERTY_TYPE'], ['L', 'G', 'E'])
            || ($arProperty['PROPERTY_TYPE'] == 'S' && in_array($arProperty['USER_TYPE'], ['directory', 'directory_id', 'UserID'])
            || ($arProperty['PROPERTY_TYPE'] == 'N' && $arProperty['USER_TYPE'] == 'Checkbox')
        );
    }
}
