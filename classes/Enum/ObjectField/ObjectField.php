<?php
namespace Rapid\Dev\Enum\ObjectField;

abstract class ObjectField
{
    protected $object;

    public function __call($name, $arguments)
    {
        $this->object->$name($arguments);
    }

    public function __get($name)
    {
        return $this->object->$name;
    }

    public function __set($name, $val)
    {
        $this->object->$name = $val;
    }

    public function __toString()
    {
        return (string)$this->object;
    }

    /**
     * может представлять массив
     * @param $arProperty
     * @return bool
     */
    abstract public static function canRepresentArray($arProperty);
}
