<?php
namespace Rapid\Dev\Enum\Item;

class IblockSection extends Iblock
{
    public static function getQuery($order, $filter)
    {
        $filter = array_merge(['ACTIVE' => 'Y'], $filter);
        return \CIBlockSection::GetList(
            $order,
            $filter,
            false,
            [
                'IBLOCK_ID',
                'ID',
                'NAME',
                'CODE',
                'XML_ID',
                'SORT',
            ]
        );
    }
}
