<?php
namespace Rapid\Dev\Enum\Item;

use Rapid\Dev\Enum\Enum;
use Rapid\Dev\Helper\Cache;
use Rapid\Dev\Helper\Iblock\Iblock as MyIblock;

class UserField extends Enum
{
    /** @var string ID инфоблока */
    protected $entityId;
    /** @var string Символьный код свойства */
    protected $propertyCode;

    public function __construct($arItem = array())
    {
        if (!$arItem) {
            return;
        }
        $this
            ->setId($arItem['ID'])
            ->setValue($arItem['VALUE'])
            ->setXmlId($arItem['XML_ID'])
            ->setSort($arItem['SORT'])
            ->setIsDefault($arItem['DEF']);
    }

    /**
     * @param string $entityId
     * @return $this
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;
        return $this;
    }

    /**
     * @param string $iblockId
     * @return $this
     */
    public function setIblockId($iblockId)
    {
        $this->entityId = 'IBLOCK_'.$iblockId.'_SECTION';
        return $this;
    }

    /**
     * @param string $iblockCode
     * @return $this
     */
    public function setIblockCode($iblockCode)
    {
        $this->entityId = 'IBLOCK_'.MyIblock::get($iblockCode).'_SECTION';
        return $this;
    }

    /**
     * @param string $propertyCode
     * @return $this
     */
    public function setPropertyCode($propertyCode)
    {
        if (strpos($propertyCode, 'UF_') !== 0) {
            $propertyCode = 'UF_'.$propertyCode;
        }
        $this->propertyCode = $propertyCode;
        return $this;
    }

    /**
     * @return $this[]
     */
    public function getAll()
    {
        $obCache = new Cache(__METHOD__, $this->querySort, $this->entityId, $this->propertyCode, $this->assocKey);
        $arRes = $obCache->get(function() {
            $arUserField = \CUserTypeEntity::GetList(
                $this->querySort,
                array(
                    'ENTITY_ID' => $this->entityId,
                    'FIELD_NAME' => $this->propertyCode
                )
            )->Fetch();
            if (!$arUserField) {
                throw new \Exception('Сущность не найдена');
            }
            $obEnum = new \CUserFieldEnum();
            $dbUserFieldValues = $obEnum->GetList(
                $this->querySort,
                array(
                    'USER_FIELD_ID' => intval($arUserField['ID']),
                )
            );
            $arRes = array();
            while ($arItem = $dbUserFieldValues->Fetch()) {
                $obItem = new $this($arItem);
                $strKey = $obItem->{$this->assocKey};
                $arRes[$strKey] = $obItem;
            }
            return $arRes;
        });
        return $arRes;
    }
}
