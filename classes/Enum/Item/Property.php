<?php

namespace Rapid\Dev\Enum\Item;

class Property extends Item
{
    public function __construct($arItem)
    {
        $this
            ->setId($arItem['ID'])
            ->setValue($arItem['VALUE'])
            ->setXmlId($arItem['XML_ID'])
            ->setSort($arItem['SORT'])
            ->setIsDefault($arItem['DEF']);
    }

    public static function getQuery($order, $filter)
    {
        return \CIBlockPropertyEnum::GetList($order, $filter);
    }
}
