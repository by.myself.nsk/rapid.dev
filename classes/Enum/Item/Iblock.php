<?php

namespace Rapid\Dev\Enum\Item;

abstract class Iblock extends Item
{
    public function __construct($arItem = array())
    {
        if (!$arItem) {
            return;
        }
        $this
            ->setId($arItem['ID'])
            ->setValue($arItem['NAME'])
            ->setXmlId($arItem['CODE'])
            ->setSort($arItem['SORT']);
    }
}
