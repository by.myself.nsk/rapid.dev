<?php

namespace Rapid\Dev\Enum\Item;

interface ItemInterface
{
    public static function getById($strId);

    public function __get($name);

    public function getId();

    public function getValue();

    public function getXmlId();

    public function getSort();

    public function isDefault();

    public function __toString();

    public static function getQuery($order, $filter);
}
