<?php

namespace Rapid\Dev\Enum\Item;

use CDBResult;

abstract class Item implements ItemInterface
{
    /** @var  int */
    protected $id;
    /** @var  string */
    protected $value;
    /** @var  string */
    protected $xmlId;
    /** @var  int */
    protected $sort;
    /** @var  bool */
    protected $isDefault;

    public static function getById($strId)
    {
        $arItem = static::getQuery([], ['=ID' => $strId])->GetNext();
        if ($arItem) {
            return new static($arItem);
        }
        return false;
    }

    public static function createFromProperty($arProperty)
    {
        return new static($arProperty);
    }

    abstract public function __construct($arItem);

    /**
     * Магический метод для возвращения значения запрашиваемого атрибута
     *
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        if (property_exists($this, $name) && in_array($name, array('id', 'value', 'xmlId', 'sort', 'isDefault'))) {
            return $this->$name;
        }
        throw new \Exception('Неверное поле '.$name.' класса '.static::class);
    }

    /**
     * @param int $id
     * @return $this
     */
    protected function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $value
     * @return $this
     */
    protected function setValue($value)
    {
        $this->value = trim($value);
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $xmlId
     * @return $this
     */
    protected function setXmlId($xmlId)
    {
        $this->xmlId = trim($xmlId);
        return $this;
    }

    /**
     * @return string
     */
    public function getXmlId()
    {
        return $this->xmlId;
    }

    /**
     * @param int $sort
     * @return $this
     */
    protected function setSort($sort)
    {
        $this->sort = intval($sort);
        return $this;
    }

    /**
     * @return int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @return bool
     */
    public function isDefault()
    {
        return $this->isDefault;
    }

    /**
     * @param bool $isDefault
     * @return $this
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = in_array($isDefault, ['Y', '1']);
        return $this;
    }

    public function __toString()
    {
        return $this->getValue();
    }

    public static function getList($order, $filter, $assocKey = 'id')
    {
        $arRes = [];
        $res = static::getQuery($order, $filter);
        while ($arItem = $res->GetNext()) {
            $obItem = new static($arItem);
            $strKey = $obItem->{$assocKey};
            $arRes[$strKey] = $obItem;
        }
        return $arRes;
    }

    /**
     * @param array $order
     * @param array $filter
     * @return CDBResult
     */
    abstract public static function getQuery($order, $filter);

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'value' => $this->getValue(),
            'xmlId' => $this->getXmlId(),
            'sort' => $this->getSort(),
            'isDefault' => $this->isDefault(),
        ];
    }


}
