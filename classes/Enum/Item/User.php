<?php

namespace Rapid\Dev\Enum\Item;

use CUser;

class User extends Item
{
    public function __construct($arItem)
    {
        $this
            ->setId($arItem['ID'])
            ->setValue(\Rapid\Dev\Helper\User::getFullName($arItem))
            ->setSort(500)
            ->setIsDefault(false);
        ;
    }

    public static function getQuery($order, $filter)
    {
        $by = key($order);
        $order = value($order);
        if (!$by) {
            $by = '';
        }
        if (!$order) {
            $order = '';
        }
        return CUser::GetList($by, $order, $filter, array('SELECT' => array("UF_*")));
    }
}
