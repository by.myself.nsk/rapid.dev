<?php
namespace Rapid\Dev\Enum\Item;

use Rapid\Dev\Helper\MyString;

class HlBlock extends Iblock
{
    protected $customFields = array();

    public function __construct($arItem)
    {
        if (!\CModule::IncludeModule('highloadblock')) {
            throw new \Exception('highloadblock module is not installed');
        }
        $this
            ->setId($arItem['ID'])
            ->setValue($arItem['VALUE'])
            ->setXmlId($arItem['XML_ID'])
            ->setSort($arItem['SORT'])
            ->setIsDefault($arItem['DEF']);
        foreach ($arItem as $strKey => $val) {
            if (substr($strKey, 0, 3) !== 'UF_' || in_array($strKey, array('UF_NAME', 'UF_XML_ID', 'UF_SORT', 'UF_DEF'))) {
                continue;
            }
            $strCamelKey = MyString::convertSnakeToCamel(substr($strKey, 3));
            $this->customFields[$strCamelKey] = $val;
        }
    }



    /**
     * @param array $order
     * @param array $filter
     * @return \CDBResult|\CIBlockResult
     * @throws \Exception
     */
    public static function getQuery($order, $filter)
    {
        if (!\CModule::IncludeModule('highloadblock')) {
            throw new \Exception('highloadblock module is not installed');
        }
        $filter = array_merge(['ACTIVE' => 'Y'], $filter);

        $arParams = array();
        $arParams['filter'] = $filter;
        $arParams['order'] = $order;
//        return $obEntity::getList($arParams); // при неверных параметрах падает

        return new \CDBResult();
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->customFields)) {
            return $this->customFields[$name];
        }
        return false;
    }
}
