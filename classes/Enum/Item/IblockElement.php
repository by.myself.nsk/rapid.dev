<?php

namespace Rapid\Dev\Enum\Item;

class IblockElement extends Iblock
{
    public function __construct($arItem)
    {
        parent::__construct($arItem);
        $this->setIsDefault($arItem['PROPERTY_DEF_VALUE']);
    }

    public static function getQuery($order, $filter)
    {
        $filter = array_merge(['ACTIVE' => 'Y'], $filter);
        return \CIBlockElement::GetList(
            $order,
            $filter,
            false,
            false,
            [
                'IBLOCK_ID',
                'ID',
                'NAME',
                'CODE',
                'XML_ID',
                'SORT',
                'PROPERTY_DEF',
            ]
        );
    }
}
