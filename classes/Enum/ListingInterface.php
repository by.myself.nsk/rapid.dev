<?php

namespace Rapid\Dev\Enum;

interface ListingInterface
{
    public static function createFromProperty($arProperty, $assocKey = 'id');

    public static function createFromUserField($arField, $assocKey = 'id');

    public function setQuerySort($querySort);

    public function setAssocKey($assocKey);

    public function getList();

    public function prepareToSelect();
}
