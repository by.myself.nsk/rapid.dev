<?php

namespace Rapid\Dev;

use CHTTP;
use CJSCore;
use CSite;
use Rapid\Dev\Helper\File;

class Events
{
    public static function main_OnBeforeProlog()
    {
        self::addEventHandlers();
        self::adminTabsBlock();
    }

    private static function addEventHandlers()
    {
        self::registerUserFieldClasses();
        self::registerPropertyClasses();
    }

    private static function registerUserFieldClasses()
    {
        $arClasses = self::getUserFieldClasses();
        foreach ($arClasses as $className) {
            AddEventHandler('main', 'OnUserTypeBuildList', ['\Rapid\Dev\UserType\\'.$className, 'GetUserTypeDescription']);
        }
    }

    private static function registerPropertyClasses()
    {
        $arClasses = self::getPropertyClasses();
        foreach ($arClasses as $className) {
            AddEventHandler('iblock', 'OnIBlockPropertyBuildList', ['\Rapid\Dev\UserType\\'.$className, 'GetUserTypeDescription']);
        }
    }

    private static function getUserFieldClasses()
    {
        $arFiles = self::getUserTypeFiles();
        $arFiles = array_filter($arFiles, function($name){return strpos($name, 'CUserField') !== false;});
        array_walk($arFiles, function(&$name){$name = File::getName($name);});
        return $arFiles;
    }

    private static function getPropertyClasses()
    {
        $arFiles = self::getUserTypeFiles();
        $arFiles = array_filter($arFiles, function($name){return strpos($name, 'CProperty') !== false;});
        array_walk($arFiles, function(&$name){$name = File::getName($name);});
        return $arFiles;
    }

    private static function getUserTypeFiles()
    {
        $strParentDir = realpath(__DIR__ . '/UserType');
        $arFiles = scandir($strParentDir);
        $arFiles = array_diff($arFiles, ['.', '..']);
        return $arFiles;
    }

    private static function adminTabsBlock()
    {
        global $USER, $APPLICATION;
        if (CSite::InDir('/bitrix/admin/') && $USER->IsAuthorized() && $USER->IsAdmin()) {
            CJSCore::Init(array('jquery'));
            $strPathToModuleDir = str_replace($_SERVER['DOCUMENT_ROOT'], '', dirname(dirname(__FILE__)));
            $strIncludePath = $strPathToModuleDir . '/include/admin/adm_tabs_block/script.js';
            $APPLICATION->AddHeadScript($strIncludePath);
        }
    }

    public static function main_OnEpilog()
    {
        self::check404Error();
    }

    private static function check404Error()
    {
        if (
            (!defined('ERROR_404') || ERROR_404 != 'Y')
            && (CHTTP::GetLastStatus() != "404 Not Found")
        ) {
            return;
        }

        global $APPLICATION;
        $APPLICATION->RestartBuffer();

        include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/header.php';
        $filePath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . '404.php';
        if (file_exists($filePath)) {
            include $filePath;
        }
        include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/footer.php';
    }

    public static function main_OnAdminContextMenuShow()
    {
        echo '<style>
			#tab_cont_seo_adv_seo_adv, #seo_adv_seo_adv {
				display: none !important;
			}
		</style>';
    }

    public static function main_OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
    {
        $aGlobalMenu['global_menu_rapid'] = array(
            'menu_id' => 'rapid',
            'text' => 'Ещё',
            "title" => "Модули Rapid",
            'sort' => 1000,
            'items_id' => 'global_menu_rapid',
            "help_section" => "",
            "items" => array(),
        );
        global $USER;
        if ($USER->IsAdmin()) {
            $aModuleMenu[] = array(
                "parent_menu" => "global_menu_rapid",
                "section" => "rapid.dev",
                "sort" => 10,
                'text' => GetMessage('RAPID_DEV_MENU_NAME'),
                "title" => GetMessage('RAPID_DEV_MENU_NAME'),
                "icon" => "",
                "page_icon" => "",
                "module_id" => "rapid.dev",
                'items_id' => 'rapid.dev',
                'items' => array(
                    array(
                        'text' => GetMessage('RAPID_DEV_MENU_MODULE_REINSTALL'),
                        'url' => 'rapid_route.php?mid=rapid.dev&page=reinstall',
                    ),
                    array(
                        'text' => GetMessage('RAPID_DEV_MENU_RECAPTCHA'),
                        'url' => 'rapid_route.php?mid=rapid.dev&page=recaptcha',
                    ),
                ),
            );
        }
    }

    public static function iblock_OnBeforeIBlockAdd(&$arParams)
    {
        $canGenerateElementCode = RapidDev::getOption('seo_iblock_add_set_element_code_generation') == 'Y';
        $canGenerateSectionCode = RapidDev::getOption('seo_iblock_add_set_section_code_generation') == 'Y';
        self::fixSlugForSeo($arParams, $canGenerateElementCode, $canGenerateSectionCode);
        if (RapidDev::getOption('events_iblock_add_set_version_2') == 'Y') {
            self::addVersion2($arParams);
        }
        if (RapidDev::getOption('events_iblock_add_set_read_mode') == 'Y') {
            self::enableReadRightsMode($arParams);
        }
    }

    public static function iblock_OnBeforeIBlockUpdate(&$arParams)
    {
        $canGenerateElementCode = RapidDev::getOption('seo_iblock_edit_set_element_code_generation') == 'Y';
        $canGenerateSectionCode = RapidDev::getOption('seo_iblock_edit_set_section_code_generation') == 'Y';
        self::fixSlugForSeo($arParams, $canGenerateElementCode, $canGenerateSectionCode);
        if (RapidDev::getOption('events_iblock_edit_set_read_mode') == 'Y') {
            self::enableReadRightsMode($arParams);
        }
    }

    public static function iblock_OnBeforeIBlockSectionAdd(&$arParams)
    {
        if (RapidDev::getOption('seo_iblock_section_add_modify_code_to_lower') == 'Y') {
            self::modifyCodeToLower($arParams);
        }
    }

    public static function iblock_OnBeforeIBlockSectionUpdate(&$arParams)
    {
        if (RapidDev::getOption('seo_iblock_section_edit_modify_code_to_lower') == 'Y') {
            self::modifyCodeToLower($arParams);
        }
    }

    public static function iblock_OnBeforeIBlockElementAdd(&$arParams)
    {
        if (RapidDev::getOption('seo_iblock_element_add_modify_code_to_lower') == 'Y') {
            self::modifyCodeToLower($arParams);
        }
    }

    public static function iblock_OnBeforeIBlockElementUpdate(&$arParams)
    {
        if (RapidDev::getOption('seo_iblock_element_edit_modify_code_to_lower') == 'Y') {
            self::modifyCodeToLower($arParams);
        }
    }

    protected static function modifyCodeToLower(&$arParams)
    {
        if (isset($arParams['CODE'])) {
            $arParams['CODE'] = ToLower($arParams['CODE']);
        }
    }

    protected static function enableReadRightsMode(&$arParams)
    {
        $arParams['RIGHTS_MODE'] = 'S';
        $arParams['GROUP_ID'] = array(
            1 => 'X',
            2 => 'R',
        );
    }

    protected static function fixSlugForSeo(&$arParams, $canGenerateElementCode, $canGenerateSectionCode)
    {
        $codes = array_filter([
            $canGenerateElementCode ? 'CODE' : '',
            $canGenerateSectionCode ? 'SECTION_CODE' : '',
        ]);
        if (!$codes) {
            return;
        }
        foreach ($codes as $code) {
            if (!is_array($arParams['FIELDS'][$code]['DEFAULT_VALUE'])) {
                $arParams['FIELDS'][$code]['DEFAULT_VALUE'] = [];
            }
            // так, потому что, например, уникальность может быть задана при создании
            $arParams['FIELDS'][$code]['DEFAULT_VALUE'] = array_merge($arParams['FIELDS'][$code]['DEFAULT_VALUE'], [
                'TRANSLITERATION' => 'Y',
                'TRANS_LEN' => '100',
                'TRANS_CASE' => 'L',
                'TRANS_SPACE' => '-',
                'TRANS_OTHER' => '-',
                'TRANS_EAT' => 'Y',
            ]);
        }
    }

    protected static function addVersion2(&$arParams)
    {
        $arParams['VERSION'] = 2;
    }
}
