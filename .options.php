<?php
/**@global CMain $APPLICATION */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
global $APPLICATION;

return [
    "events" => [
        "TAB_NAME" => "События",
        "TAB_TITLE" => "Параметры обработки событий",
        "TAB_ICON" => "",
        "FIELDS" => [
            [
                "CODE" => 'h_iblock_create',
                'TYPE' => 'HEADING',
                'VALUE' => 'Создание инфоблоков',
            ],
            [
                "CODE" => 'iblock_add_set_version_2',
                'NAME' => 'При добавлении инфоблока устанавливать версию ИБ 2',
                'TYPE' => 'CHECKBOX',
                'DEFAULT' => 'Y',
                'SAVE' => 'Y',
            ],
            [
                "CODE" => 'h_iblock_rights',
                'TYPE' => 'HEADING',
                'VALUE' => 'Права доступа к инфоблокам',
            ],
            [
                "CODE" => 'iblock_add_set_read_mode',
                'NAME' => 'При добавлении инфоблока устанавливать доступ "Чтение" для группы "Все пользователи"',
                'TYPE' => 'CHECKBOX',
                'DEFAULT' => 'Y',
                'SAVE' => 'Y',
            ],
            [
                "CODE" => 'iblock_edit_set_read_mode',
                'NAME' => 'При обновлении инфоблока устанавливать доступ "Чтение" для группы "Все пользователи"',
                'TYPE' => 'CHECKBOX',
'DEFAULT' => 'N',
                'SAVE' => 'Y',
            ],
        ],
    ],
    "seo" => [
        "TAB_NAME" => "SEO",
        "TAB_TITLE" => "Параметры обработки событий для SEO",
        "TAB_ICON" => "",
        "FIELDS" => [
            [
                "CODE" => 'h_iblock_params',
                'TYPE' => 'HEADING',
                'VALUE' => 'Параметры инфоблоков',
            ],
            [
                "CODE" => 'iblock_add_set_section_code_generation',
                'NAME' => 'При добавлении инфоблока устанавливать свойства генерации симв.кода разделов',
                'TYPE' => 'CHECKBOX',
'DEFAULT' => 'Y',
                'SAVE' => 'Y',
            ],
            [
                "CODE" => 'iblock_add_set_element_code_generation',
                'NAME' => 'При добавлении инфоблока устанавливать свойства генерации симв.кода элементов',
                'TYPE' => 'CHECKBOX',
'DEFAULT' => 'Y',
                'SAVE' => 'Y',
            ],
            [
                "CODE" => 'iblock_edit_set_section_code_generation',
                'NAME' => 'При обновлении инфоблока устанавливать свойства генерации симв.кода разделов',
                'TYPE' => 'CHECKBOX',
'DEFAULT' => 'Y',
                'SAVE' => 'Y',
            ],
            [
                "CODE" => 'iblock_edit_set_element_code_generation',
                'NAME' => 'При обновлении инфоблока устанавливать свойства генерации симв.кода элементов',
                'TYPE' => 'CHECKBOX',
'DEFAULT' => 'Y',
                'SAVE' => 'Y',
            ],
            [
                "CODE" => 'h_data_params',
                'TYPE' => 'HEADING',
                'VALUE' => 'Параметры данных',
            ],
            [
                "CODE" => 'iblock_section_add_modify_code_to_lower',
                'NAME' => 'При создании раздела инфоблока приводить симв.код к нижнему регистру',
                'TYPE' => 'CHECKBOX',
'DEFAULT' => 'Y',
                'SAVE' => 'Y',
            ],
            [
                "CODE" => 'iblock_element_add_modify_code_to_lower',
                'NAME' => 'При создании элемента инфоблока приводить симв.код к нижнему регистру',
                'TYPE' => 'CHECKBOX',
'DEFAULT' => 'Y',
                'SAVE' => 'Y',
            ],
            [
                "CODE" => 'iblock_section_edit_modify_code_to_lower',
                'NAME' => 'При обновлении раздела инфоблока приводить симв.код к нижнему регистру',
                'TYPE' => 'CHECKBOX',
'DEFAULT' => 'Y',
                'SAVE' => 'Y',
            ],
            [
                "CODE" => 'iblock_element_edit_modify_code_to_lower',
                'NAME' => 'При обновлении элемента инфоблока приводить симв.код к нижнему регистру',
                'TYPE' => 'CHECKBOX',
'DEFAULT' => 'Y',
                'SAVE' => 'Y',
            ],
        ],
    ],
];
