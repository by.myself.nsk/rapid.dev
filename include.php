<?php

use Bitrix\Main\Localization\Loc;
use Rapid\Dev\Helper\Autoloader;
use Rapid\Dev\Helper\SitePage;
use Rapid\Dev\RapidDev;

global $APPLICATION;
Loc::loadMessages($_SERVER['DOCUMENT_ROOT'].'/local/php_interface/init.php');

$strAutoloaderPath = realpath(__DIR__ . '/classes/Helper/Autoloader.php');
if (file_exists($strAutoloaderPath)) {
    /** @noinspection PhpIncludeInspection */
    include $strAutoloaderPath;
    $obLoader = Autoloader::getInstance();
    $obLoader->addPath(__DIR__ . DIRECTORY_SEPARATOR . 'classes', '\Rapid\Dev\\');
    $obLoader->addPath($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'local' . DIRECTORY_SEPARATOR . 'php_interface' . DIRECTORY_SEPARATOR . 'classes');
}

RapidDev::init();
CModule::IncludeModule('iblock');

if (SitePage::isAdmin()) {
    Loc::loadMessages(__FILE__);
    $APPLICATION->AddHeadString('
        <style>
            .adm-rapid .adm-main-menu-item-icon,
            .adm-main-menu-item-active.adm-rapid .adm-main-menu-item-icon,
            .adm-rapid:hover .adm-main-menu-item-icon {
                background: url(/local/modules/rapid.dev/include/admin/rapid.png) center no-repeat;
                background-size: cover;
                width: 45px;
                height: 45px;
                margin: auto;
            }
        </style>'
    );
}
