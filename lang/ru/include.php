<?php
$MESS = array_merge($MESS, array(
    'RAPID_DEV_DEVELOPER_NAME' => 'Rapid',
    'RAPID_DEV_PROJECT_NAME' => 'Rapid: Модуль разработчика',
    'RAPID_DEV_DEVELOPER_PROJECT_DESCRIPTION' => '',
    'RAPID_DEV_MENU_NAME' => 'Rapid: Модуль разработчика',
    'RAPID_DEV_MENU_CREATE_MAIL_TEMPLATES' => 'Создание почтовых шаблонов',
    'RAPID_DEV_MENU_PROJECT_CHECK' => 'Проверка проекта',
    'RAPID_DEV_MENU_MODULE_REINSTALL' => 'Переустановка модулей',
    'RAPID_DEV_MENU_RECAPTCHA' => 'Настройки g-recaptcha',

    /*    'RAPID_DEV_PARAM_SORT_ASC' => 'По возрастанию',
        'RAPID_DEV_PARAM_SORT_DESC' => 'По убыванию',
        'RAPID_DEV_PARAM_SORT_ID' => 'ID',
        'RAPID_DEV_PARAM_SORT_NAME' => "Название",
        'RAPID_DEV_PARAM_SORT_ACT' => "Дата начала активности",
        'RAPID_DEV_PARAM_SORT_SORT' => "Сортировка",
        'RAPID_DEV_PARAM_SORT_MODIFY' => "Дата последнего изменения",
        'RAPID_DEV_PARAM_SORT_BY1' => "Поле сортировки 1",
        'RAPID_DEV_PARAM_SORT_BY2' => "Поле сортировки 2",
        'RAPID_DEV_PARAM_SORT_ORDER1' => "Направление сортировки 1",
        'RAPID_DEV_PARAM_SORT_ORDER2' => "Направление сортировки 2",

        'RAPID_DEV_PARAM_BLOCK_HEADER' => 'Заголовок блока',
        'RAPID_DEV_PARAM_RESIZE_WIDTH' => 'Ширина картинки',
        'RAPID_DEV_PARAM_RESIZE_HEIGHT' => 'Высота картинки',
        'RAPID_DEV_PARAM_RESIZE_TYPE' => 'Как масштабировать картинку',
        'RAPID_DEV_PARAM_RESIZE_TYPE_PROPORTIONAL' => 'Пропорционально',
        'RAPID_DEV_PARAM_RESIZE_TYPE_CROP' => 'Обрезать',

        'RAPID_DEV_PARAM_LINK_URL' => 'Ссылка',
        'RAPID_DEV_PARAM_LINK_TEXT' => 'Текст ссылки',*/
));
