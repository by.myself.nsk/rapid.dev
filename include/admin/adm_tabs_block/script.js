BX.ready(function () {
    $('body').on('click', '.adm-detail-tab', function () {
        var $tab = $(this);
        var currentTabName = $tab.attr('id').replace('tab_cont_', '');
        var blockName = $tab.closest('form').attr('name').replace('_form', '');

        var strSearch = window.location.search;
        // var re = /(obAdminTabControl_active_tab=)([^\b]+)/; // old style, doesn't work
        var re = new RegExp('(' + blockName + '_active_tab=)([^\\b]+)', 'gim');
        if (re.test(strSearch)) {
            strSearch = strSearch.replace(re, '$1' + currentTabName);
        } else {
            strSearch += strSearch ? '&' : '?';
            strSearch += blockName + '_active_tab=' + currentTabName;
        }
        history.pushState(null, '', strSearch);
    });
});

