<?

use Rapid\Dev\Helper\Module\Options;
use Rapid\Dev\Helper\MyDev;
use Rapid\Dev\Helper\SitePage;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
global $APPLICATION;
global $USER;
if (!$USER->IsAdmin()) {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}
IncludeModuleLangFile(__FILE__);
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/options.php");

$arOptions = array(
    'recaptcha' => array(
        "TAB_NAME" => "Настройки g-recaptcha",
        "TAB_TITLE" => "",
        "TAB_ICON" => "",
        'FIELDS' => array(
            array(
                "CODE" => 'public',
                'NAME' => 'Публичный ключ',
                'TYPE' => 'TEXT',
                'SAVE' => 'Y',
            ),
            array(
                "CODE" => 'secret',
                'NAME' => 'Секретный ключ',
                'TYPE' => 'TEXT',
                'SAVE' => 'Y',
            ),
        )
    )
);

$obOptions = new Options($arOptions);
$obAdminTabControl = $obOptions->getAdminTabControl();
$obOptions->saveOptions();

$obOptions->showForm('rapid-recaptcha-options', 'rapid-options-wrapper', array('apply'));
