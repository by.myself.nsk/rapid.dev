<?

use Rapid\Dev\Helper\Module\Options;
use Rapid\Dev\Helper\MyDev;
use Rapid\Dev\Helper\SitePage;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
global $APPLICATION;
global $USER;
if (!$USER->IsAdmin()) {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}
IncludeModuleLangFile(__FILE__);
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/options.php");

CJSCore::Init(array('jquery'));
CModule::IncludeModule('iblock');

$arOptions = array(
    'main' => array(
        "TAB_NAME" => "Переустановка модулей",
        "TAB_TITLE" => "",
        "TAB_ICON" => "",
        'FIELDS' => array(
            array(
                "CODE" => 'partner',
                'NAME' => 'Отображать только сторонние модули',
                'TYPE' => 'CHECKBOX',
                'DEFAULT' => 'Y',
                'REFRESH' => 'Y'
            ),
            array(
                "CODE" => 'module',
                'TYPE' => 'LIST',
                'REFRESH' => 'Y',
                'VALUES' => array(),
            ),
            array(
                "CODE" => 'reinstall_button',
                "VALUE" => "Переустановить",
                'TYPE' => 'BUTTON',
            ),
        )
    )
);
//----------------------------------------------------------------------------------------------------------------------
$obOptions = new Options($arOptions);
// задание массива опций
$dbInstalledModules = CModule::GetList();
$arModules = array();
while ($arItem = $dbInstalledModules->Fetch()) {
    if ($obOptions->getOptionValue('main', 'partner') == 'Y' && strpos($arItem['ID'], '.') === false) {
        continue;
    }
    $arModules[$arItem['ID']] = $arItem['ID'];
}
// сортировка: rapid в начало списка
uasort($arModules, function ($a, $b) {
    $bARapid = strpos($a, 'rapid') === 0;
    $bBRapid = strpos($b, 'rapid') === 0;
    if ($bARapid xor $bBRapid) { // только 1 - наш
        return $bBRapid;
    } else {
        return $a > $b;
    }
});
$obOptions->setOptionParamValue('main', 'module', 'VALUES', $arModules, true);
//----------------------------------------------------------------------------------------------------------------------
// обработка событий по ajax
ob_start();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    switch ($_POST['action']) {
        case 'reinstall':
            if (MyDev::reInstallModule($_REQUEST['main_module'])) {
                CAdminMessage::ShowNote('Модуль ' . $_REQUEST['module'] . ' переустановлен');
            } else {
                CAdminMessage::ShowMessage('Модуль ' . $_REQUEST['module'] . ' не переустановлен');
            }
            break;
    }
}
$strProcessingData = ob_get_clean();
//----------------------------------------------------------------------------------------------------------------------
// вывод страницы
$APPLICATION->SetTitle("Переустановка модулей");

$bAjax = SitePage::isAjax(array('refresh', 'reinstall'));

?>
    <div id="reinstall-wrapper"><?
        if ($bAjax) {
            $APPLICATION->RestartBuffer();
            ob_start();
        }
        $obOptions->getOptionsTree();
        echo $strProcessingData;
        $obOptions->showForm('reinstall', 'reinstall-wrapper');
        if ($bAjax) {
            $strLocation = SitePage::GetCurPageParam($_POST, 0, array('sessid', 'autosave_id', 'action'));
            echo json_encode(array('html' => ob_get_clean(), 'location' => $strLocation));
            die();
        }
        ?></div>
    <script>
        $(function () {
            var $wrapper = $('#reinstall-wrapper');
            $wrapper.on('click', '#main_reinstall_button', function () {
                makeRequest('reinstall');
            });
        });
    </script><?
