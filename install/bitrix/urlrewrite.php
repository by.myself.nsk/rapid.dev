<?
// тут SITE_DIR не определен, т.к. пролог, видимо, еще не выполнялся
if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/urlrewrite.php')) {
    include_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/urlrewrite.php');
} elseif (file_exists($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php')) {
    include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');
}
// тут SITE_DIR будет определен, только если пролог выполнился где-то внутри urlrewrite - если сработал ЧПУ
if (!defined(SITE_DIR)) {
    require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
}
$siteDir = defined(SITE_DIR) ? SITE_DIR : '/';
$path = $_SERVER['DOCUMENT_ROOT'] . $siteDir . '404.php';

if (file_exists($path)) {
    include_once($path);
}
