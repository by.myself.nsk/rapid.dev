<?

use Rapid\Dev\Helper\Module\Module;
use Rapid\Dev\Helper\MyString;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
global $APPLICATION;
global $USER;
//if(!$USER->IsAdmin()) {
//    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
//}
if (!CModule::IncludeModule('rapid.dev')) {
    return;
}
MyString::textProcessing($_REQUEST);
$strModuleId = &$_REQUEST['mid'];
$strPage = &$_REQUEST['page'];
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/options.php");
CJSCore::Init(array('jquery'));
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
try {
    Module::routeAdminPage($_REQUEST['mid'], $_REQUEST['page']);
} catch (Exception $e) {
    echo $e->getMessage();
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
