<?php
/** BitrixVars
 * @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDatabase $DB
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $templateFile
 * @var string $templateFolder
 * @var string $componentPath
 * @var CUserLocation $component
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
$this->setFrameMode(true);
if(empty($arResult['CITIES']))
    return;
reset($arResult['CITIES']);
$strCurCity = $arResult['SELECTED_CITY'] ?: key($arResult['CITIES']);
?><div class="header__city"><?
   ?><span class="header__city-wrapper"><?=$strCurCity?></span>
    <nav class="header__city-dropdown"><?
        foreach($arResult['CITIES'] as &$arCity)
        {
//            $bSelected = $arCity['SELECTED'];
            $bSelected = $arCity['NAME'] == $strCurCity;
            ?><a href="#" class="header__city-link<?=$bSelected ? ' header__city-link_current' : ''?>"><?=$arCity['NAME']?></a><?
        }
        unset($arCity);
    ?></nav>
</div><?
