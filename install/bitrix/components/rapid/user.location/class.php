<?php
/**@global CMain $APPLICATION */

use Rapid\Dev\Component\Component;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
global $APPLICATION;

// подумать над возможностью транслитерации данных в результирующем массиве
// как вариант - параметр компонента "транслитерировать"
// тогда писать 2 ключа - "KEY" и "~KEY"

class CUserLocation extends Component
{
    public function onPrepareComponentParams($arParams)
    {
        $this->setDefaultComponentParams($arParams);
        $this->prepareParam($arParams['IBLOCK_TYPE'], 'string');
        $this->prepareParam($arParams['IBLOCK_ID'], 'int');
        $this->prepareParam($arParams['SOURCE_TYPE'], 'string');
        $this->prepareParam($arParams['FILTER_NAME'], 'string');
        $this->prepareParam($arParams['SORT_BY1'], 'string');
        $this->prepareParam($arParams['SORT_ORDER1'], 'string');
        $this->prepareParam($arParams['SORT_BY2'], 'string');
        $this->prepareParam($arParams['SORT_ORDER2'], 'string');
        $this->prepareParam($arParams['CACHE_GEO_DATA'], 'bool');
        $this->prepareParam($arParams['COOKIE_PARAM_NAME'], 'string', 'city');
        $this->prepareParam($arParams['COOKIE_PARAM_EXPIRES'], 'int', 30);
        $strComponentName = str_replace('.', '_', basename(dirname(__FILE__)));
        $arParams['COOKIE_PARAM_NAME'] = $strComponentName.'-'.$arParams['COOKIE_PARAM_NAME'];
        return $arParams;
    }

    public function getCities($arrFilter = array())
    {
        $arParams = &$this->arParams;
        $arResult = &$this->arResult;
        $arOrder = array(
            $arParams["SORT_BY1"] => $arParams["SORT_ORDER1"],
            $arParams["SORT_BY2"] => $arParams["SORT_ORDER2"],
        );
        $arReturn = array();
        if ($arParams['SOURCE_TYPE'] == 'ELEMENTS') {
            $dbItems = CIblockElement::GetList(
                $arOrder,
                $arFilter = array_merge(
                    array(
                        'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                        'ACTIVE' => 'Y',
                    ),
                    $arrFilter
                ),
                false,
                false,
                array(
                    'IBLOCK_ID',
                    'ID',
                    'NAME',
                )
            );
            while ($arItem = $dbItems->Fetch()) {
                $arReturn[$arItem['NAME']] = $arItem;
            }
        } else // 'SECTIONS'
        {
            $arFilter = array_merge(
                array(
                    'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                    'ACTIVE' => 'Y',
                    'DEPTH_LEVEL' => 1,
                ),
                $arrFilter
            );
            $dbSections = CIblockSection::GetList(
                $arOrder,
                $arFilter,
                false,
                array(
                    'IBLOCK_ID',
                    'ID',
                    'NAME',
                )
            );
            while ($arSection = $dbSections->Fetch()) {
                $arReturn[$arSection['NAME']] = $arSection;
            }
        }
        return $arReturn;
    }

    public function executeComponent()
    {
        $arParams = &$this->arParams;
        $arResult = &$this->arResult;
        $strCookieParam = &$arParams['COOKIE_PARAM_NAME'];
        if (strlen($arParams["FILTER_NAME"]) <= 0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"])) {
            $arrFilter = array();
        } else {
            $arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
            if (!is_array($arrFilter)) {
                $arrFilter = array();
            }
        }
        // todo определять charset
        $obGeo = new MyGeo(array('charset' => 'UTF-8'));
        $arResult['IP'] = $obGeo->get_ip();
        if ($this->StartResultCache(false, array($arrFilter, $_COOKIE[$strCookieParam], $arResult['IP']))) {
            $arResult['CITIES'] = $this->getCities($arrFilter);

            if (!empty($_COOKIE[$strCookieParam])) {
                // если промахнулись, то стереть плюшку, и следующий код отработает
                if (!array_key_exists($_COOKIE[$strCookieParam], $arResult['CITIES'])) {
                    unset($_COOKIE[$strCookieParam]);
                } else {
                    $arResult['FROM_COOKIE'] = true;
                    $arSelectedCity = &$arResult['CITIES'][$_COOKIE[$strCookieParam]];
                    $arSelectedCity['SELECTED'] = true;
                    $arResult['SELECTED_CITY'] = $_COOKIE[$strCookieParam];
                }
            }
            if (empty($_COOKIE[$strCookieParam])) {
                $arResult['FROM_COOKIE'] = false;
                $arResult['GEO'] = $obGeo->get_value('', $arParams['CACHE_GEO_DATA']);
                if (array_key_exists($arResult['GEO']['city'], $arResult['CITIES'])) {
                    $arResult['SELECTED_CITY'] = $arResult['GEO']['city'];
                    $arResult['CITIES'][$arResult['GEO']['city']]['SELECTED'] = true;
                } else {
                    $arResult['SELECTED_CITY'] = false;
                }
            }
            $arResult['SELECTED_CITY_ID'] = $arResult['CITIES'][$arResult['SELECTED_CITY']]['ID'];
            $arResult['SELECTED_CITY_ID'] = intval($arResult['SELECTED_CITY_ID']) ?: false;
            array_walk($arResult['CITIES'], function (&$arItem) {
                $arItem['SELECTED'] = $arItem['SELECTED'] ?: false;
            });
            $this->includeComponentTemplate();
        } else {
            $this->abortResultCache();
        }
        ?>
        <script>
            self.userLocationCookieParam = <?=json_encode($arParams['COOKIE_PARAM_NAME'])?>;
            self.userLocationCookieExpires = <?=json_encode($arParams['COOKIE_PARAM_EXPIRES'])?>;
        </script><?
        return $arResult;
    }
}
