<?php
/**@global CMain $APPLICATION */

use Rapid\Dev\Component\Component;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
global $APPLICATION;

$arIblockTypes = Component::getParametersIblockTypes();
$arIblocks = Component::getParametersIblocks($arCurrentValues["IBLOCK_TYPE"], $_REQUEST['site']);
$arSorts = MyComponentParams::sortDirections();
$arSortFields = MyComponentParams::sortFields();

$arComponentParameters = array(
    "GROUPS" => array(
        "DATA_SOURCE" => array(
            "NAME" => "Источник данных",
            "SORT" => "100",
        ),
        "VIEW" => array(
            "NAME" => "Параметры отображения",
            "SORT" => "200",
        ),
        "CACHE" => array(
            "NAME" => "Параметры кеширования",
            "SORT" => "500",
        ),
    ),
    "PARAMETERS" => array(
        "IBLOCK_TYPE" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => "Тип инфоблока",
            "TYPE" => "LIST",
            "VALUES" => $arIblockTypes,
            "DEFAULT" => "news",
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => "Инфоблок",
            "TYPE" => "LIST",
            "VALUES" => $arIblocks,
            "DEFAULT" => '={$_REQUEST["ID"]}',
            "REFRESH" => "Y",
        ),
        'SOURCE_TYPE' => array(
            "PARENT" => "DATA_SOURCE",
            'NAME' => 'Тип данных',
            'TYPE' => 'LIST',
            'VALUES' => array(
                'ELEMENTS' => 'Элементы',
                'SECTIONS' => 'Разделы',
            ),
            'DEFAULT' => 'ELEMENTS',
        ),
        "FILTER_NAME" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => "Фильтр",
            "DEFAULT" => "",
        ),
        "SORT_BY1" => array(
            "PARENT" => "VIEW",
            "NAME" => 'Поле сортировки 1',
            "TYPE" => "LIST",
            "DEFAULT" => "ACTIVE_FROM",
            "VALUES" => $arSortFields,
            "ADDITIONAL_VALUES" => "Y",
        ),
        "SORT_ORDER1" => array(
            "PARENT" => "VIEW",
            "NAME" => 'Направление сортировки 1',
            "TYPE" => "LIST",
            "DEFAULT" => "DESC",
            "VALUES" => $arSorts,
            "ADDITIONAL_VALUES" => "Y",
        ),
        "SORT_BY2" => array(
            "PARENT" => "VIEW",
            "NAME" => 'Поле сортировки 2',
            "TYPE" => "LIST",
            "DEFAULT" => "SORT",
            "VALUES" => $arSortFields,
            "ADDITIONAL_VALUES" => "Y",
        ),
        "SORT_ORDER2" => array(
            "PARENT" => "VIEW",
            "NAME" => 'Направление сортировки 2',
            "TYPE" => "LIST",
            "DEFAULT" => "ASC",
            "VALUES" => $arSorts,
            "ADDITIONAL_VALUES" => "Y",
        ),
        "CACHE_TIME" => Array(
            "PARENT" => "CACHE",
            "DEFAULT" => 36000000,
        ),
        'CACHE_GEO_DATA' => array(
            "PARENT" => "CACHE",
            'NAME' => 'Кешировать геоданные',
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'Y'
        ),
        'COOKIE_PARAM_NAME' => array(
            "PARENT" => "CACHE",
            'NAME' => 'Имя ключа в массиве COOKIE, по которому будет сохранен выбор пользователя',
            'DEFAULT' => 'city'
        ),
        'COOKIE_PARAM_EXPIRES' => array(
            "PARENT" => "CACHE",
            'NAME' => 'Срок хранения cookie',
            'DEFAULT' => '30'
        )
    )
);
