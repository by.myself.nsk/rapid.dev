<?php
/**@global CMain $APPLICATION */

use Rapid\Dev\Component\Component;
use Rapid\Dev\Helper\MyString;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
global $APPLICATION;

class CIncludeArea extends Component
{
    public function onPrepareComponentParams($arParams)
    {
        $this->setDefaultComponentParams($arParams);
        $this->prepareParam($arParams['FILE'], 'string');
        $this->prepareParam($arParams['AREA_TYPE'], 'string');
        $this->prepareParam($arParams['SHOW_BORDER'], 'bool');
        return $arParams;
    }

    public function executeComponent()
    {
        global $APPLICATION;
        $arParams = &$this->arParams;
        $arResult = &$this->arResult;

        if (substr($arParams['FILE'], 0, 1) != "/") {
            $path = getLocalPath("templates/" . SITE_TEMPLATE_ID . "/" . $arParams['FILE'], BX_PERSONAL_ROOT);
            if ($path === false) {
                $path = getLocalPath("templates/.default/" . $arParams['FILE'], BX_PERSONAL_ROOT);
                if ($path === false) {
                    $path = BX_PERSONAL_ROOT . "/templates/" . SITE_TEMPLATE_ID . "/" . $arParams['FILE'];
                    $module_id = substr($arParams['FILE'], 0, strpos($arParams['FILE'], "/"));
                    if (strlen($module_id) > 0) {
                        $path = "/bitrix/modules/" . $module_id . "/install/templates/" . $arParams['FILE'];
                        if (!file_exists($_SERVER["DOCUMENT_ROOT"] . $path)) {
                            $path = BX_PERSONAL_ROOT . "/templates/" . SITE_TEMPLATE_ID . "/" . $arParams['FILE'];
                        }
                    }
                }
            }
        } else {
            $path = $arParams['FILE'];
        }
        $arResult['REAL_PATH'] = $_SERVER['DOCUMENT_ROOT'] . $path;
        if (!$arResult['REAL_PATH']) {
            return false;
        }
        $arResult['RELATIVE_PATH'] = str_replace($_SERVER['DOCUMENT_ROOT'], '', $arResult['REAL_PATH']);
        $arResult['FILE_CONTENT'] = false;
        if (file_exists($arResult['REAL_PATH'])) {
            $arResult['FILE_CONTENT'] = file_get_contents($arResult['REAL_PATH']);
        }
        ob_start();
        $APPLICATION->IncludeFile(
            $arResult['RELATIVE_PATH'],
            array(),
            array(
                'SHOW_BORDER' => $arParams['SHOW_BORDER'],
                'NAME' => '',
                'MODE' => 'text',
            )
        );
        $arResult['AREA_CONTENT'] = ob_get_clean();
        if ($arResult['FILE_CONTENT']) {
            switch ($arParams['AREA_TYPE']) {
                case 'EMAIL':
                    $arResult['FILE_CONTENT'] = 'mailto:' . trim($arResult['FILE_CONTENT']);
                    break;
                case 'PHONE':
                    $arResult['FILE_CONTENT'] = 'tel:' . trim(MyString::phoneLinkFormat($arResult['FILE_CONTENT']));
                    break;
            }
        }
        $this->includeComponentTemplate();
        return $arResult;
    }
}
