<?php
/**@global CMain $APPLICATION */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
global $APPLICATION;

$arComponentParameters = array(
    "GROUPS" => array(
        "DATA_SOURCE" => array(
            "NAME" => "Источник данных",
            "SORT" => "100",
        ),
        "VIEW" => array(
            "NAME" => "Параметры отображения",
            "SORT" => "200",
        ),
        "CACHE" => array(
            "NAME" => "Параметры кеширования",
            "SORT" => "500",
        ),
    ),
    "PARAMETERS" => array(
        "FILE" => Array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => "Файл включаемой области"
        ),
        "AREA_TYPE" => Array(
            "PARENT" => "VIEW",
            "NAME" => "Тип данных включаемой области",
            "TYPE" => "LIST",
            "VALUES" => array(
                'TEXT' => 'Текст',
                'EMAIL' => 'E-mail',
                'PHONE' => 'Телефон',
            ),
            "DEFAULT" => "TEXT",
        ),
        "SHOW_BORDER" => Array(
            "PARENT" => "VIEW",
            "NAME" => "Отображать рамку редактирования",
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        ),
        "CACHE_TIME" => Array(
            "PARENT" => "CACHE",
            "DEFAULT" => 36000000,
        ),
    )
);
