<?php
/**@global CMain $APPLICATION */

use Rapid\Dev\Component\Component;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $APPLICATION;

class RapidFilterSearch extends Component
{
    public function onPrepareComponentParams($arParams)
    {
        $this->prepareParam($arParams['IBLOCK_ID'], 'int');
        return $arParams;
    }

    public function executeComponent()
    {
        global $APPLICATION;
        $arParams = &$this->arParams;
        $arResult = &$this->arResult;

        if (!CModule::IncludeModule('search')) {
            ShowError('Модуль поиска не установлен');
            return;
        }

        $arResult['IBLOCK_ID'] = $arParams['IBLOCK_ID'];
        if (!CIBlock::GetByID($arResult['IBLOCK_ID'])->SelectedRowsCount()) {
            ShowError('Инфоблок не найден');
            return;
        }

        $arResult['SEARCH_STRING'] = htmlspecialcharsex($arParams['SEARCH_STRING']);
        $arResult['IS_APPLIED'] = strlen($arResult['SEARCH_STRING']) > 2;

        $arResult['RETURN'] = [
            'IS_APPLIED' => $arResult['IS_APPLIED'],
            'FOUND_IDS' => [],
        ];

        if ($arResult['IS_APPLIED']) {
            $arSearchFilter = array(
                'QUERY' => $arResult['SEARCH_STRING'],
                'SITE_ID' => SITE_ID,
                'MODULE_ID' => 'iblock',
                'PARAM2' => [$arResult['IBLOCK_ID']],
            );
            $arSearchSort = array(
                'CUSTOM_RANK'=>'DESC',
                'TITLE_RANK'=>'DESC',
                'RANK'=>'DESC'
            );
            $arSearchExtFilter = array('STEMMING' => true);
            $obSearch = new CSearch();
            $obSearch->Search($arSearchFilter, $arSearchSort, $arSearchExtFilter);

            $arItems = [];

            try {
                if ($obSearch->errorno != 0) {
                    throw new Exception();
                }
                if (!$obSearch->SelectedRowsCount() && $obSearch->Query->bStemming) {
                    $arSearchExtFilter['STEMMING'] = false;
                    $obSearch = new CSearch();
                    $obSearch->Search($arSearchFilter, $arSearchSort, $arSearchExtFilter);
                    if ($obSearch->errorno != 0) {
                        throw new Exception();
                    }
                }
                while ($arSearch = $obSearch->GetNext()) {
                    $arItems[] = $arSearch['ITEM_ID'];
                }
            } catch (Exception $e) {

            }

            $arResult['RETURN']['FOUND_IDS'] = $arItems ?: false;
        }

        if ($this->StartResultCache(false, array())) {

            $this->includeComponentTemplate();
        } else {
            $this->abortResultCache();
        }

        return $arResult['RETURN'];
    }
}
