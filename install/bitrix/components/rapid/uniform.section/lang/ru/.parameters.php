<?
global $MESS;
if(!is_array($MESS))
    $MESS = array();
$MESS = array_merge($MESS, array(
    'RPDUFS_PARAM_ELEMENT_NAME_TEMPLATE' => 'Шаблон имени нового раздела инфоблока',
    'RPDUFS_PARAM_ELEMENT_NAME_TEMPLATE_DEF' => 'Запрос от #DATE#',
    'RPDUFS_PARAM_IS_ELEMENT_ACTIVE' => 'Элемент инфоблока активен',
    'RPDUFS_PARAM_ID_PREFIX' => 'Префикс идентификаторов элементов формы',
    'RPDUFS_PARAM_SEND_MAIL' => 'Отправлять сообщение после отправки формы',
    'RPDUFS_PARAM_SEND_IMMEDIATE' => 'Отправлять мгновенно без регистрации события',
    'RPDUFS_PARAM_DUPLICATE_MAIL' => 'Дублировать письмо на адреса, указанные в настройках главного модуля',
    'RPDUFS_PARAM_ADD_MAIL_TYPE' => 'Тип почтового события при добавлении элемента',
    'RPDUFS_PARAM_EDIT_MAIL_TYPE' => 'Тип почтового события при редактировании элемента',
    'RPDUFS_PARAM_IBLOCK_TYPE' => 'Тип информационного блока (используется только для проверки)',
    'RPDUFS_PARAM_IBLOCK_ID' => 'Код информационного блока',
    'RPDUFS_PARAM_IBLOCK_SECTION_ID' => 'Раздел информационного блока',
    'RPDUFS_PARAM_ENABLE_EDIT' => 'Разрешить редактирование элемента',
    'RPDUFS_PARAM_SECTION_ID' => 'ID редактируемого элемента',
    'RPDUFS_PARAM_SECTION_CODE' => 'Символьный код редактируемого элемента',
    'RPDUFS_PARAM_IBLOCK_FIELD' => 'Поля',
    'RPDUFS_PARAM_IBLOCK_PROPERTY' => 'Свойства',
    'RPDUFS_PARAM_BLOCK_HEADER' => 'Заголовок блока',
    'RPDUFS_PARAM_SHOW_DESCRIPTION_BLOCK' => 'Показывать текстовый блок',
    'RPDUFS_PARAM_HIDDEN_INPUTS' => 'Скрывать поля ввода',
    'RPDUFS_PARAM_MAX_FILE_SIZE' => 'Максимальный размер загружаемых файлов, байт (0 - не ограничивать)',
    'RPDUFS_PARAM_USE_CAPTCHA' => 'Использовать CAPTCHA',
    'RPDUFS_PARAM_MSG_SUBMIT' => 'Текст кнопки отправки',
    'RPDUFS_PARAM_MSG_SUBMIT_DEF' => 'Отправить',
    'RPDUFS_PARAM_EMPTY_MAIL_FIELD' => 'Текст по умолчанию для замены пустых полей почтового сообщения',
    'RPDUFS_PARAM_EMPTY_MAIL_FIELD_DEF' => 'Не указано',
));
?>
