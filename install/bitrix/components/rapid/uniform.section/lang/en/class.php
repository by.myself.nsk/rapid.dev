<?
$MESS["RPDUFS_MSG_IBLOCK_MODULE_NOT_INSTALLED"] = "Модуль Информационных блоков не установлен";
$MESS["RPDUFS_MSG_IBLOCK_ERROR_FILE_TOO_LARGE"] = "Размер загруженного файла превышает допустимое значение";
$MESS["RPDUFS_MSG_FILL_IN"] = "Заполните поле";
$MESS["RPDUFS_MSG_NOT_EMAIL"] = "Введите корректный email";
$MESS["RPDUFS_MSG_SELECT_FILE"] = "Укажите файл";
$MESS["RPDUFS_MSG_WRONG_CAPTCHA"] = "Неверно введено слово с картинки";
$MESS["RPDUFS_MSG_ELEMENT_ADDED"] = "Запрос успешно отправлен";
?>
