<?
$MESS["RPDUFS_PARAM_ELEMENT_NAME_TEMPLATE"] = "Шаблон имени нового элемента инфоблока";
$MESS["RPDUFS_PARAM_ELEMENT_NAME_TEMPLATE_DEF"] = "Запрос от #DATE#";
$MESS["RPDUFS_PARAM_IS_ELEMENT_ACTIVE"] = "Элемент инфоблока активен";
$MESS["RPDUFS_PARAM_ID_PREFIX"] = "Префикс идентификаторов элементов формы";
$MESS["RPDUFS_PARAM_SEND_MAIL"] = "Отправлять сообщение после отправки формы";
$MESS["RPDUFS_PARAM_SEND_IMMEDIATE"] = "Отправлять мгновенно без регистрации события";
$MESS["RPDUFS_PARAM_DUPLICATE_MAIL"] = "Дублировать письмо на адреса, указанные в настройках главного модуля";
$MESS["RPDUFS_PARAM_ADD_MAIL_TYPE"] = "Тип почтового события при добавлении элемента";
$MESS["RPDUFS_PARAM_EDIT_MAIL_TYPE"] = "Тип почтового события при редактировании элемента";
$MESS["RPDUFS_PARAM_IBLOCK_FIELD"] = "Поля";
$MESS["RPDUFS_PARAM_IBLOCK_TYPE"] = "Тип информационного блока (используется только для проверки)";
$MESS["RPDUFS_PARAM_IBLOCK_ID"] = "Код информационного блока";
$MESS["RPDUFS_PARAM_ENABLE_EDIT"] = "Разрешить редактирование элемента";
$MESS["RPDUFS_PARAM_SECTION_ID"] = "ID редактируемого элемента";
$MESS["RPDUFS_PARAM_SECTION_CODE"] = "Символьный код редактируемого элемента";
$MESS["RPDUFS_PARAM_IBLOCK_PROPERTY"] = "Поля";
$MESS["RPDUFS_PARAM_IBLOCK_PROPERTY"] = "Свойства";
$MESS["RPDUFS_PARAM_BLOCK_HEADER"] = "Заголовок блока";
$MESS["RPDUFS_PARAM_SHOW_DESCRIPTION_BLOCK"] = "Показывать текстовый блок";
$MESS["RPDUFS_PARAM_HIDDEN_INPUTS"] = "Скрывать поля ввода";
$MESS["RPDUFS_PARAM_MAX_FILE_SIZE"] = "Максимальный размер загружаемых файлов, байт (0 - не ограничивать)";
$MESS["RPDUFS_PARAM_USE_CAPTCHA"] = "Использовать CAPTCHA";
$MESS["RPDUFS_PARAM_MSG_SUBMIT"] = "Текст кнопки отправки";
$MESS["RPDUFS_PARAM_MSG_SUBMIT_DEF"] = "Отправить";
$MESS["RPDUFS_PARAM_EMPTY_MAIL_FIELD"] = "Текст по умолчанию для замены пустых полей почтового сообщения";
$MESS["RPDUFS_PARAM_EMPTY_MAIL_FIELD_DEF"] = "Не указано";
?>
