<?php
/**@global CMain $APPLICATION */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
$arComponentDescription = array(
    "NAME" => "uniform.section",
    "DESCRIPTION" => GetMessage('RPDUFS_DESC_NAME'),
    "PATH" => array(
        "ID" => "rapiddev",
        "NAME" => 'Rapid.dev',
    ),
);
?>
