<?php
/**@global CMain $APPLICATION */

use Rapid\Dev\Component\Component;

/** @var array $arCurrentValues */
global $APPLICATION;
global $USER_FIELD_MANAGER;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!CModule::IncludeModule("iblock")) {
    return;
}

$arComponentParameters = array(
    "GROUPS" => array(
        "DATA_SOURCE" => array(
            "NAME" => 'Источник данных',
            "SORT" => 200
        ),
        "VIEW" => array(
            "NAME" => 'Параметры отображения формы',
            "SORT" => 300
        ),
        "SAVE" => array(
            "NAME" => 'Параметры заявки',
            "SORT" => 400
        ),
        "MAIL" => array(
            "NAME" => 'Параметры отправки почты',
            "SORT" => 500
        ),
        "CACHE" => array(
            "NAME" => "Настройки кеширования",
            "SORT" => 900
        )
    )
);
$arIblockTypes = Component::getParametersIblockTypes();
$arIBlocks = Component::getParametersIblocks($arCurrentValues["IBLOCK_TYPE"], $_REQUEST["site"]);
$arComponentParameters["PARAMETERS"] = array(
    "IBLOCK_TYPE" => array(
        "PARENT" => "DATA_SOURCE",
        "NAME" => GetMessage("RPDUFS_PARAM_IBLOCK_TYPE"),
        "TYPE" => "LIST",
        "VALUES" => $arIblockTypes,
        "DEFAULT" => "forms",
        "REFRESH" => "Y",
    ),
    "IBLOCK_ID" => array(
        "PARENT" => "DATA_SOURCE",
        "NAME" => GetMessage("RPDUFS_PARAM_IBLOCK_ID"),
        "TYPE" => "LIST",
        "VALUES" => $arIBlocks,
        "DEFAULT" => '',
        "REFRESH" => "Y",
    ),
    "IBLOCK_SECTION_ID" => array(
        "PARENT" => "DATA_SOURCE",
        "NAME" => GetMessage("RPDUFS_PARAM_IBLOCK_SECTION_ID"),
        "DEFAULT" => '',
    ),
    "CACHE_TIME" => Array(
        "DEFAULT" => 3600,
    ),
);
if(!empty($arCurrentValues["IBLOCK_ID"]))
{
    // поля разделов
    $arProperty_UF = array();
    $arUserFields = $USER_FIELD_MANAGER->GetUserFields("IBLOCK_".$arCurrentValues["IBLOCK_ID"]."_SECTION", 0, LANGUAGE_ID);
    foreach($arUserFields as $FIELD_NAME=>$arUserField)
    {
        $arUserField['LIST_COLUMN_LABEL'] = (string)$arUserField['LIST_COLUMN_LABEL'];
        $arProperty_UF[$FIELD_NAME] = $arUserField['LIST_COLUMN_LABEL'] ? '['.$FIELD_NAME.'] '.$arUserField['LIST_COLUMN_LABEL'] : $FIELD_NAME;
    }

    $arInputVisibility = array();
    $arIblockFields = Component::getParametersIblockSectionFields($arCurrentValues["IBLOCK_ID"]);
    foreach($arIblockFields as $strKey => $strValue)
    {
        $arInputVisibility['FIELD_'.$strKey] = $strValue;
    }
    $arComponentParameters['PARAMETERS'] = array_merge($arComponentParameters['PARAMETERS'], array(
        "ENABLE_EDIT" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage('RPDUFS_PARAM_ENABLE_EDIT'),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
            "REFRESH" => "Y",
        ),
    ));
    if($arCurrentValues['ENABLE_EDIT'] == 'Y')
    {
        $arComponentParameters['PARAMETERS'] = array_merge($arComponentParameters['PARAMETERS'], array(
            "ITEM_ID" => array(
                "PARENT" => "DATA_SOURCE",
                "NAME" => GetMessage('RPDUFS_PARAM_SECTION_ID'),
                "TYPE" => "STRING",
                "DEFAULT" => '={$_REQUEST["SECTION_ID"]}',
            ),
            "ITEM_CODE" => array(
                "PARENT" => "DATA_SOURCE",
                "NAME" => GetMessage('RPDUFS_PARAM_SECTION_CODE'),
                "TYPE" => "STRING",
                "DEFAULT" => '',
            ),
        ));
    }
    $arComponentParameters['PARAMETERS'] = array_merge($arComponentParameters['PARAMETERS'], array(
        "FIELD_CODE" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("RPDUFS_PARAM_IBLOCK_FIELD"),
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "VALUES" => $arIblockFields,
        ),
        "PROPERTY_CODE" =>array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("RPDUFS_PARAM_IBLOCK_PROPERTY"),
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => $arProperty_UF,
        ),
        "HIDDEN" => array(
            "PARENT" => "VIEW",
            "NAME" => GetMessage('RPDUFS_PARAM_HIDDEN_INPUTS'),
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "VALUES" => $arInputVisibility,
            "ADDITIONAL_VALUES" => "Y",
        ),
    ));
}
$arComponentParameters['PARAMETERS'] = array_merge($arComponentParameters['PARAMETERS'], array(
    "USE_CAPTCHA" => array(
        "PARENT" => "VIEW",
        "NAME" => GetMessage("RPDUFS_PARAM_USE_CAPTCHA"),
        "TYPE" => "CHECKBOX",
    ),
    'ELEMENT_NAME_TEMPLATE' => array(
        "PARENT" => "SAVE",
        "NAME" => GetMessage('RPDUFS_PARAM_ELEMENT_NAME_TEMPLATE'),
        "TYPE" => "STRING",
        "DEFAULT" => GetMessage('RPDUFS_PARAM_ELEMENT_NAME_TEMPLATE_DEF')
    ),
    "IS_ACTIVE" => array(
        "PARENT" => "SAVE",
        "NAME" => GetMessage('RPDUFS_PARAM_IS_ELEMENT_ACTIVE'),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "N",
    ),
    "MAX_FILE_SIZE" => array(
        "PARENT" => "SAVE",
        "NAME" => GetMessage("RPDUFS_PARAM_MAX_FILE_SIZE"),
        "TYPE" => "TEXT",
        "DEFAULT" => "0",
    ),
    "SEND_MAIL" => array(
        "PARENT" => "MAIL",
        "NAME" => GetMessage('RPDUFS_PARAM_SEND_MAIL'),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "N",
        "REFRESH" => "Y"
    ),
//    "SET_TITLE" => array(),// автоматически "PARENT" => "ADDITIONAL_SETTINGS",
));
if($arCurrentValues['SEND_MAIL'] == 'Y')
{
    $arEventTypes = Component::getParametersMailTypes();
    $arComponentParameters['PARAMETERS'] = array_merge($arComponentParameters['PARAMETERS'], array(
        "SEND_IMMEDIATE" => array(
            "PARENT" => "MAIL",
            "NAME" => GetMessage('RPDUFS_PARAM_SEND_IMMEDIATE'),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),
        "DUPLICATE_MAIL" => array(
            "PARENT" => "MAIL",
            "NAME" => GetMessage('RPDUFS_PARAM_DUPLICATE_MAIL'),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),
        "MSG_EMPTY_MAIL_FIELD" => array(
            "PARENT" => "MAIL",
            "NAME" => GetMessage('RPDUFS_PARAM_EMPTY_MAIL_FIELD'),
            "TYPE" => "STRING",
            "DEFAULT" => GetMessage('RPDUFS_PARAM_EMPTY_MAIL_FIELD_DEF'),
        ),
        'ADD_MAIL_TYPE' => array(
            "NAME" => GetMessage('RPDUFS_PARAM_ADD_MAIL_TYPE'),
            "PARENT" => "MAIL",
            "TYPE" => "LIST",
            "VALUES" => $arEventTypes,
            "DEFAULT" => ""
        ),
    ));
    if($arCurrentValues['ENABLE_EDIT'] == 'Y')
    {
        $arComponentParameters['PARAMETERS'] = array_merge($arComponentParameters['PARAMETERS'], array(
            'EDIT_MAIL_TYPE' => array(
                "NAME" => GetMessage('RPDUFS_PARAM_EDIT_MAIL_TYPE'),
                "PARENT" => "MAIL",
                "TYPE" => "LIST",
                "VALUES" => $arEventTypes,
                "DEFAULT" => ""
            ),
        ));
    }
}
