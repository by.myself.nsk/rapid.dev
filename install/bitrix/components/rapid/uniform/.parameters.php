<?php
/**@global CMain $APPLICATION */

use Rapid\Dev\Component\Component;

/** @var array $arCurrentValues */
global $APPLICATION;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

if(!CModule::IncludeModule("iblock"))
    return;

$arComponentParameters = array(
    "GROUPS" => array(
        "DATA_SOURCE" => array(
            "NAME" => 'Источник данных',
            "SORT" => 200
        ),
        "VIEW" => array(
            "NAME" => 'Параметры отображения формы',
            "SORT" => 300
        ),
        "SAVE" => array(
            "NAME" => 'Параметры заявки',
            "SORT" => 400
        ),
        "MAIL" => array(
            "NAME" => 'Параметры отправки почты',
            "SORT" => 500
        ),
        "CACHE" => array(
            "NAME" => "Настройки кеширования",
            "SORT" => 900
        )
    )
);
$arIblockTypes = Component::getParametersIblockTypes();
$arIBlocks = Component::getParametersIblocks($arCurrentValues["IBLOCK_TYPE"], $_REQUEST["site"]);
$arComponentParameters["PARAMETERS"] = array(
    "IBLOCK_TYPE" => array(
        "PARENT" => "DATA_SOURCE",
        "NAME" => GetMessage("RPDUF_PARAM_IBLOCK_TYPE"),
        "TYPE" => "LIST",
        "VALUES" => $arIblockTypes,
        "DEFAULT" => "forms",
        "REFRESH" => "Y",
    ),
    "IBLOCK_ID" => array(
        "PARENT" => "DATA_SOURCE",
        "NAME" => GetMessage("RPDUF_PARAM_IBLOCK_ID"),
        "TYPE" => "LIST",
        "VALUES" => $arIBlocks,
        "DEFAULT" => '',
        "REFRESH" => "Y",
    ),
    "CACHE_TIME" => Array(
        "DEFAULT" => 3600,
    ),
);
if(!empty($arCurrentValues["IBLOCK_ID"]))
{
    $arIblockFields = Component::getParametersIblockFields($arCurrentValues["IBLOCK_ID"]);
    $arIblockProperties = Component::getParametersIblockProperties($arCurrentValues["IBLOCK_ID"]);//CUniform::getIblockProperties($arCurrentValues["IBLOCK_ID"]);
    $arInputVisibility = array();
    foreach($arIblockFields as $strKey => $strValue)
    {
        $arInputVisibility['FIELD_'.$strKey] = $strValue;
    }
    foreach($arIblockProperties as $strKey => $strValue)
    {
        $arInputVisibility['PROPERTY_'.$strKey] = $strValue;
    }

    $arComponentParameters['PARAMETERS'] = array_merge($arComponentParameters['PARAMETERS'], array(
        "ENABLE_EDIT" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage('RPDUF_PARAM_ENABLE_EDIT'),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
            "REFRESH" => "Y",
        ),
    ));
    if($arCurrentValues['ENABLE_EDIT'] == 'Y')
    {
        $arComponentParameters['PARAMETERS'] = array_merge($arComponentParameters['PARAMETERS'], array(
            "ITEM_ID" => array(
                "PARENT" => "DATA_SOURCE",
                "NAME" => GetMessage('RPDUF_PARAM_ITEM_ID'),
                "TYPE" => "STRING",
                "DEFAULT" => '={$_REQUEST["ITEM_ID"]}',
            ),
            "ITEM_CODE" => array(
                "PARENT" => "DATA_SOURCE",
                "NAME" => GetMessage('RPDUF_PARAM_ITEM_CODE'),
                "TYPE" => "STRING",
                "DEFAULT" => '',
            ),
        ));
    }
    $arComponentParameters['PARAMETERS'] = array_merge($arComponentParameters['PARAMETERS'], array(
        "FIELD_CODE" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("RPDUF_PARAM_IBLOCK_FIELD"),
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "VALUES" => $arIblockFields,
        ),
        $arIblockFields,
        "PROPERTY_CODE" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("RPDUF_PARAM_IBLOCK_PROPERTY"),
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "VALUES" => $arIblockProperties,
            "ADDITIONAL_VALUES" => "Y",
        ),
    ));
}
$arComponentParameters['PARAMETERS'] = array_merge($arComponentParameters['PARAMETERS'], array(
    "USE_CAPTCHA" => array(
        "PARENT" => "VIEW",
        "NAME" => GetMessage("RPDUF_PARAM_USE_CAPTCHA"),
        "TYPE" => "CHECKBOX",
    ),
    'ELEMENT_NAME_TEMPLATE' => array(
        "PARENT" => "SAVE",
        "NAME" => GetMessage('RPDUF_PARAM_ELEMENT_NAME_TEMPLATE'),
        "TYPE" => "STRING",
        "DEFAULT" => GetMessage('RPDUF_PARAM_ELEMENT_NAME_TEMPLATE_DEF')
    ),
    "IS_ACTIVE" => array(
        "PARENT" => "SAVE",
        "NAME" => GetMessage('RPDUF_PARAM_IS_ELEMENT_ACTIVE'),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "N",
    ),
    "MAX_FILE_SIZE" => array(
        "PARENT" => "SAVE",
        "NAME" => GetMessage("RPDUF_PARAM_MAX_FILE_SIZE"),
        "TYPE" => "TEXT",
        "DEFAULT" => "0",
    ),
    "SEND_MAIL" => array(
        "PARENT" => "MAIL",
        "NAME" => GetMessage('RPDUF_PARAM_SEND_MAIL'),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "N",
        "REFRESH" => "Y"
    ),
//    "SET_TITLE" => array(),// автоматически "PARENT" => "ADDITIONAL_SETTINGS",
));
if($arCurrentValues['SEND_MAIL'] == 'Y')
{
    $arEventTypes = Component::getParametersMailTypes();
    $arComponentParameters['PARAMETERS'] = array_merge($arComponentParameters['PARAMETERS'], array(
        "SEND_IMMEDIATE" => array(
            "PARENT" => "MAIL",
            "NAME" => GetMessage('RPDUF_PARAM_SEND_IMMEDIATE'),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),
        "DUPLICATE_MAIL" => array(
            "PARENT" => "MAIL",
            "NAME" => GetMessage('RPDUF_PARAM_DUPLICATE_MAIL'),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),
        "MSG_EMPTY_MAIL_FIELD" => array(
            "PARENT" => "MAIL",
            "NAME" => GetMessage('RPDUF_PARAM_EMPTY_MAIL_FIELD'),
            "TYPE" => "STRING",
            "DEFAULT" => GetMessage('RPDUF_PARAM_EMPTY_MAIL_FIELD_DEF'),
        ),
        'ADD_MAIL_TYPE' => array(
            "NAME" => GetMessage('RPDUF_PARAM_ADD_MAIL_TYPE'),
            "PARENT" => "MAIL",
            "TYPE" => "LIST",
            "VALUES" => $arEventTypes,
            "DEFAULT" => ""
        ),
    ));
    if($arCurrentValues['ENABLE_EDIT'] == 'Y')
    {
        $arComponentParameters['PARAMETERS'] = array_merge($arComponentParameters['PARAMETERS'], array(
            'EDIT_MAIL_TYPE' => array(
                "NAME" => GetMessage('RPDUF_PARAM_EDIT_MAIL_TYPE'),
                "PARENT" => "MAIL",
                "TYPE" => "LIST",
                "VALUES" => $arEventTypes,
                "DEFAULT" => ""
            ),
        ));
    }
}
