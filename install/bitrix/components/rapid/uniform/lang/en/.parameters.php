<?
$MESS["RPDUF_PARAM_ELEMENT_NAME_TEMPLATE"] = "Шаблон имени нового элемента инфоблока";
$MESS["RPDUF_PARAM_ELEMENT_NAME_TEMPLATE_DEF"] = "Запрос от #DATE#";
$MESS["RPDUF_PARAM_IS_ELEMENT_ACTIVE"] = "Элемент инфоблока активен";
$MESS["RPDUF_PARAM_ID_PREFIX"] = "Префикс идентификаторов элементов формы";
$MESS["RPDUF_PARAM_SEND_MAIL"] = "Отправлять сообщение после отправки формы";
$MESS["RPDUF_PARAM_SEND_IMMEDIATE"] = "Отправлять мгновенно без регистрации события";
$MESS["RPDUF_PARAM_DUPLICATE_MAIL"] = "Дублировать письмо на адреса, указанные в настройках главного модуля";
$MESS["RPDUF_PARAM_ADD_MAIL_TYPE"] = "Тип почтового события при добавлении элемента";
$MESS["RPDUF_PARAM_EDIT_MAIL_TYPE"] = "Тип почтового события при редактировании элемента";
$MESS["RPDUF_PARAM_IBLOCK_FIELD"] = "Поля";
$MESS["RPDUF_PARAM_IBLOCK_TYPE"] = "Тип информационного блока (используется только для проверки)";
$MESS["RPDUF_PARAM_IBLOCK_ID"] = "Код информационного блока";
$MESS["RPDUF_PARAM_ENABLE_EDIT"] = "Разрешить редактирование элемента";
$MESS["RPDUF_PARAM_ITEM_ID"] = "ID редактируемого элемента";
$MESS["RPDUF_PARAM_ITEM_CODE"] = "Символьный код редактируемого элемента";
$MESS["RPDUF_PARAM_IBLOCK_PROPERTY"] = "Поля";
$MESS["RPDUF_PARAM_IBLOCK_PROPERTY"] = "Свойства";
$MESS["RPDUF_PARAM_BLOCK_HEADER"] = "Заголовок блока";
$MESS["RPDUF_PARAM_SHOW_DESCRIPTION_BLOCK"] = "Показывать текстовый блок";
$MESS["RPDUF_PARAM_HIDDEN_INPUTS"] = "Скрывать поля ввода";
$MESS["RPDUF_PARAM_MAX_FILE_SIZE"] = "Максимальный размер загружаемых файлов, байт (0 - не ограничивать)";
$MESS["RPDUF_PARAM_USE_CAPTCHA"] = "Использовать CAPTCHA";
$MESS["RPDUF_PARAM_MSG_SUBMIT"] = "Текст кнопки отправки";
$MESS["RPDUF_PARAM_MSG_SUBMIT_DEF"] = "Отправить";
$MESS["RPDUF_PARAM_EMPTY_MAIL_FIELD"] = "Текст по умолчанию для замены пустых полей почтового сообщения";
$MESS["RPDUF_PARAM_EMPTY_MAIL_FIELD_DEF"] = "Не указано";
?>
