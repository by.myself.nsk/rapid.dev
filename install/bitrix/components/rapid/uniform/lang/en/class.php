<?
$MESS["RPDUF_MSG_IBLOCK_MODULE_NOT_INSTALLED"] = "Модуль Информационных блоков не установлен";
$MESS["RPDUF_MSG_IBLOCK_ERROR_FILE_TOO_LARGE"] = "Размер загруженного файла превышает допустимое значение";
$MESS["RPDUF_MSG_FILL_IN"] = "Заполните поле";
$MESS["RPDUF_MSG_NOT_EMAIL"] = "Введите корректный email";
$MESS["RPDUF_MSG_SELECT_FILE"] = "Укажите файл";
$MESS["RPDUF_MSG_WRONG_CAPTCHA"] = "Неверно введено слово с картинки";
$MESS["RPDUF_MSG_ELEMENT_ADDED"] = "Запрос успешно отправлен";
?>
