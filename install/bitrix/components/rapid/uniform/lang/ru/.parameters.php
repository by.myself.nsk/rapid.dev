<?
global $MESS;
if(!is_array($MESS))
    $MESS = array();
$MESS = array_merge($MESS, array(
    'RPDUF_PARAM_ELEMENT_NAME_TEMPLATE' => 'Шаблон имени нового элемента инфоблока',
    'RPDUF_PARAM_ELEMENT_NAME_TEMPLATE_DEF' => 'Запрос от #DATE#',
    'RPDUF_PARAM_IS_ELEMENT_ACTIVE' => 'Элемент инфоблока активен',
    'RPDUF_PARAM_ID_PREFIX' => 'Префикс идентификаторов элементов формы',
    'RPDUF_PARAM_SEND_MAIL' => 'Отправлять сообщение после отправки формы',
    'RPDUF_PARAM_SEND_IMMEDIATE' => 'Отправлять мгновенно без регистрации события',
    'RPDUF_PARAM_DUPLICATE_MAIL' => 'Дублировать письмо на адреса, указанные в настройках главного модуля',
    'RPDUF_PARAM_ADD_MAIL_TYPE' => 'Тип почтового события при добавлении элемента',
    'RPDUF_PARAM_EDIT_MAIL_TYPE' => 'Тип почтового события при редактировании элемента',
    'RPDUF_PARAM_IBLOCK_TYPE' => 'Тип информационного блока (используется только для проверки)',
    'RPDUF_PARAM_IBLOCK_ID' => 'Код информационного блока',
    'RPDUF_PARAM_IBLOCK_SECTION_ID' => 'Раздел информационного блока',
    'RPDUF_PARAM_ENABLE_EDIT' => 'Разрешить редактирование элемента',
    'RPDUF_PARAM_ITEM_ID' => 'ID редактируемого элемента',
    'RPDUF_PARAM_ITEM_CODE' => 'Символьный код редактируемого элемента',
    'RPDUF_PARAM_IBLOCK_FIELD' => 'Поля',
    'RPDUF_PARAM_IBLOCK_PROPERTY' => 'Свойства',
    'RPDUF_PARAM_BLOCK_HEADER' => 'Заголовок блока',
    'RPDUF_PARAM_SHOW_DESCRIPTION_BLOCK' => 'Показывать текстовый блок',
    'RPDUF_PARAM_HIDDEN_INPUTS' => 'Скрывать поля ввода',
    'RPDUF_PARAM_MAX_FILE_SIZE' => 'Максимальный размер загружаемых файлов, байт (0 - не ограничивать)',
    'RPDUF_PARAM_USE_CAPTCHA' => 'Использовать CAPTCHA',
    'RPDUF_PARAM_MSG_SUBMIT' => 'Текст кнопки отправки',
    'RPDUF_PARAM_MSG_SUBMIT_DEF' => 'Отправить',
    'RPDUF_PARAM_EMPTY_MAIL_FIELD' => 'Текст по умолчанию для замены пустых полей почтового сообщения',
    'RPDUF_PARAM_EMPTY_MAIL_FIELD_DEF' => 'Не указано',
));
?>
