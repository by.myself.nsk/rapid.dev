$(function(){
    $('body').on('submit', '.js-uniform', function(e){
        e.preventDefault();
        var $form = $(this);
        var url = $form.attr('action');
        if($form.hasClass('is-loading'))
        {
            return false;
        }
        $form.addClass('is-loading');
        $(this).find('.input-wrapper').removeClass('msg error');
        $.ajax({
            url: url,
            data: new FormData($form.get(0)),
            type: 'POST',
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(answ) {
                var options = {
                    INPUTS: [],
                    INFO: {
                        ERROR: '',
                        MESSAGES: []
                    },
                    id: '',
                    NEW_ELEMENT_ID: '',
                    template: ''
                };
                $.extend(options, answ);
                var $form = $('#' + options.id);
                $form.replaceWith(answ.template);
            }
        }).done(function(){
            $form.removeClass('is-loading');
        });
    });
});
/*/
$(function(){
    var $forms = $('.js-uniform');
    $forms.each(function(){
        var $form = $(this);
        var url = $form.attr('action');
        $form.on('submit', function(){
            if($form.hasClass('is-loading')) {
                // ...
                return false;
            }
            $form.addClass('is-loading');
            $(this).find('.input-wrapper').removeClass('msg error');
            $.ajax({
                url: url,
                data: new FormData($form.get(0)),
                type: 'POST',
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function(answ) {
                    var options = {
                        INPUTS: [],
                        INFO: {
                            ERROR: '',
                            MESSAGES: []
                        },
                        id: '',
                        NEW_ELEMENT_ID: ''
                    };
                    $.extend(options, answ);
                    var $form = $('#' + options.id);
                    $form.removeClass('is-loading');
                    if(options.INFO.ERROR == true)
                    {
                        for(var strInputName in options.INPUTS)
                        {
                            if(options.INPUTS[strInputName].ERROR == true)
                            {
                                var $input = $form.find('*[name="' + strInputName + '"]');
                                $input.parents('.input-wrapper').addClass('msg error');
                            }
                        }
                    }
                    else
                    {
                        switch(options.id)
                        {
                            case '':
                                // ...
                                break;
                        }
                        $form.find('.input-wrapper').removeClass('msg error');
                        $form.get(0).reset();
                    }
                }
            }).done(function(error){
                $form.removeClass('is-loading');
                if(error)
                {
                    // ...
                    return false;
                }

            });
            return false;
        });
    });
});
/**/
