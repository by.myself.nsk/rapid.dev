<?php
/** BitrixVars
 * @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDatabase $DB
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $templateFile
 * @var string $templateFolder
 * @var string $componentPath
 * @var Uniform $component
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
$this->setFrameMode(true);

$component->getErrorsFormatted();
require 'printProp.php';
if(!empty($arResult['PROPERTIES']) || !empty($arResult['FIELDS']))
{
    ?><form <?
        ?>method="POST" <?
        ?>enctype="multipart/form-data" <?
        ?>id="<?=$arResult['FORM_ID']?>" <?
        ?>action="<?=$arResult['ACTION']?>" <?
        ?>class="uniForm js-uniform"<?
    ?>><?
    $component->printHiddenInputs();
        if(!empty($arParams['BLOCK_HEADER']))
        {
            ?><h2 class="uniFormHeader"><?=$arParams['BLOCK_HEADER']?></h2><?
        }
        if($arParams['SHOW_DESCRIPTION_BLOCK'] == 'Y')
        {
            ?><div class="row"><?
                $APPLICATION->IncludeFile(
                    $templateFolder.'/description.php',
                    Array(),
                    Array(
                        'SHOW_BORDER' => true,
                        'NAME' => 'текстовый блок формы',
                    )
                );
            ?></div><?
        }
        if(!empty($arResult['FIELDS']))
        {
            ?><div class="fields"><?
            foreach ($arResult['FIELDS'] as $arField)
            {
                ?><div class="row">
                    <div class="input-wrapper<?=$arField['ERROR'] ? ' msg error' : ''?>"><?
                        $printProp($arField, $arResult['FORM_ID'], true);
                    ?></div>
                </div><?
            }
            ?></div><?
        }
        if(!empty($arResult['PROPERTIES']))
        {
            ?><div class="properties"><?
                foreach($arResult['PROPERTIES'] as $arProperty)
                {
                    ?><div class="row">
                        <div class="input-wrapper<?=$arProperty['ERROR'] ? ' msg error' : ''?>"><?
                            $printProp($arProperty, $arResult['FORM_ID'], false);
                        ?></div>
                    </div><?
                }
            ?></div><?
        }
        if($arParams["USE_CAPTCHA"] == "Y")
        {
            ?><div class="row">
                <div class="input-wrapper">
                    <input type="hidden" name="CAPTCHA_SID" value="<?=$arResult["CAPTCHA_CODE"]?>"/>
                    <label class="required" data-value="captcha"><?
                        echo GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT");
                    ?></label>
                    <img class="captcha-image" src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA"/>
                    <input type="text" name="CAPTCHA_WORD" maxlength="50" value="">
                </div>
            </div><?
        }
        ?><div class="row">
            <div class="input-wrapper">
                <input type="submit" value="<?=$arParams['MSG_SUBMIT']?>"/>
            </div>
        </div>
    </form><?
}
