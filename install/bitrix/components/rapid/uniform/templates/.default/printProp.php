<?php
/**@global CMain $APPLICATION */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$printProp = function($arProperty, $strIdPrefix='', $bIsField = false)
{
    $strId = (!isset($arProperty['MULTIPLE']) || $arProperty['MULTIPLE'] != 'Y') ? ' id="'.$strIdPrefix.$arProperty['ID'].'"' : '';
    $strNamePrefix = $bIsField ? 'FIELD_' : 'PROPERTY_';
    $strName = ' name="'.$strNamePrefix.$arProperty['CODE'].((!isset($arProperty['MULTIPLE']) || $arProperty['MULTIPLE'] != 'Y') ? '"' : '[]"');
    $strHidden = !empty($arProperty['HIDDEN']) ? ' hidden' : '';
    if(empty($arProperty['HIDDEN']))
    {
        ?><label for="<?=$strIdPrefix?><?=$arProperty['ID']?>"<?=$arProperty['IS_REQUIRED'] == 'Y' ? ' class="required"' : ''?> data-value="<?=ToLower($arProperty['NAME'])?>"><?=$arProperty['NAME']?></label><?
    }
    ob_start();
    switch($arProperty['PROPERTY_TYPE'])
    {
        default:
        case 'S': // строка
        case 'N': // число
            if((int)$arProperty['ROW_COUNT'] > 1)
            {
                ?><textarea<?=$strId?><?=$strName?> cols="<?=$arProperty['COL_COUNT']?>" rows="<?=$arProperty['ROW_COUNT']?>"<?=$strHidden?>></textarea><?
            }
            else
            {
                ?><input type="text"<?=$strId?><?=$strName?> maxlength="<?=$arProperty['COL_COUNT']?>"<?=$strHidden?>/><?
            }
            break;
        case 'L': // список
        case 'G': // привязка к разделам
        case 'E': // привязка к элементам
            if($arProperty['LIST_TYPE'] == 'C')
            {// флажки
                if($arProperty['MULTIPLE'] != 'Y')
                    $strInputType = 'radio';
                else
                    $strInputType = 'checkbox';
                foreach($arProperty['VALUES'] as $strKey => $arValue)
                {
                    ?><input type="<?=$strInputType?>"<?=$strName?> value="<?=$arValue['ID']?>"<?=$arValue['DEF'] == 'Y' ? ' checked' : ''?><?=$strHidden?>/><?=$arValue['VALUE']?><?
                }
            }
            elseif($arProperty['LIST_TYPE'] == 'L')
            {// выпадающий список
                if($arProperty['MULTIPLE'] != 'Y')
                {
                    ?><select size="1"<?=$strId?><?=$strName?><?=$strHidden?>><?
                    $bHasDefault = false;
                    foreach($arProperty['VALUES'] as $strKey => $arValue)
                    {
                        if($arValue['DEF'] == 'Y')
                        {
                            $bHasDefault = true;
                            break;
                        }
                    }
                    if($arProperty['IS_REQUIRED'] != 'Y')
                    {
                        ?><option value=""<?=!$bHasDefault ? ' selected' : ''?>></option><?
                    }
                }
                else
                {
                    ?><select size="<?=$arProperty['MULTIPLE_CNT']?>" multiple<?=$strId?><?=$strName?><?=$strHidden?>><?
                }
                foreach($arProperty['VALUES'] as $strKey => $arValue)
                {
                    ?><option value="<?=$arValue['ID']?>"<?=$arValue['DEF'] == 'Y' ? ' selected' : ''?>><?=$arValue['VALUE']?></option><?
                }
                ?></select><?
            }
            break;
        case 'F': // файл
            ?><input type="file"<?=$strId?><?=$strName?><?=$strHidden?>/><?
            break;
    }
    $strInputContent = ob_get_contents();
    ob_end_clean();
    if($arProperty['MULTIPLE'] != 'Y' || $arProperty['PROPERTY_TYPE'] == 'L')
    {
        echo $strInputContent;
    }
    else
    {
        for($intPropertyIterator = 0; $intPropertyIterator < (int)$arProperty['MULTIPLE_CNT']; $intPropertyIterator++)
        {
            echo $strInputContent;
            if($intPropertyIterator == (int)$arProperty['MULTIPLE_CNT'] - 1)
            {
                ?><button class="js-delete-input"<?=!$intPropertyIterator ? ' disabled' : ''?>>-</button><?
                ?><button class="js-add-input">+</button><?
            }
        }
    }
};
