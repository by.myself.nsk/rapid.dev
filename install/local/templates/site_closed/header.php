<?php
/**@global CMain $APPLICATION */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
global $APPLICATION;
?><!DOCTYPE html>
    <html>
        <head>
            <meta name="viewport" content="width=device-width, initial-scale=1"><?
                $APPLICATION->ShowHead();
            ?><title><?$APPLICATION->ShowTitle();?></title>
            <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
        </head>
        <body>
            <div id="panel">
                <?$APPLICATION->ShowPanel();?>
            </div>
            <?
            $APPLICATION->ShowTitle(false);
