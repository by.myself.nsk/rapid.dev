<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $MESS;
if (!is_array($MESS)) {
    $MESS = [];
}
$MESS += [
    'foo' => 'bar'
];
