<?php

use Rapid\Dev\Helper\File;

if (class_exists('rapid_dev')) {
    return;
}

class rapid_dev extends CModule
{
    var $path;

    public function __construct()
    {
        $this->fillModuleFields();
        $this->includeModuleInit();
    }

    private function fillModuleFields()
    {
        $this->path = $this->getModulePath();
        $this->MODULE_ID = $this->getModuleIdFromModulePath($this->path);
        $this->MODULE_NAME = GetMessage('RAPID_DEV_PROJECT_NAME');
        $this->MODULE_DESCRIPTION = GetMessage("RAPID_DEV_DEVELOPER_PROJECT_DESCRIPTION");
        $this->PARTNER_NAME = GetMessage('RAPID_DEV_DEVELOPER_NAME');
        $this->PARTNER_URI = '';
        $arModuleVersion = [];
        /** @noinspection PhpIncludeInspection */
        include($this->path . '/version.php');
        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }
    }

    private function getModulePath()
    {
        return dirname(__FILE__);
    }

    private function getModuleIdFromModulePath($modulePath)
    {
        $modulePath = substr($modulePath, strlen($_SERVER['DOCUMENT_ROOT']));
        $arPaths = explode(DIRECTORY_SEPARATOR, $modulePath);
        $pos = array_search('modules', $arPaths);
        $modulePos = $pos + 1;
        $moduleId = $arPaths[$modulePos];
        return $moduleId ?: '';
    }

    private function includeModuleInit()
    {
        /** @noinspection PhpIncludeInspection */
        include_once $this->path . '/../include.php';
    }

    public function DoInstall()
    {
        if (!$this->InstallFiles()) {
            return;
        }
        $this->InstallEvents();
        RegisterModule($this->MODULE_ID);
    }

    public function InstallFiles()
    {
        $this->copyBitrixDir();
        $this->copyLocalDirWithIgnore();
        $this->copyLocalDirWithoutIgnore();
        return $this;
    }

    private function copyBitrixDir()
    {
        File::copyDirFiles(
            $this->path . '/bitrix',
            $_SERVER['DOCUMENT_ROOT'] . '/bitrix',
            true,
            true
        );
    }

    private function copyLocalDirWithIgnore()
    {
        File::copyDirFiles(
            $this->path . '/local',
            $_SERVER['DOCUMENT_ROOT'] . '/local',
            true,
            true,
            false,
            array(
                'php_interface/lang',
                'php_interface/classes',
                'templates',
            )
        );
    }

    private function copyLocalDirWithoutIgnore()
    {
        File::copyDirFiles(
            $this->path . '/local',
            $_SERVER['DOCUMENT_ROOT'] . '/local',
            false,
            true,
            false
        );
    }

    public function InstallEvents()
    {
        RegisterModuleDependences("main", "OnBeforeProlog", $this->MODULE_ID); // autoload
        $this->registerEventHandlersFromClass('Rapid\Dev\Events');
        return $this;
    }

    /**
     * метод регистрирует обработчики событий
     * @param string $strClassName <p>название класса, методы которого являются обработчиками событий</p>
     * @return bool <p><b>false</b>, если ни один обработчик не зарегистрирован (класс или его методы не существуют, методы не соответствуют регулярному выражению), <b>true</b> в остальных случаях.<p><br>
     * <p>Названия методов обработчиков должны соответствовать регулярному выражению /^(.+)_On(.+)$/<br>$1 - модуль, инициирующий событие<br>$2 - название обработчика события</p>
     * <p><i>Например, main_OnBeforeProlog</i></p>
     */
    private function registerEventHandlersFromClass($strClassName)
    {
        $arClassMethods = get_class_methods($strClassName);
        if (empty($arClassMethods)) {
            return false;
        }
        $bAdded = false;
        foreach ($arClassMethods as &$strMethod) {
            if (!preg_match('/^(.+)_On(.+)$/', $strMethod)) {
                continue;
            }
            $strFromModuleId = preg_replace('/^(.*)_On(.*)$/', '$1', $strMethod);
            $strEventId = 'On' . preg_replace('/^(.*)_On(.*)$/', '$2', $strMethod);
            RegisterModuleDependences($strFromModuleId, $strEventId, $this->MODULE_ID, $strClassName, $strMethod);
            $bAdded = true;
        }
        unset($strMethod);
        return $bAdded;
    }

    public function DoUninstall()
    {
        $this
            ->UnInstallEvents()
            ->UnInstallFiles();
        UnRegisterModule($this->MODULE_ID);
    }

    public function UnInstallEvents()
    {
        global $DB;
        $dbModuleDepends = $DB->Query('SELECT * FROM b_module_to_module WHERE TO_MODULE_ID = "' . $this->MODULE_ID . '"');
        while ($arModuleDepend = $dbModuleDepends->Fetch()) {
            UnRegisterModuleDependences(
                $arModuleDepend['FROM_MODULE_ID'],
                $arModuleDepend['MESSAGE_ID'],
                $arModuleDepend['TO_MODULE_ID'],
                $arModuleDepend['TO_CLASS'],
                $arModuleDepend['TO_METHOD']
            );
        }
        return $this;
    }

    public function UnInstallFiles()
    {
        $strDirFrom = $this->path . '/bitrix/components/rapid';
        $strDirTo = '/bitrix/components/rapid';
        if (is_dir($strDirFrom)) {
            $obDir = dir($strDirFrom);
            while ($strFileName = $obDir->read()) {
                if ($strFileName == "." || $strFileName == "..") {
                    continue;
                }
                DeleteDirFilesEx($strDirTo . "/" . $strFileName);
            }
            $obDir->close();
        }
        return $this;
    }
/*    protected function checkComposer()
    {
        return $this->checkComposerInstallation();
    }

    protected function checkComposerInstallation()
    {
        return strlen(exec('which composer')) > 0;
    }*/
}
